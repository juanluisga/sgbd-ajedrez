VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmTorneos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Torneos."
   ClientHeight    =   5685
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8730
   Icon            =   "frmTorneos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5685
   ScaleWidth      =   8730
   Tag             =   "Torneos."
   Begin TabDlg.SSTab SSTab1 
      Height          =   5655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   9975
      _Version        =   327680
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "&Datos Torneo"
      TabPicture(0)   =   "frmTorneos.frx":030A
      Tab(0).ControlCount=   17
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblTor(5)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblTor(4)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblTor(3)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblTor(2)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblTor(1)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblTor(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "fraTor"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "txtTor(5)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmdTor(1)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cmdTor(0)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "chkTor"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtTor(4)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtTor(3)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtTor(2)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtTor(1)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "txtTor(0)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lstTor"
      Tab(0).Control(16).Enabled=   0   'False
      TabCaption(1)   =   "&Resultados"
      TabPicture(1)   =   "frmTorneos.frx":0326
      Tab(1).ControlCount=   3
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdTor(3)"
      Tab(1).Control(0).Enabled=   -1  'True
      Tab(1).Control(1)=   "cmdTor(2)"
      Tab(1).Control(1).Enabled=   -1  'True
      Tab(1).Control(2)=   "lvwResult"
      Tab(1).Control(2).Enabled=   0   'False
      Begin VB.CommandButton cmdTor 
         Height          =   570
         Index           =   3
         Left            =   -68220
         Picture         =   "frmTorneos.frx":0342
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Actualizar lista"
         Top             =   4920
         Width           =   765
      End
      Begin VB.CommandButton cmdTor 
         Height          =   570
         Index           =   2
         Left            =   -67200
         Picture         =   "frmTorneos.frx":1184
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Ver partida"
         Top             =   4920
         Width           =   765
      End
      Begin VB.ListBox lstTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   300
         Sorted          =   -1  'True
         TabIndex        =   9
         Top             =   3120
         Visible         =   0   'False
         Width           =   3945
      End
      Begin VB.TextBox txtTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   300
         TabIndex        =   1
         Top             =   675
         Width           =   7200
      End
      Begin VB.TextBox txtTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   3090
         TabIndex        =   6
         Top             =   2115
         Width           =   1155
      End
      Begin VB.TextBox txtTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   300
         TabIndex        =   3
         Top             =   1545
         Width           =   3030
      End
      Begin VB.TextBox txtTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   300
         TabIndex        =   5
         Top             =   2115
         Width           =   2670
      End
      Begin VB.TextBox txtTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   300
         TabIndex        =   7
         Top             =   2745
         Width           =   3645
      End
      Begin VB.CheckBox chkTor 
         Caption         =   "Open"
         Height          =   240
         Left            =   3540
         TabIndex        =   4
         Top             =   1560
         Width           =   750
      End
      Begin VB.CommandButton cmdTor 
         Caption         =   "..."
         Height          =   375
         Index           =   0
         Left            =   3960
         TabIndex        =   8
         Top             =   2745
         Width           =   300
      End
      Begin VB.CommandButton cmdTor 
         Default         =   -1  'True
         Height          =   570
         Index           =   1
         Left            =   7620
         Picture         =   "frmTorneos.frx":148E
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Width           =   765
      End
      Begin VB.TextBox txtTor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Index           =   5
         Left            =   4410
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   1545
         Width           =   4005
      End
      Begin VB.Frame fraTor 
         Caption         =   "Torneos"
         Height          =   1950
         Left            =   360
         TabIndex        =   12
         Top             =   3420
         Width           =   8055
         Begin ComctlLib.ListView lvwTor 
            Height          =   1590
            Left            =   60
            TabIndex        =   11
            Top             =   240
            Width           =   7905
            _ExtentX        =   13944
            _ExtentY        =   2805
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   327680
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmTorneos.frx":1798
            NumItems        =   4
            BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "C�digo"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               SubItemIndex    =   1
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Denominaci�n"
               Object.Width           =   6174
            EndProperty
            BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               SubItemIndex    =   2
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Lugar"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               SubItemIndex    =   3
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Fecha"
               Object.Width           =   1587
            EndProperty
         End
      End
      Begin ComctlLib.ListView lvwResult 
         Height          =   4350
         Left            =   -74880
         TabIndex        =   13
         Top             =   480
         Width           =   8445
         _ExtentX        =   14896
         _ExtentY        =   7673
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327680
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "frmTorneos.frx":17B4
         NumItems        =   4
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Ronda"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Blancas"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   2
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Negras"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   2
            SubItemIndex    =   3
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Resultado"
            Object.Width           =   1764
         EndProperty
      End
      Begin VB.Label lblTor 
         AutoSize        =   -1  'True
         Caption         =   "Denominaci�n"
         Height          =   195
         Index           =   0
         Left            =   315
         TabIndex        =   19
         Top             =   495
         Width           =   1020
      End
      Begin VB.Label lblTor 
         AutoSize        =   -1  'True
         Caption         =   "Organizador"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   18
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label lblTor 
         AutoSize        =   -1  'True
         Caption         =   "Lugar de celebraci�n"
         Height          =   195
         Index           =   2
         Left            =   300
         TabIndex        =   17
         Top             =   1935
         Width           =   1500
      End
      Begin VB.Label lblTor 
         AutoSize        =   -1  'True
         Caption         =   "Fecha"
         Height          =   195
         Index           =   3
         Left            =   3090
         TabIndex        =   16
         Top             =   1935
         Width           =   450
      End
      Begin VB.Label lblTor 
         AutoSize        =   -1  'True
         Caption         =   "Pais"
         Height          =   195
         Index           =   4
         Left            =   300
         TabIndex        =   15
         Top             =   2580
         Width           =   300
      End
      Begin VB.Label lblTor 
         AutoSize        =   -1  'True
         Caption         =   "Comentario"
         Height          =   195
         Index           =   5
         Left            =   4410
         TabIndex        =   14
         Top             =   1365
         Width           =   795
      End
   End
End
Attribute VB_Name = "frmTorneos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strDen As String
    ' Guarda la denominaci�n del �ltimo tipo de modalidad
    ' buscado. Se utiliza para refrescar la lista despu�s
    ' de las modificaciones.
Private mobjDatos As New clsDatos
Private RsTor  As Recordset

Public Estado As String
Private ReLoad As Boolean

' Variable para controlar cambios
Private blnChange As Boolean

Private Sub InicioForm()
'***********************************************************************************
' Nombre...........................: InicioForm
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 18/8/99
' Descripci�n...................: Crea objetos y configura el inicio del form.
'***********************************************************************************
    Dim lngStyle As Long

    ' Dimensionamos el formulario.
    Width = 8820
    Height = 6060
    
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_TORNEOS
    
    ' Para poder se�alar un fila completa del ListView
    lngStyle = SendMessage(lvwTor.hWnd, _
              LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    lngStyle = lngStyle Or LVS_EX_FULLROWSELECT
    Call SendMessage(lvwTor.hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, ByVal lngStyle)

    lngStyle = SendMessage(lvwResult.hWnd, _
              LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    lngStyle = lngStyle Or LVS_EX_FULLROWSELECT
    Call SendMessage(lvwResult.hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, ByVal lngStyle)

    ReLoad = False
    frmAbiertos = frmAbiertos + 1
    DisableAllFields
End Sub

Private Sub cmdTor_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Ayuda de campo
            ListaPaises lstTor, txtTor(4)
        Case 1
            ' Bot�n "Buscar Ahora"
            ThrowSearch
        Case 2
            ' Hiperv�nculo con
            ' el formulario "Partidas"
            Dim mItem As ListItem
            Set mItem = lvwResult.SelectedItem
            If Not mItem Is Nothing Then
                frmPartidas.IdPartida = _
                    Mid$(mItem.Key, 2, Len(mItem.Key))
            End If
        Case 3
            ' Actualizar lista de partidas
            Clock 1
            ShowResults RsTor!to_id
            Clock 0
    End Select

End Sub

Private Sub chkTor_Click()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub FinForm()
'****************************************************************************
' Nombre...........................: FinForm
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 18/8/99
' Descripci�n...................: Destruye los objetos.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If

    CheckChanges blnChange
    Set RsTor = Nothing
    Set mobjDatos = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub DisableAllFields()
'****************************************************************************
' Nombre..........................: DisableAllFields
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio..............: 18/8/99
' Descripci�n..................: Deshabilita los campos del formulario.
'****************************************************************************
    Dim ctlTextBox As TextBox
    Dim ctlOptBoton As OptionButton
    
    For Each ctlTextBox In txtTor
        DesActivarTxt ctlTextBox
    Next
    DesActivarChk chkTor
    DesActivarLstView lvwTor
    DesActCmd cmdTor(0)
    cmdTor(1).Enabled = False
    With SSTab1
        .Tab = 0
        .TabEnabled(0) = False
        .TabEnabled(1) = False
    End With
    
End Sub

Private Sub ThrowSearch()
'****************************************************************************
' Nombre............................: ThrowSearch
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 18/8/99
' Descripci�n....................: Lanza el proceso de b�squeda.
'****************************************************************************
    Dim strSQL As String
    
    Clock 1
    strDen = "*" & txtTor(0) & "*"
    If Not (Trim$(txtTor(0)) = "") Then
        ' Buscar por los criterios seleccionados.
        strSQL = "SELECT * FROM Torneos WHERE to_den LIKE " & _
                SnglQuote(strDen) & _
                " ORDER BY to_den"
    Else
        ' Buscar todos
        strSQL = "Torneos"
    End If
    Set RsTor = dbDatos.OpenRecordset(strSQL)
    If Not RsTor.EOF Then
        DisplayRegs RsTor
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    Clock 0
End Sub

Private Sub DisplayRegs(Rs As Recordset)
'************************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra en la lista de selecci�n los registros
'..............................................: recuperados en el proceso de b�squeda.
'***********************************************************************************

    Dim mItem As ListItem
    
    lvwTor.ListItems.Clear
    With Rs
        Do While Not .EOF
            ' Las claves (Key) de los items del lvw no pueden empezar
            ' por un n�mero, por lo que le colocamos una X delante.
            ' Es un poco chapuza pero no hay otra forma de salvar ese bug.
            Set mItem = lvwTor.ListItems.Add(, "X" & CStr(!to_id), CStr(!to_id))
            mItem.Tag = CStr(!to_id)
            mItem.SubItems(1) = !to_den
            mItem.SubItems(2) = CheckForNull(!to_loc, "STRING")
            mItem.SubItems(3) = Format(CheckForNull(!to_fec, "DATE"), "dd/mm/yyyy")
            .MoveNext
        Loop
    End With
End Sub

Private Sub SetSelectControls()
'******************************************************************************************
' Nombre.............................: SetSelectControls
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Habilita los controles del formulario necesarios
'..............................................: para que el usuario pueda seleccionar entre los
' .............................................: registros recuperados en la b�squeda.
'*****************************************************************************************

    Dim ctlTextBox As TextBox

    mobjControlMDI.Buscar
    For Each ctlTextBox In txtTor
        DesActivarTxt ctlTextBox
    Next
    DesActivarChk chkTor
    DesActCmd cmdTor(0)
    ActivarLstView lvwTor
    cmdTor(1).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
    End With

End Sub

Public Sub Buscar()
    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub SetSearchControls()
'************************************************************************************
' Nombre...........................: SetSearchControls
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 18/8/99
' Descripci�n...................: Habilita los campos del formulario para que
'............................................: el usuario pueda hacer b�squedas.
'************************************************************************************
    
    ActivarTxt txtTor(0)
    txtTor(0).SetFocus
    cmdTor(1).Enabled = True
    DesActivarLstView lvwTor
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
    End With
    
End Sub

Private Sub lstTor_DblClick()
    ' Ayuda de campo de paises.
    With lstTor
        txtTor(4) = .List(.ListIndex)
        txtTor(4).Tag = .ItemData(.ListIndex)
        .Visible = False
    End With
End Sub

Private Sub lstTor_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 27
            ' Si ESC escondemos la lista
            lstTor.Visible = False
            txtTor(4).Tag = ""
            txtTor(4) = ""
        Case 13
            ' ENTER = Doble Click
            lstTor_DblClick
    End Select
End Sub

Private Sub lstTor_LostFocus()
    lstTor.Visible = False
End Sub

Private Sub lvwTor_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
    ' Ordenar el lvw
    With lvwTor
        .SortKey = ColumnHeader.Index - 1
        .Sorted = True
    End With
End Sub

Private Sub lvwTor_ItemClick(ByVal Item As ComctlLib.ListItem)
    ' Mostrar el registro seleccionado en la lista.
    
    Clock 1
    CheckChanges blnChange
    If RsTor.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        RsTor.Index = "to_id"
        RsTor.Seek "=", CLng(Val(Mid$(Item.Key, 2, Len(Item.Key) - 1)))
    Else
        ' Utilizamos el m�todo FIND si el Recordset es DYNASET o SNAPSHOT
        RsTor.FindFirst "to_id =" & CLng(Val(Mid$(Item.Key, 2, Len(Item.Key) - 1)))
    End If
    SetEditControls
    mobjControlMDI.Edicion
    DisplayFields
    Clock 0

End Sub

Private Sub SetEditControls()
'****************************************************************************
' Nombre............................: SetEditControls
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 19/8/99
' Descripci�n....................: Habilita los controles necesarios para
'.............................................: operaciones de busqueda y alta.
'****************************************************************************
    Dim ctlTextBox As TextBox
    Dim ctlOptBoton As OptionButton
    
    For Each ctlTextBox In txtTor
        ActivarTxt ctlTextBox
    Next
    txtTor(0).SetFocus
    ActivarChk chkTor
    ActCmd cmdTor(0)
    cmdTor(1).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        If Estado = "ALTA" Then
            .TabEnabled(1) = False
        Else
            .TabEnabled(1) = True
        End If
    End With

    
End Sub

Private Sub DisplayFields()
'*************************************************************************************
' Nombre.............................: DisplayFields
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 17/8/99
' Descripci�n.....................: Muestra en el form los datos del registro le�do.
'**************************************************************************************

    ClearFields
    With RsTor
        txtTor(0) = CheckForNull(!to_den, "STRING")
        txtTor(1) = Format$(CheckForNull(!to_fec, "DATE"), "dd/mm/yyyy")
        txtTor(2) = CheckForNull(!to_org, "STRING")
        txtTor(3) = CheckForNull(!to_loc, "STRING")
        txtTor(4) = mobjDatos.GetPais(CheckForNull(!to_pais, "LONG"))
        txtTor(4).Tag = IIf(IsNull(!to_pais), "", !to_pais)
        txtTor(5) = CheckForNull(!to_comen, "STRING")
        If !to_open Then
            chkTor.Value = 1
        Else
            chkTor.Value = 0
        End If
        ShowResults !to_id
    End With
    blnChange = False
End Sub

Private Sub ClearFields()
'**************************************************************************************
' Nombre..............................: ClearFields
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio.................: 19/8/99
' Descripci�n......................: Borra el contenido de los campos del form.
'**************************************************************************************
    Dim ctlTextBox As TextBox
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    
    For Each ctlTextBox In txtTor
        ctlTextBox = ""
        ctlTextBox.Tag = ""
    Next
    chkTor.Value = 0
    blnChange = False
End Sub

Public Sub Cancelar()
'**************************************************************************************
' Nombre..............................: Cancelar
' Tipo.....................................: Sub
' �mbito................................: P�blico
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio..................: 4/8/99
' Descripci�n.......................: Cancela la acci�n que se estuviera
' ...............................................: realizando y vuelve al estado de espera.
'*************************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If

    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
End Sub

Public Sub Insertar()
    mobjControlMDI.Alta
    SetEditControls
    ClearFields

End Sub

Public Sub Grabar()
'********************************************************************************************
' Nombre.........................: Grabar
' Tipo................................: Sub
' �mbito...........................: P�blico
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............:19/8/99
' Descripci�n.................: Graba un reg. en la BB.DD. con los datos introducidos.
'********************************************************************************************
    Dim mItem As ListItem
    
    ' Validamos datos.
    If Not DatosValidos() Then Exit Sub
    ' Si el objeto RsTor a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If RsTor Is Nothing Then _
        Set RsTor = dbDatos.OpenRecordset("Torneos", dbOpenDynaset)
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            RsTor.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            RsTor.Edit
    End Select

    AsignarValores
    RsTor.Update
    If Estado = "EDICION" Then
        With RsTor
'                Set mItem = lvwApes.SelectedItem
            Set mItem = lvwTor.FindItem(CStr(!to_id), lvwTag)
            If Not mItem Is Nothing Then
                mItem.Text = !to_id
                mItem.SubItems(1) = !to_den
                mItem.SubItems(2) = CheckForNull(!to_loc, "STRING")
                mItem.SubItems(3) = Format(CheckForNull(!to_fec, "DATE"), "dd/mm/yyyy")
            End If
        End With
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    ClearFields
    SetSelectControls
    mobjControlMDI.Espera
End Sub

Private Function DatosValidos() As Boolean
'****************************************************************************
' Nombre........................: DatosValidos
' Tipo...............................: Function
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Booleano
' �ltimo cambio...........: 18/8/99
' Descripci�n................: Valida los datos
' ........................................: referidos al jugador.
'****************************************************************************

    Dim msg$, cCtrlErr As Control, iCod%, j%

    msg = "": iCod = 0: DatosValidos = True
    
    ' Denominaci�n
    If Len(txtTor(0)) = 0 Then
        Set cCtrlErr = txtTor(0)
        msg = "La denominaci�n del torneo es obligatoria."
        iCod = 1
    ElseIf Len(txtTor(0)) > 65 Then
        Set cCtrlErr = txtTor(0)
        msg = "Longitud m�x. 65 caracteres."
        iCod = 2
    End If
    
    ' Fecha
    If iCod = 0 And Validar(txtTor(1)) = False Then
        Set cCtrlErr = txtTor(1)
        msg = "El formato de fecha es 'dd/mm/aaaa'."
        iCod = 3
    End If
    
    ' Organizador
    If iCod = 0 And Len(txtTor(2)) > 20 Then
        Set cCtrlErr = txtTor(2)
        msg = "Longitud m�x. 20 caracteres."
        iCod = 4
    End If
    
    ' Lugar de celebraci�n
    If iCod = 0 And Len(txtTor(3)) > 20 Then
        Set cCtrlErr = txtTor(3)
        msg = "Longitud m�x. 20 caracteres."
        iCod = 5
    End If
    
    
    If iCod = 0 And Len(txtTor(5)) > 2500 Then
        Set cCtrlErr = txtTor(5)
        msg = "Longitud m�x. 2500 caracteres."
        iCod = 6
    End If
    
    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
    
End Function

Private Sub AsignarValores()
'****************************************************************************
' Nombre............................: AsignarValores
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 19/8/99
' Descripci�n....................: Asigna los valores que el usuario ha
'.............................................: introducido en el formulario a la clase.
'****************************************************************************

    With RsTor
        !to_den = txtTor(0)
        !to_fec = IIf(Len(txtTor(1)) = 0, Null, Format(txtTor(1), "dd/mm/yyyy"))
        !to_org = txtTor(2)
        !to_loc = txtTor(3)
        !to_pais = IIf(txtTor(4).Tag = "", Null, Val(txtTor(4).Tag))
        !to_comen = txtTor(5)
        Select Case chkTor.Value
            Case 0
                !to_open = False
            Case 1
                !to_open = True
        End Select
    End With
    
End Sub

Public Sub Borrar()
'****************************************************************************
' Nombre.............................: Borrar
' Tipo....................................: Sub
' �mbito...............................: P�blico
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Da de baja un registro.
'****************************************************************************
    Dim Respuesta   As Integer

    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
End Sub

Private Sub Delete()
    
    On Error GoTo errDel
    
    With lvwTor
        .ListItems.Remove .SelectedItem.Index
    End With
    RsTor.Delete
    ClearFields
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number
    
End Sub

Private Sub txtTor_Change(Index As Integer)
    Select Case Index
        Case 4
            If txtTor(Index) = "" Then txtTor(Index).Tag = ""
    End Select
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub txtTor_GotFocus(Index As Integer)
    Select Case Index
        Case 4
            With txtTor(4)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
    txtTor(Index).BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtTor_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 4
            If KeyAscii = vbKeyReturn Then cmdTor_Click 0
    End Select
End Sub

Private Sub txtTor_LostFocus(Index As Integer)
    txtTor(Index) = Trim$(txtTor(Index))
    Select Case Index
        Case 1  ' Fecha
            If IsNumeric(txtTor(Index)) And _
                Len(txtTor(Index)) = 8 Then
                txtTor(Index) = Format$(txtTor(Index), "00/00/0000")
            End If
    End Select
    txtTor(Index).BackColor = CT_COLOR_BLANCO
End Sub

Private Sub ShowResults(ByVal lngIdTor As Long)
'****************************************************************************
' Nombre.......................: ShowResults
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 20/2/00
' Descripci�n...............: Muestra los resultados
' .......................................: correspondientes al
' .......................................: torneo seleccionado.
'****************************************************************************
    Dim rsResult As Recordset
    Dim sSql As String
    Dim mItem As ListItem
    Dim sTanteo As String
    
    sSql = "SELECT Partidas.pt_id As IDPART,Partidas.pt_ronda As RONDA, Jugadores.ju_apenom As BLANCAS, Jugadores_1.ju_apenom As NEGRAS, Partidas.pt_resultado As CODRESULT " & _
            "FROM (Partidas INNER JOIN Jugadores ON Partidas.pt_blancas = Jugadores.ju_id) INNER JOIN Jugadores AS Jugadores_1 ON Partidas.pt_negras = Jugadores_1.ju_id " & _
            "Where (((Partidas.pt_torneo) = " & CStr(lngIdTor) & ")) " & _
            "ORDER BY Partidas.pt_ronda, Partidas.pt_fecha"

    Set rsResult = dbDatos.OpenRecordset(sSql)
    lvwResult.ListItems.Clear
    
    With rsResult
        Do While Not .EOF
            Set mItem = lvwResult.ListItems.Add(, "X" & !IDPART, CStr(!RONDA))
            mItem.SubItems(1) = !blancas
            mItem.SubItems(2) = !negras
            Select Case !CODRESULT
                Case 1
                    sTanteo = "1-0"
                Case 2
                    sTanteo = "0-1"
                Case 3
                    sTanteo = "Tablas"
                Case 4
                    sTanteo = "*"
            End Select
            mItem.SubItems(3) = sTanteo
            .MoveNext
        Loop
        Set rsResult = Nothing
    End With

End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub


