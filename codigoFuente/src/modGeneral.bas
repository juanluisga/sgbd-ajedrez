Attribute VB_Name = "modGeneral"
Option Explicit

Public Function CheckForNull(Field As Field, strDatatype As String) As Variant
 '**********************************************************************************
' Nombre....................: CheckForNull
' Tipo...........................: Function
' �mbito.......................: P�blico
' Par�metros..............: Field, String
' Valor de retorno.....: Ninguno
' �ltimo cambio.........: 7/8/99
' Descripci�n..............: Hace un tratamiento para los valores nulos
' ......................................: para el campo que se pasa como par�metro
' ......................................: en funci�n del tipo de dato al que pertenezca.
'**********************************************************************************

    If IsNull(Field) Then
        Select Case UCase$(strDatatype)
        Case "STRING", "VARIANT", "DATE"
            CheckForNull = ""
        Case Else
            CheckForNull = 0
        End Select
    Else
        CheckForNull = Field.Value
    End If

End Function

Public Function SnglQuote(strValue As String) As String
'**********************************************************************************
' Nombre....................: SnglQuote
' Tipo...........................: Sub
' �mbito.......................: Privado
' Par�metros..............: String
' Valor de retorno.....: Ninguno
' �ltimo cambio.........: 7/8/99
' Descripci�n..............: Encierra entre comillas simples la cadena
' ......................................: que es pasada como par�metro.
'**********************************************************************************

    SnglQuote = Chr$(39) + strValue + Chr$(39)
    
End Function

Public Function HyperJump(ByVal URL As String) As Long

   HyperJump = ShellExecute(0&, vbNullString, URL, vbNullString, vbNullString, vbNormalFocus)
End Function

Sub DesActivarTxt(Txt As TextBox)
'****************************************************************************
' Nombre......................: DesActivarTxt
' Tipo.............................: Procedure
' Ambito........................: Public
' Par�metros...............: TextBox
' Valor de retorno......: Ninguno
' Ultimo cambio.........: 11/6/99
' Descripci�n..............: Desactiva una caja de texto
'****************************************************************************
    With Txt
            .Enabled = False
            .BackColor = CT_COLOR_GRIS
            .ForeColor = CT_COLOR_BLANCO
    End With
End Sub

Sub ActivarChk(chk As CheckBox)
'****************************************************************************
' Nombre........................: ActivarChk
' Tipo...............................: Procedure
' Ambito..........................: Public
' Par�metros.................: CheckBox
' Valor de retorno........:
' Ultimo cambio............: 11/6/99
' Descripci�n................: Activa la caja de chequeo
'****************************************************************************
    chk.Enabled = True
End Sub

Sub DesActivarChk(chk As CheckBox)
'****************************************************************************
' Nombre......................: DesActivarChk
' Tipo.............................: Procedure
' Ambito........................: Public
' Par�metros...............: CheckBox
' Valor de retorno......:
' Ultimo cambio.........: 11/6/99
' Descripci�n..............: Desactiva una caja de chequeo
'****************************************************************************

    chk.Enabled = False
End Sub

Sub ActivarTxt(Txt As TextBox)
'****************************************************************************
' Nombre........................: ActivarTxt
' Tipo...............................: Procedure
' Ambito..........................: Public
' Par�metros.................: TextBox
' Valor de retorno........:
' Ultimo cambio...........: 11/6/99
' Descripci�n.................: Activa una caja de texto
'****************************************************************************

    With Txt
        .Enabled = True
        .BackColor = CT_COLOR_BLANCO
        .ForeColor = CT_COLOR_NEGRO
    End With
End Sub

Sub DesActivarLstView(lstView As ListView)
'****************************************************************************
' Nombre........................: DesActivarLstView
' Tipo...............................: Procedure
' Ambito..........................: Public
' Par�metros.................: ListView
' Valor de retorno........:
' Ultimo cambio............: 11/6/99
' Descripci�n.................: Desactiva el Listview del form. activo
'****************************************************************************

    With lstView
        .Enabled = False
        .BackColor = CT_COLOR_GRIS
        .ListItems.Clear
    End With
End Sub

Sub ActivarLstView(lstView As ListView)
'****************************************************************************
' Nombre...........................: ActivarListView
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: Listwiew
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: Activa el listview del fomrm. activo
'****************************************************************************

    With lstView
        .Enabled = True
        .BackColor = CT_COLOR_BLANCO
    End With
End Sub

Sub ActivarCbo(cbo As ComboBox)
'****************************************************************************
' Nombre...........................: ActivarCbo
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: ComboBox
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: Activa el ComboBox.
'****************************************************************************

    With cbo
        .Enabled = True
        .BackColor = CT_COLOR_BLANCO
        .ForeColor = CT_COLOR_NEGRO
    End With

End Sub

Sub ActivarOpt(Opt As OptionButton)
'****************************************************************************
' Nombre...........................: ActivarOpt
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: OptionButton
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: Activa el OptionButton
'****************************************************************************

     Opt.Enabled = True
End Sub

Sub DesActivarOpt(Opt As OptionButton)
'****************************************************************************
' Nombre...........................: DesActivarOpt
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: OptionButton
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: desactiva el OptionButton
'****************************************************************************

    Opt.Enabled = False
End Sub

Sub DesActivarCbo(cbo As ComboBox)
'****************************************************************************
' Nombre...........................: DesActivarCbo
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: ComboBox
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: Desactiva el ComboBox
'****************************************************************************

    With cbo
        .Enabled = False
        .BackColor = CT_COLOR_GRIS
        .ForeColor = CT_COLOR_BLANCO
    End With
End Sub

Sub ActCmd(cmd As CommandButton)
'****************************************************************************
' Nombre...........................: ActCmd
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: CommandButton
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: Activa el CommandButton
'****************************************************************************

    cmd.Enabled = True
End Sub

Sub DesActCmd(cmd As CommandButton)
'****************************************************************************
' Nombre...........................: DesActCmd
' Tipo..................................: Procedure
' Ambito.............................: Public
' Par�metros....................: CommandButton
' Valor de retorno...........:
' Ultimo cambio...............: 11/6/99
' Descripci�n....................: Desactiva el CommandButton
'****************************************************************************
    cmd.Enabled = False
End Sub

Public Function Validar(sFecha As String) As Variant
'****************************************************************************
' Nombre......................: Validar
' Tipo.............................: Function
' �mbito........................: P�blico
' Par�metros...............: Fecha (cadena)
' Valor de retorno......: Variant. False si no es fecha correcta
' ......................................: y formato fecha si s� lo es.
' �ltimo cambio.........: 8/6/99
' Descripci�n..............: Validaciones de fecha.
'***************************************************************************

    Dim sFec As String, i As Integer, C As Integer, Inicio As Integer, iCar As String
    Dim sMes As String, sA�o As String
    C = 0: i = 0: Inicio = 1
    If Len(sFecha) = 0 Then
        Validar = True
        Exit Function
    End If
    If Len(sFecha) > 10 Or Len(sFecha) < 6 Then
        Validar = False
        Exit Function
    End If
    
    ' Convertimos los guiones "-" en barras "/"
'    sFecha = Replace(sFecha, "-", "/", , , vbTextCompare)
    Mid$(sFecha, 3, 1) = "/"
    Mid$(sFecha, 6, 1) = "/"
    
    For i = 1 To Len(sFecha) 'Solo 0-9 y '/'
        iCar = Asc(Mid$(sFecha, i, 1))
        If iCar < 47 Or iCar > 57 Then
            Validar = False
            Exit Function
        End If
    Next i
    
    Do While Inicio <= Len(sFecha)
        i = InStr(Inicio, sFecha, "/")
        If i = 0 Then Exit Do
        Inicio = i + 1
        C = C + 1
    Loop
    
    Select Case C
    Case 0 'No hay ning�n '/', por lo tanto debe escribir 8 caracteres.
       If Len(sFecha) <> 8 Then
          Validar = False
          Exit Function
       End If
       sMes = Mid$(sFecha, 3, 2)
       sFec = Left$(sFecha, 2) & "/" & sMes & "/" & Right$(sFecha, 4)
       Validar = sFec
    Case 2 'Hay dos '/' pero pueden estar mal colocados
       Inicio = InStr(1, sFecha, "/")
       i = InStr(Inicio + 1, sFecha, "/")
       sMes = Mid$(sFecha, Inicio + 1, i - Inicio - 1)
       sA�o = Mid$(sFecha, i + 1)
       If Len(sMes) = 0 Or Len(sA�o) <> 4 Then
          Validar = False
          Exit Function
       End If
       sFec = sFecha
       Validar = sFecha
    Case Else
       Validar = False
       Exit Function
   End Select
   
   If (Val(sMes) > 12) Or (Val(sMes) < 1) Or (Not IsDate(sFec)) Then
      Validar = False
      Exit Function
   End If
End Function

Public Sub Clock(iVal As Integer)
'***************************************************************************************
' Nombre......................: Clock
' Tipo.............................: Sub
' �mbito........................: P�blico
' Par�metros...............: Integer -> 1= reloj ON
' ......................................:                     0 = reloj OFF
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 8/6/99
' Descripci�n..............: Muestra en pantalla el puntero o el reloj.
'.......................................: �til para indicar que la aplicaci�n est� ocupada.
'**************************************************************************************

    If iVal = 0 Then
        Screen.ActiveForm.MousePointer = 1
    ElseIf iVal = 1 Then
        Screen.ActiveForm.MousePointer = 11
    End If
End Sub

Public Sub DesActivarLstBox(Lst As ListBox)
'****************************************************************************
' Nombre......................: DesActivarLstBox
' Tipo.............................: Sub
' �mbito........................: P�blico
' Par�metros...............: ListBox
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 8/6/99
' Descripci�n..............: Desactiva el listbox.
'***************************************************************************

    With Lst
        .Clear
        .Enabled = False
        .BackColor = CT_COLOR_GRIS
        .ForeColor = CT_COLOR_BLANCO
    End With
End Sub

Public Sub ActivarLstBox(Lst As ListBox)
'****************************************************************************
' Nombre......................: ActivarLstBox
' Tipo.............................: Sub
' �mbito........................: P�blico
' Par�metros...............: ListBox
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 8/6/99
' Descripci�n..............: Activa el listbox.
'***************************************************************************

    With Lst
        .Enabled = True
        .BackColor = CT_COLOR_BLANCO
        .ForeColor = CT_COLOR_NEGRO
    End With
End Sub

Public Sub GetPicture(f As Field, img As Image)
'*****************************************************************************
' Nombre.........................: GetPicture
' Tipo................................: Procedure
' Ambito...........................: Public
' Par�metros..................: Field, image
' Valor de retorno.........: Ninguno
' Ultimo cambio.............: 16/3/00
' Descripci�n..................: Trae una imagen almacenada en la
' ..........................................: base de datos a un control image.
'****************************************************************************

    Dim X() As Byte
    Dim ff As Integer
    
    ff = FreeFile
    Open "pic" For Binary Access Write As ff
    
    X() = f.GetChunk(0, f.FieldSize)
    Put ff, , X()
    
    Close ff
    img = LoadPicture("pic")
    Kill "pic"
End Sub

Public Sub LetPicture(f As Field, img As Image)
'********************************************************************************
' Nombre........................: LetPicture
' Tipo...............................: Procedure
' Ambito..........................: Public
' Par�metros.................: Field, image
' Valor de retorno.........: Ninguno
' Ultimo cambio............: 16/3/00
' Descripci�n.................: Almacena una imagen contenida en
' .........................................: un control image en la BB.DD.
'********************************************************************************
    Dim X() As Byte
    Dim n As Long
    Dim ff As Integer
    
    SavePicture img.Picture, "pic"
    
    ff = FreeFile
    Open "pic" For Binary Access Read As ff
    n = LOF(ff)
    If n Then
        ReDim X(n)
        Get ff, , X()
        f.AppendChunk X()
        Close ff
    End If
    Kill "pic"

End Sub

Public Sub LoadNAG()
'********************************************************************************
' Nombre...............................: LoadNAG
' Tipo......................................: Procedure
' Ambito.................................: Public
' Par�metros........................: Ninguno
' Valor de retorno...............: Ninguno
' Ultimo cambio..................: 8/4/00
' Descripci�n.......................: Almacena en la matriz "mtNAG" los
' ...............................................: literales correspondientes a las
' ...............................................: etiquetas NAG utilizadas para
' ...............................................: codificar comentarios en las
' ...............................................: partidas en formato PGN.
'*******************************************************************************

    mtNAG(0) = "Anotaci�n nula."
    mtNAG(1) = "Movimiento bueno (tradicional '!')."
    mtNAG(2) = "Movimiento pobre (tradicional '?')."
    mtNAG(3) = "Movimiento muy bueno (tradicional '!!')."
    mtNAG(4) = "Movimiento muy pobre (tradicional '??')."
    mtNAG(5) = "Movimiento especulativo (tradicional '!?')."
    mtNAG(6) = "Movimiento cuestionable (tradicional '?!')."
    mtNAG(7) = "Movimiento forzado (cualquier otro pierde r�pidamente)."
    mtNAG(8) = "Movimiento singular ( ninguna alternativa razonable)."
    mtNAG(9) = "Peor movimiento."
    mtNAG(10) = "Posici�n de tablas."
    mtNAG(11) = "Igualdad de oportunidades, posici�n est�tica."
    mtNAG(12) = "Igualdad de oportunidades, posici�n activa."
    mtNAG(13) = "Posici�n poco clara."
    mtNAG(14) = "Las blancas tienen una ligera ventaja."
    mtNAG(15) = "Las negras tienen una ligera ventaja."
    mtNAG(16) = "Las blancas tienen una ventaja moderada."
    mtNAG(17) = "Las negras tienen una ventaja moderada."
    mtNAG(18) = "Las blancas tienen una ventaja firme."
    mtNAG(19) = "Las negras tienen una ventaja firme."
    mtNAG(20) = "Las blancas tienen una ventaja aplastante (las negras deben rendir)."
    mtNAG(21) = "Las negras tienen una ventaja aplastante (las blancas deben rendir)."
    mtNAG(22) = "Blancas en 'zugzwang'."
    mtNAG(23) = "Negras en 'zugzwang'."
    mtNAG(24) = "Las blancas tienen una leve ventaja de espacio."
    mtNAG(25) = "Las negras tienen una leve ventaja de espacio."
    mtNAG(26) = "Las blancas tienen una ventaja moderada de espacio."
    mtNAG(27) = "Las negras tienen una ventaja moderada de espacio."
    mtNAG(28) = "Las blancas tienen una ventaja firme de espacio."
    mtNAG(29) = "Las negras tienen una ventaja firme de espacio."
    mtNAG(30) = "Las blancas tienen una ligera ventaja de tiempo (desarrollo)."
    mtNAG(31) = "Las negras tienen una ligera ventaja de tiempo (desarrollo)."
    mtNAG(32) = "Las blancas tienen una ventaja moderada de tiempo (desarrollo)."
    mtNAG(33) = "Las negras tienen una ventaja moderada de tiempo (desarrollo)."
    mtNAG(34) = "Las blancas tienen una firme ventaja de tiempo (desarrollo)."
    mtNAG(35) = "Las negras tienen una firme ventaja de tiempo (desarrollo)."
    mtNAG(36) = "Las blancas tienen la iniciativa."
    mtNAG(37) = "Las negras tienen la iniciativa."
    mtNAG(38) = "Las blancas tienen una iniciativa duradera."
    mtNAG(39) = "Las negras tienen una iniciativa duradera."
    mtNAG(40) = "Las blancas tienen el ataque."
    mtNAG(41) = "Las negras tienen el ataque."
    mtNAG(42) = "Las blancas obtienen una compensaci�n insuficiente por d�ficit de material."
    mtNAG(43) = "Las negras obtienen una compensaci�n insuficiente por d�ficit de material."
    mtNAG(44) = "Las blancas obtienen una compensaci�n suficiente por d�ficit de material."
    mtNAG(45) = "Las negras obtienen una compensaci�n suficiente por d�ficit de material."
    mtNAG(46) = "Las blancas obtienen una compensaci�n m�s que adecuada por d�ficit de material."
    mtNAG(47) = "Las negras obtienen una compensaci�n m�s que adecuada por d�ficit de material."
    mtNAG(48) = "Las blancas tienen una ligera ventaja en el dominio del centro."
    mtNAG(49) = "Las negras tienen una ligera ventaja en el dominio del centro."
    mtNAG(50) = "Las blancas tienen una ventaja moderada en el dominio del centro."
    mtNAG(51) = "Las negras tienen una ventaja moderada en el dominio del centro."
    mtNAG(52) = "Las blancas tienen una firme ventaja en el dominio del centro."
    mtNAG(53) = "Las negras tienen una firme ventaja en el dominio del centro."
    mtNAG(54) = "Las blancas tienen una ligera ventaja en el dominio del flanco del rey."
    mtNAG(55) = "Las negras tienen una ligera ventaja en el dominio del flanco del rey."
    mtNAG(56) = "Las blancas tienen una ventaja moderada en dominio del flanco del rey."
    mtNAG(57) = "Las Negras tienen una ventaja moderada en el dominio del flanco del rey."
    mtNAG(58) = "Las blancas tienen una firme ventaja en el dominio del flanco del rey."
    mtNAG(59) = "Las negras tienen una firme ventaja en el dominio del flanco del rey."
    mtNAG(60) = "Las blancas tienen una ligera ventaja en el dominio del flanco de la dama."
    mtNAG(61) = "Las negras tienen una ligera ventaja en el dominio del flanco de la dama."
    mtNAG(62) = "Las blancas tienen una moderada ventaja en el dominio del flanco de la dama."
    mtNAG(63) = "Las negras tienen una moderada ventaja en el dominio del flanco de la dama."
    mtNAG(64) = "Las blancas tienen una firme ventaja en el dominio del flanco de la dama."
    mtNAG(65) = "Las negras tienen una firme ventaja en el dominio del flanco de la dama."
    mtNAG(66) = "Las blancas tienen una vulnerable primera l�nea."
    mtNAG(67) = "Las negras tienen una vulnerable primera l�nea."
    mtNAG(68) = "Las blancas tienen bien protegida la primera l�nea."
    mtNAG(69) = "Las negras tienen bien protegida la primera l�nea."
    mtNAG(70) = "Las blancas tienen d�bilmente protegido a su rey."
    mtNAG(71) = "Las negras tienen d�bilmente protegido a su rey."
    mtNAG(72) = "Las blancas tienen a su rey bien protegido."
    mtNAG(73) = "Las negras tienen a su rey bien protegido."
    mtNAG(74) = "Las blancas tienen a su rey pobremente posicionado."
    mtNAG(75) = "Las negras tienen a su rey pobremente posicionado."
    mtNAG(76) = "Las blancas tienen a su rey bien posicionado."
    mtNAG(77) = "Las negras tienen a su rey bien posicionado."
    mtNAG(78) = "Las blancas tienen una estructura de peones muy d�bil."
    mtNAG(79) = "Las negras tienen una estructura de peones muy d�bil."
    mtNAG(80) = "Las blancas tienen una estructura de peones algo d�bil."
    mtNAG(81) = "Las negras tienen una estructura de peones algo d�bil."
    mtNAG(82) = "Las blancas tienen una estructura de peones algo fuerte."
    mtNAG(83) = "Las negras tienen una estructura de peones algo fuerte."
    mtNAG(84) = "Las blancas tienen una estructura de peones muy fuerte."
    mtNAG(85) = "Las negras tienen una estructura de peones muy fuerte."
    mtNAG(86) = "Las blancas tienen el caballo d�bilmente situado."
    mtNAG(87) = "Las negras tienen el caballo d�bilmente situado."
    mtNAG(88) = "Las blancas tienen el caballo bien situado."
    mtNAG(89) = "Las negras tienen el caballo bien situado."
    mtNAG(90) = "Las blancas tienen el alfil d�bilmente situado."
    mtNAG(91) = "Las negras tienen el alfil d�bilmente situado."
    mtNAG(92) = "Las blancas tienen el alfil bien situado."
    mtNAG(93) = "Las negras tienen el alfil bien situado."
    mtNAG(94) = "Las blancas tienen la torre d�bilmente situada."
    mtNAG(95) = "Las negras tienen la torre d�bilmente situada."
    mtNAG(96) = "Las blancas tienen la torre bien situada."
    mtNAG(97) = "Las negras tienen la torre bien situada."
    mtNAG(98) = "Las blancas tienen la dama d�bilmente situada."
    mtNAG(99) = "Las negras tienen la dama d�bilmente situada."
    mtNAG(100) = "Las blancas tienen la dama bien situada."
    mtNAG(101) = "Las negras tienen la dama bien situada."
    mtNAG(102) = "Las blancas tienen una pobre coordinaci�n de piezas."
    mtNAG(103) = "Las negras tienen una pobre coordinaci�n de piezas."
    mtNAG(104) = "Las blancas tienen una buena coordinaci�n de piezas."
    mtNAG(105) = "Las negras tienen una buena coordinaci�n de piezas."
    mtNAG(106) = "Las blancas han jugado una apertura muy d�bil."
    mtNAG(107) = "Las negras han jugado una apertura muy d�bil."
    mtNAG(108) = "Las blancas han jugado una apertura d�bil."
    mtNAG(109) = "Las negras han jugado una apertura d�bil."
    mtNAG(110) = "Las blancas han jugado una buena apertura."
    mtNAG(111) = "Las negras han jugado una buena apertura."
    mtNAG(112) = "Las blancas han jugado una apertura muy buena."
    mtNAG(113) = "Las negras han jugado una apertura muy buena."
    mtNAG(114) = "Las blancas han jugado un medio juego muy d�bil."
    mtNAG(115) = "Las negras han jugado un medio juego muy d�bil."
    mtNAG(116) = "Las blancas han jugado un medio juego d�bil."
    mtNAG(117) = "Las negras han jugado un medio juego d�bil."
    mtNAG(118) = "Las blancas han jugado un buen medio juego."
    mtNAG(119) = "Las negras han jugado un buen medio juego."
    mtNAG(120) = "Las blancas han jugado muy bien el medio juego."
    mtNAG(121) = "Las negras han jugado muy bien el medio juego."
    mtNAG(122) = "Las blancas han jugado un final muy d�bil."
    mtNAG(123) = "Las negras han jugado un final muy d�bil."
    mtNAG(124) = "Las blancas han jugado un final d�bil."
    mtNAG(125) = "Las negras han jugado un final d�bil."
    mtNAG(126) = "Las blancas han jugado bien el final."
    mtNAG(127) = "Las negras han jugado bien el final."
    mtNAG(128) = "Las blancas han jugado muy bien el final."
    mtNAG(129) = "Las negras han jugado muy bien el final."
    mtNAG(130) = "Las blancas dan ligera contestaci�n."
    mtNAG(131) = "Las negras dan ligera contestaci�n."
    mtNAG(132) = "Las blancas dan moderada contestaci�n."
    mtNAG(133) = "Las negras dan moderada contestaci�n."
    mtNAG(134) = "Las blancas dan firme contestaci�n."
    mtNAG(135) = "Las negras dan firme contestaci�n."
    mtNAG(136) = "Las blancas ejercen moderada presi�n sobre el control de tiempo."
    mtNAG(137) = "Las negras ejercen moderada presi�n sobre el control de tiempo."
    mtNAG(138) = "Las blancas ejercen severa presi�n sobre el control de tiempo."
    mtNAG(139) = "Las negras ejercen severa presi�n sobre el control de tiempo."
End Sub

Public Sub LoadCboFins(cbo As ComboBox)
'*********************************************************************************
' Nombre..............................: LoadCboFins
' Tipo.....................................: Procedure
' Ambito................................: Public
' Par�metros.......................: control ComboBox
' Valor de retorno...............: Ninguno
' Ultimo cambio..................: 17/6/00
' Descripci�n.......................: Llena el control ComboBox que
' ...............................................: se pasa como par�metro con todos
' ...............................................: los tipos de finales almacenados
' ...............................................: en la tabla "Finales".
'*********************************************************************************

    Dim rsFinales As Recordset
    On Error GoTo LoadCboFinsError
    
    Set rsFinales = dbDatos.OpenRecordset("Finales", dbOpenForwardOnly)
    
    cbo.Clear
    
    If Not rsFinales Is Nothing Then
        Do While Not rsFinales.EOF
            With cbo
                .AddItem rsFinales!fi_den
                .ItemData(.NewIndex) = rsFinales!fi_id
            End With
            rsFinales.MoveNext
        Loop
    End If
    rsFinales.Close
    
GetExit:
    Exit Sub
    
LoadCboFinsError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub ListaAperturas(Lst As ListBox, Txt As TextBox)
'**************************************************************************************
' Nombre............................: ListaAperturas
' Tipo...................................: Sub
' �mbito..............................: P�blico
' Par�metros.....................: Control ListBox, Criterio de b�squeda
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 17/6/00
' Descripci�n....................: Muestra la lista de Aperturas que cumplen
'.............................................: los criterios de la ayuda de campo.
'*************************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    Dim strNom As String
    On Error GoTo ListaAperturasError
    
    Clock 1
    strNom = "*" & Trim$(Txt.Text) & "*"
    strSQL = "SELECT * FROM Aperturas WHERE ap_den LIKE " & SnglQuote(strNom)
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs Is Nothing Then
        With Lst
            .Clear
            Do While Not Rs.EOF
                .AddItem Rs!ap_den
                .ItemData(.NewIndex) = Rs!ap_id
                Rs.MoveNext
            Loop
            If Lst.ListCount = 1 Then
                Txt.Text = Lst.List(0)
                Txt.Tag = Lst.ItemData(0)
            ElseIf Lst.ListCount > 1 Then
                .Height = 1620
                .Visible = True
                .SetFocus
            Else
                Txt.Text = ""
            End If
            Rs.Close
        End With
    End If

GetExit:
    Clock 0
    Exit Sub
    
ListaAperturasError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub ListaModalidades(Lst As ListBox, Txt As TextBox)
'**************************************************************************************
' Nombre.............................: ListaModalidades
' Tipo....................................: Sub
' �mbito...............................: P�blico
' Par�metros......................: Control ListBox, Criterio de b�squeda
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 17/6/00
' Descripci�n.....................: Muestra la lista de Modalidades que cumplen
'..............................................: los criterios de la ayuda de campo.
'***************************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    Dim strNom As String
    On Error GoTo ListaModalidadesError
    
    Clock 1
    strNom = "*" & Trim$(Txt.Text) & "*"
    strSQL = "SELECT * FROM Modalidades WHERE mo_den LIKE " & SnglQuote(strNom)
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs Is Nothing Then
        With Lst
            .Clear
            Do While Not Rs.EOF
                .AddItem Rs!mo_den
                .ItemData(.NewIndex) = Rs!mo_id
                Rs.MoveNext
            Loop
            If Lst.ListCount = 1 Then
                Txt.Text = Lst.List(0)
                Txt.Tag = Lst.ItemData(0)
            ElseIf Lst.ListCount > 1 Then
                .Height = 1620
                .Visible = True
                .SetFocus
            Else
                Txt.Text = ""
            End If
            Rs.Close
        End With
    End If

GetExit:
    Clock 0
    Exit Sub
    
ListaModalidadesError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub ListaTorneos(Lst As ListBox, Txt As TextBox)
'****************************************************************************
' Nombre......................: ListaTorneos
' Tipo.............................: Sub
' �mbito........................: P�blico
' Par�metros...............: Control ListBox, Criterio de b�squeda
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 17/6/00
' Descripci�n..............: Muestra la lista de Torneos que cumplen
'.......................................: los criterios de la ayuda de campo.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    Dim strNom As String
    On Error GoTo ListaTorneosError
    
    Clock 1
    strNom = "*" & Trim$(Txt.Text) & "*"
    strSQL = "SELECT * FROM Torneos WHERE to_den LIKE " & SnglQuote(strNom)
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs Is Nothing Then
        With Lst
            .Clear
            Do While Not Rs.EOF
                .AddItem Rs!to_den
                .ItemData(.NewIndex) = Rs!to_id
                Rs.MoveNext
            Loop
            If Lst.ListCount = 1 Then
                Txt.Text = Lst.List(0)
                Txt.Tag = Lst.ItemData(0)
            ElseIf Lst.ListCount > 1 Then
                .Height = 1620
                .Visible = True
                .SetFocus
            Else
                Txt.Text = ""
            End If
            Rs.Close
        End With
    End If

GetExit:
    Clock 0
    Exit Sub
    
ListaTorneosError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub ListaJugadores(Lst As ListBox, Txt As TextBox)
'**************************************************************************************
' Nombre........................: ListaJugadores
' Tipo...............................: Sub
' �mbito..........................: P�blico
' Par�metros.................: Control ListBox, Criterio de b�squeda
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 17/6/00
' Descripci�n................: Muestra la lista con los jugadores que cumplen
'.........................................: los criterios de la ayuda de campo.
'**************************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    Dim strNom As String
    On Error GoTo ListaJugBlError
    
    Clock 1
    strNom = "*" & Trim$(Txt.Text) & "*"
    strSQL = "SELECT * FROM Jugadores WHERE ju_apenom LIKE " & SnglQuote(strNom)
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs Is Nothing Then
        With Lst
            .Clear
            Do While Not Rs.EOF
                .AddItem Rs!ju_apenom
                .ItemData(.NewIndex) = Rs!ju_id
                Rs.MoveNext
            Loop
            If Lst.ListCount = 1 Then
                Txt.Text = Lst.List(0)
                Txt.Tag = Lst.ItemData(0)
            ElseIf Lst.ListCount > 1 Then
                .Height = 1620
                .Visible = True
                .SetFocus
            Else
                Txt.Text = ""
            End If
            Rs.Close
        End With
    End If
    
GetExit:
    Clock 0
    Exit Sub
    
ListaJugBlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub
Public Sub ListaPaises(Lst As ListBox, Txt As TextBox)
'****************************************************************************
' Nombre.....................: ListaPaises
' Tipo............................: Sub
' �mbito.......................: P�blico
' Par�metros..............: Ninguno
' Valor de retorno.....: Ninguno
' �ltimo cambio.........: 18/8/99
' Descripci�n..............: Muestra la lista con
' ......................................: los paises que cumplen
' ......................................: los criterios de la ayuda
' ......................................: de campo de pais.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    Dim strNom As String
    
    Clock 1
    strNom = "*" & Trim$(Txt.Text) & "*"
    strSQL = "SELECT * FROM Paises WHERE pa_den LIKE " & SnglQuote(strNom)
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs Is Nothing Then
        With Lst
            .Clear
            Do While Not Rs.EOF
                .AddItem Rs!pa_den
                .ItemData(.NewIndex) = Rs!pa_id
                Rs.MoveNext
            Loop
            If Lst.ListCount = 1 Then
                Txt.Text = Lst.List(0)
                Txt.Tag = Lst.ItemData(0)
            ElseIf Lst.ListCount > 1 Then
                .Height = 1620
                .Visible = True
                .SetFocus
            Else
                Txt.Text = ""
            End If
            Rs.Close
        End With
    End If
    Clock 0
End Sub

Public Sub TratarError(lngCode As Long)
'****************************************************************************
' Nombre..................: Tratar error
' Tipo.........................: Sub
' �mbito....................: P�blico
' Par�metros...........: C�digo del error (Long)
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 1/7/2000
' Descripci�n..........: Muestra un mensaje espec�fico para el
'...................................: error cuyo c�digo se pasa como par�metro.
'****************************************************************************
    Dim strMensaje As String
    
    Select Case lngCode
        Case 3200
            ' Violaci�n de la integridad referencial
            strMensaje = "Control de la integridad referencial: " & _
                Chr(13) & "Imposible borrar el registro porque contiene " & _
                "elementos relacionados en otras tablas."
    End Select
    
    MsgBox strMensaje, vbExclamation, "Operaci�n cancelada."
End Sub

Public Function FindItem(ctlLst As ListBox, lngKey As Long) As Long
'*******************************************************************
' Nombre.....................: FindItem
' Tipo............................: Function
' �mbito.......................: Privado
' Par�metros.............: Control listbox, Long
' Valor de retorno.....: Long
' �ltimo cambio........: 29/10/99
' Descripci�n............: Busca en los elementos del listbox en su
' .....................................: propiedad 'ItemData' el valor 'lngKey' que se pasa
' ..............................: como par�metro, devolviendo el �ndice si lo encuentra o
' ..............................: -1 si no lo encuentra.
'*******************************************************************
    Dim Idx As Integer
    On Error GoTo FIError
    
    FindItem = -1
    For Idx = 0 To ctlLst.ListCount
        If ctlLst.ItemData(Idx) = lngKey Then
            FindItem = Idx
            Exit Function
        End If
    Next
    
GetExit:
    Exit Function

FIError:
    MsgBox "C�digo" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n" & vbTab & Err.Description & vbCrLf & vbCrLf _
        , vbInformation, "Fuente " & Err.Source
    Resume GetExit

End Function
