VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.MDIForm frmPadre 
   BackColor       =   &H80000004&
   Caption         =   "Sistema Gestor de Base de Datos de Ajedrez."
   ClientHeight    =   4470
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   5730
   HelpContextID   =   1
   Icon            =   "frmPadre.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "frmPadre.frx":030A
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbOpciones 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5730
      _ExtentX        =   10107
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ilsBotones"
      _Version        =   327680
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   21
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botInsertar"
            Object.ToolTipText     =   "A�adir"
            Object.Tag             =   ""
            ImageIndex      =   1
            Object.Width           =   1e-4
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botBuscar"
            Object.ToolTipText     =   "Buscar"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botEliminar"
            Object.ToolTipText     =   "Eliminar"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botGuardar"
            Object.ToolTipText     =   "Guardar"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botCancelar"
            Object.ToolTipText     =   "Cancelar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botAperturas"
            Object.ToolTipText     =   "Aperturas"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botFinales"
            Object.ToolTipText     =   "Finales"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botJugadores"
            Object.ToolTipText     =   "Jugadores"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botModalidades"
            Object.ToolTipText     =   "Modalidades"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botPaises"
            Object.ToolTipText     =   "Paises"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button12 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botPartidas"
            Object.ToolTipText     =   "Partidas"
            Object.Tag             =   ""
            ImageIndex      =   11
         EndProperty
         BeginProperty Button13 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "botTorneos"
            Object.ToolTipText     =   "Torneos"
            Object.Tag             =   ""
            ImageIndex      =   12
         EndProperty
         BeginProperty Button14 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button15 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button16 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "botNuevaBD"
            Object.ToolTipText     =   "Nueva Base de Datos"
            Object.Tag             =   ""
            ImageIndex      =   13
         EndProperty
         BeginProperty Button17 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "botAbrirBD"
            Object.ToolTipText     =   "Abrir Base de Datos"
            Object.Tag             =   ""
            ImageIndex      =   14
         EndProperty
         BeginProperty Button18 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button19 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button20 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button21 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "botAyuda"
            Object.ToolTipText     =   "Ayuda"
            Object.Tag             =   ""
            ImageIndex      =   15
         EndProperty
      EndProperty
      BorderStyle     =   1
      MouseIcon       =   "frmPadre.frx":67844
   End
   Begin MSComDlg.CommonDialog dlgPadre 
      Index           =   0
      Left            =   780
      Top             =   2820
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "bda"
      Filter          =   "Bases de datos de ajedrez|*.bda"
      FilterIndex     =   1
      Flags           =   4
   End
   Begin MSComDlg.CommonDialog dlgPadre 
      Index           =   1
      Left            =   1380
      Top             =   2820
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      FilterIndex     =   1
      Flags           =   4
      HelpFile        =   "SGBDA Help"
   End
   Begin ComctlLib.ImageList ilsBotones 
      Left            =   120
      Top             =   2760
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327680
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   15
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":67860
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":67B7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":67E94
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":681AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":684C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":687E2
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":68AFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":68E16
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":69130
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":6944A
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":69764
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":69A7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":69D98
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":6A0B2
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPadre.frx":6A3CC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArchivo 
      Caption         =   "&Archivo"
      Begin VB.Menu mnuAbrirDB 
         Caption         =   "&Abrir Base de Datos"
      End
      Begin VB.Menu mnuCerrarDB 
         Caption         =   "&Cerrar Base de Datos"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuNuevaDB 
         Caption         =   "&Nueva Base de Datos"
      End
      Begin VB.Menu mnuSeparador 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "&Salir"
      End
   End
   Begin VB.Menu mnuMant 
      Caption         =   "&Mantenimientos"
      Begin VB.Menu mnuApes 
         Caption         =   "&Aperturas"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFins 
         Caption         =   "&Finales"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuJugs 
         Caption         =   "&Jugadores"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuModals 
         Caption         =   "&Modalidades"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuPaises 
         Caption         =   "Pa&ises"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuPt 
         Caption         =   "&Partidas"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuTors 
         Caption         =   "&Torneos"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuReg 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegAlta 
         Caption         =   "&A�adir"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRegBuscar 
         Caption         =   "&Buscar"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRegEliminar 
         Caption         =   "&Eliminar"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRegGuardar 
         Caption         =   "&Guardar"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRegCancelar 
         Caption         =   "&Cancelar"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Herramientas"
      Begin VB.Menu mnuCompactDB 
         Caption         =   "&Compactar Base de Datos"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRepairDB 
         Caption         =   "&Reparar Base de Datos"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuPassword 
         Caption         =   "C&ontrase�a"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuExport 
         Caption         =   "&Exportar partidas"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuVent 
      Caption         =   "&Ventana"
      Begin VB.Menu mnuVentCas 
         Caption         =   "&Cascada"
      End
      Begin VB.Menu mnuVentHor 
         Caption         =   "Mosaico &Horizontal"
      End
      Begin VB.Menu mnuVentVert 
         Caption         =   "Mosaico &Vertical"
      End
      Begin VB.Menu mnuVentOrg 
         Caption         =   "&Organizar Iconos"
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "Ay&uda"
      Begin VB.Menu mnuTemAyuda 
         Caption         =   "&Ayuda"
      End
      Begin VB.Menu mnuAcerca 
         Caption         =   "A&cerca de SGBD Ajedrez"
      End
   End
End
Attribute VB_Name = "frmPadre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()
    InicioForm
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    Set mobjControlMDI = Nothing
    Set dbDatos = Nothing
End Sub

Private Sub mnuAbrirDB_Click()
    Dim strBaseDatos As String
    strBaseDatos = SeleccionarBD()
    AbrirBaseDatos strBaseDatos
End Sub

Private Sub mnuAcerca_Click()
    frmAbout.Show vbModal
End Sub

Private Sub mnuApes_Click()
    ShowForm frmAperturas
End Sub

Private Sub mnuCerrarDB_Click()
    CerrarBaseDatos
End Sub

Private Sub mnuCompactDB_Click()
    CompactaBD
End Sub

Private Sub mnuExport_Click()
    With frmExportar
        .Show
        .ZOrder 0
    End With
End Sub

Private Sub mnuFins_Click()
    ShowForm frmFinales
End Sub

Private Sub mnuJugs_Click()
    ShowForm frmJugadores
End Sub

Private Sub mnuModals_Click()
    ShowForm frmModalidades
End Sub

Private Sub mnuNuevaDB_Click()
    NuevaBD
End Sub

Private Sub mnuPaises_Click()
    ShowForm frmPaises
End Sub

Private Sub mnuPassword_Click()
    frmChangePaso.Show vbModal
End Sub

Private Sub mnuPt_Click()
    ShowForm frmPartidas
End Sub

Private Sub mnuRegAlta_Click()
    ActiveForm.Insertar
End Sub

Private Sub mnuRegBuscar_Click()
    ActiveForm.Buscar
End Sub

Private Sub mnuRegCancelar_Click()
    ActiveForm.Cancelar
End Sub

Private Sub mnuRegEliminar_Click()
    ActiveForm.Borrar
End Sub

Private Sub mnuRegGuardar_Click()
    ActiveForm.Grabar
End Sub

Private Sub mnuRepairDB_Click()
    ' Repara la Base de Datos
    ReparaBD
End Sub

Private Sub mnuSalir_Click()
'    Unload Me
    SalirAplicacion
End Sub

Private Sub mnuTemAyuda_Click()
    ShowAyuda
End Sub

Private Sub mnuTors_Click()
    ShowForm frmTorneos
End Sub

Private Sub mnuVentCas_Click()
    frmPadre.Arrange vbCascade
End Sub

Private Sub mnuVentHor_Click()
    frmPadre.Arrange vbTileHorizontal
End Sub

Private Sub mnuVentOrg_Click()
    frmPadre.Arrange vbArrangeIcons
End Sub

Private Sub mnuVentVert_Click()
    frmPadre.Arrange vbTileVertical
End Sub

Private Sub tlbOpciones_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Key
    
        Case "botInsertar"
            ActiveForm.Insertar
            
        Case "botBuscar"
            ActiveForm.Buscar
            
        Case "botEliminar"
            ActiveForm.Borrar
            
        Case "botGuardar"
            ActiveForm.Grabar
            
        Case "botCancelar"
            ActiveForm.Cancelar
            
        Case "botAyuda"
            ShowAyuda
            
        Case "botAperturas"
            ShowForm frmAperturas
            
        Case "botFinales"
            ShowForm frmFinales
            
        Case "botJugadores"
            ShowForm frmJugadores
            
        Case "botModalidades"
            ShowForm frmModalidades
            
        Case "botPaises"
            ShowForm frmPaises
            
        Case "botPartidas"
            ShowForm frmPartidas
            
        Case "botTorneos"
            ShowForm frmTorneos
            
        Case "botNuevaBD"
            NuevaBD
            
        Case "botAbrirBD"
            Dim strBaseDatos As String
            strBaseDatos = SeleccionarBD()
            AbrirBaseDatos strBaseDatos
        
    End Select
End Sub

Private Sub AbrirBaseDatos(ByVal strDB As String)
'************************************************************************************
' Nombre.............................: AbrirBaseDatos
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: String -> Nombre de la base de datos.
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Abre la base de datos cuyo nombre se pasa
'..............................................: como par�metro y habilita los botones para
' .............................................: hacer posible su manejo.
'***********************************************************************************

    On Error GoTo ControlError
    
    Clock 1
    If strDB <> "" Then
        ' Se selecciona BBDD.
        ' Se abre en modo exclusivo.
        DoEvents
        Set dbDatos = OpenDatabase(strDB, True)
        strPassword = ""
        Me.Caption = CT_NOMBRE_APP & " [" & _
                    strDB & "]"
        mobjControlMDI.Mantenimientos_On
        MsgBox "El fichero '" & strDB & _
            "' ha sido abierto con �xito.", vbInformation, _
            "Abrir fichero"
    End If
    
    
GetExit:
    Clock 0
    Exit Sub
    
ControlError:
    If Err.Number = 3031 Then
        frmPaso.NombreDB = strDB
        frmPaso.Show vbModal
    ElseIf Err.Number = 3051 Then
        MsgBox "Imposible abrir el fichero '" & _
            strDB & "'. Posee el atributo de SOLO LECTURA." & _
            " Desmarque dicho atributo para proceder a la apertura del fichero.", vbCritical, _
            "Abrir base de datos."
    End If
    Resume GetExit
End Sub

Public Sub SalirAplicacion()
'************************************************************************************
' Nombre.............................: SalirAplicacion
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Termina la ejecuci�n de la aplicaci�n cerrando
'..............................................: todos y cada uno de los formularios abiertos.
'***********************************************************************************

    Dim frmOpen As Form
    
    For Each frmOpen In Forms
        Unload frmOpen
        Set frmOpen = Nothing
    Next
    End
End Sub

Public Sub CerrarBaseDatos()
'************************************************************************************
' Nombre.............................: CerrarBaseDatos
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Cierra la base de datos actualmente en uso
'..............................................: y adem�s deshabilita los controles de opera-
' .............................................: ciones para evitar posibles errores.
'***********************************************************************************

    Dim frmOpen As Form
    
    Set dbDatos = Nothing
    Me.Caption = CT_NOMBRE_APP
    mobjControlMDI.Mantenimientos_Off
    For Each frmOpen In Forms
        If Not frmOpen.Name = "frmPadre" Then
            Unload frmOpen
            Set frmOpen = Nothing
        End If
    Next
End Sub

Private Sub ReparaBD()
'************************************************************************************
' Nombre.............................: ReparaBD
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Repara la base de datos actualmente en uso.
'..............................................: Antes de proceder a la reparaci�n hay que
' .............................................: cerrarla y, una vez reparada, volverla a abrir.
'***********************************************************************************

    Dim strBase As String
    Dim ErrBucle As Error

    If MsgBox("�Desea reparar la base de datos " & _
            dbDatos.Name & "?", vbYesNo + vbMsgBoxHelpButton, _
            , CT_HELP_FILE, CT_HELP_REPARAR) = vbYes Then
        On Error GoTo Err_Reparar
    
        strBase = dbDatos.Name
        ' Antes de reparar la BBDD,
        ' debemos cerrarla
        dbDatos.Close
        DBEngine.RepairDatabase strBase
        On Error GoTo 0
        MsgBox "�Fin del procedimiento reparar!"
        ' Una vez reparada, la volvemos a abrir
        Set dbDatos = OpenDatabase(strBase, True, False, ";PWD=" & strPassword)
    End If

    Exit Sub

Err_Reparar:

'    For Each ErrBucle In DBEngine.Errors
        MsgBox "�Fall� Repair!" & vbCr & _
            "N�mero de error: " & Err.Number & _
            vbCr & Err.Description

'    Next ErrBucle

End Sub

Private Sub CompactaBD()
'********************************************************************************************
' Nombre.............................: CompactaBD
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Compacta la base de datos actualmente en uso.
'..............................................: Antes de proceder hay que cerrarla y posteriormente
' .............................................: volverla a abrir una vez finalizada la compactaci�n.
'********************************************************************************************

    ' Compacta la Base de Datos
    ' DBEngine.CompactDatabase dbDatos.Name, dbDatos.Name
    Dim strOldName As String
    Dim strNewName As String
    Dim ErrBucle As Error
    
    strNewName = "DATA_TMP.mdb"
    If MsgBox("�Desea compactar la base de datos " & _
            dbDatos.Name & "?", vbYesNo + vbMsgBoxHelpButton, _
            , CT_HELP_FILE, CT_HELP_COMPACTAR) = vbYes Then
        On Error GoTo Err_Compactar
        strOldName = dbDatos.Name
        ' Antes de compactar la BBDD,
        ' debemos cerrarla
        dbDatos.Close
        If Dir(strNewName) <> "" Then Kill strNewName
        DBEngine.CompactDatabase strOldName, strNewName, , , _
                ";PWD=" & strPassword
        If Dir(strOldName) <> "" Then Kill strOldName
        DBEngine.CompactDatabase strNewName, strOldName, , , _
                ";PWD=" & strPassword
        If Dir(strNewName) <> "" Then Kill strNewName
        On Error GoTo 0
        MsgBox "�Fin del procedimiento compactar!"
        ' Una vez reparada, la volvemos a abrir
        Set dbDatos = OpenDatabase(strOldName, True, False, ";PWD=" & strPassword)
    End If

    Exit Sub

Err_Compactar:

'    For Each ErrBucle In DBEngine.Errors
        MsgBox "�Fall� Repair!" & vbCr & _
            "N�mero de error: " & Err.Number & _
            vbCr & Err.Description, vbCritical

'    Next ErrBucle

End Sub

Private Sub NuevaBD()
'************************************************************************************
' Nombre.............................: NuevaBD
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Crea una nueva base de datos.
'***********************************************************************************

    ' Abre una nueva Base de Datos
    Dim strSQL As String
    Dim strNombre As String
    Dim ErrBucle As Error
    On Error GoTo Err_NuevaBD
    
    ' Cerramos la anterior.
'    Set dbDatos = Nothing
    CerrarBaseDatos
    
    dlgPadre(0).filename = "Sin T�tulo"
    dlgPadre(0).DialogTitle = "Grabar Base de Datos como..."
    dlgPadre(0).ShowSave
    strNombre = dlgPadre(0).filename
    If Not Trim$(strNombre) = "" Then
        Set dbDatos = DBEngine.CreateDatabase(strNombre, dbLangGeneral)
        With dbDatos
            ' Crea la tabla de Aperturas
            strSQL = "CREATE TABLE Aperturas ( " & _
                "ap_id COUNTER NOT NULL, " & _
                "ap_codigo TEXT(10), " & _
                "ap_den TEXT(50) NOT NULL, " & _
                "ap_tipo TEXT(1), " & _
                "ap_info LONGTEXT, " & _
                "CONSTRAINT ap_id PRIMARY KEY (ap_id))"
            .Execute strSQL
            
            ' Crea la tabla de Finales
            strSQL = "CREATE TABLE Finales ( " & _
                "fi_id COUNTER NOT NULL, " & _
                "fi_den TEXT(50) NOT NULL, " & _
                "CONSTRAINT fi_id PRIMARY KEY (fi_id))"
            .Execute strSQL
            
            ' Crea la tabla de Jugadores
            strSQL = "CREATE TABLE Jugadores ( " & _
                "ju_id COUNTER NOT NULL, " & _
                "ju_apenom TEXT(50) NOT NULL, " & _
                "ju_tipo TEXT(1) NOT NULL, " & _
                "ju_pais LONG, " & _
                "ju_titulo TEXT(20), " & _
                "ju_palmares LONGTEXT, " & _
                "ju_fnac DATETIME, " & _
                "ju_dir TEXT(35), " & _
                "ju_pob TEXT(20), " & _
                "ju_email TEXT, " & _
                "ju_club TEXT(20), " & _
                "ju_comen LONGTEXT, " & _
                "ju_tlf TEXT(12), " & _
                "ju_elo SHORT, " & _
                "CONSTRAINT ju_id PRIMARY KEY (ju_id))"
            .Execute strSQL
            
            ' Crea la tabla de Modalidades
            strSQL = "CREATE TABLE Modalidades ( " & _
                "mo_id COUNTER NOT NULL, " & _
                "mo_den TEXT(50) NOT NULL, " & _
                "CONSTRAINT mo_id PRIMARY KEY (mo_id))"
            .Execute strSQL
            
            ' Crea la tabla de Movimientos
            strSQL = "CREATE TABLE Movimientos ( " & _
                "mv_partida LONG NOT NULL, " & _
                "mv_nmov SHORT NOT NULL, " & _
                "mv_bando BYTE NOT NULL, " & _
                "mv_orig BYTE NOT NULL, " & _
                "mv_dest BYTE NOT NULL, " & _
                "mv_coment LONGTEXT, " & _
                "CONSTRAINT Movimientos_PK PRIMARY KEY (mv_partida, mv_nmov, mv_bando))"
            .Execute strSQL

            
            ' Crea la tabla Paises
            strSQL = "CREATE TABLE Paises ( " & _
                "pa_id COUNTER NOT NULL, " & _
                "pa_den TEXT(25) NOT NULL, " & _
                "pa_flag LONGBINARY, " & _
                "CONSTRAINT pa_id PRIMARY KEY (pa_id))"
            .Execute strSQL
            
            ' Crea la tabla Partidas
            strSQL = "CREATE TABLE Partidas ( " & _
                "pt_id COUNTER NOT NULL, " & _
                "pt_blancas LONG NOT NULL, " & _
                "pt_negras LONG NOT NULL, " & _
                "pt_torneo LONG, " & _
                "pt_lugar TEXT(20), " & _
                "pt_fecha DATETIME, " & _
                "pt_resultado BYTE NOT NULL, " & _
                "pt_elobl SHORT, " & _
                "pt_elong SHORT, " & _
                "pt_apertura LONG, " & _
                "pt_anotador TEXT(35), " & _
                "pt_tipofin LONG, " & _
                "pt_modalidad LONG, " & _
                "pt_notas LONGTEXT, " & _
                "pt_ronda BYTE, " & _
                "CONSTRAINT pt_id PRIMARY KEY (pt_id))"
            .Execute strSQL
            
            ' Crea la tabla Torneos
            strSQL = "CREATE TABLE Torneos ( " & _
                "to_id COUNTER NOT NULL, " & _
                "to_fec DATETIME, " & _
                "to_den TEXT(65) NOT NULL, " & _
                "to_loc TEXT(20), " & _
                "to_org TEXT(20), " & _
                "to_open BIT, " & _
                "to_pais LONG, " & _
                "to_comen LONGTEXT, " & _
                "CONSTRAINT to_id PRIMARY KEY (to_id))"
                
            .Execute strSQL
            
            ' Crea la clave ajena Pais en la tabla Jugadores
            strSQL = "ALTER TABLE Jugadores " & _
                "ADD CONSTRAINT relPaises " & _
                "FOREIGN KEY(ju_pais) " & _
                "REFERENCES Paises(pa_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Partida en la tabla Movimentos
'            strSQL = "ALTER TABLE Movimientos " & _
'                "ADD CONSTRAINT relPartidas " & _
'                "FOREIGN KEY(mv_partida) " & _
'                "REFERENCES Partidas(pt_id)"
'            .Execute strSQL
            
            ' Definimos la restricci�n de borrado en cascada
            Dim relPartidas As Relation
            Set relPartidas = .CreateRelation _
            ("relPartidas", "Partidas", "Movimientos", dbRelationDeleteCascade)
            
            ' Crea el objeto Field para la colecci�n Fields
            ' del objeto Relation nuevo. Establece las
            ' propiedades Name y ForeignName basadas en los
            ' campos que se van a utilizar en la relaci�n.
            relPartidas.Fields.Append relPartidas.CreateField("pt_id")
            relPartidas.Fields!pt_id.ForeignName = "mv_partida"
            .Relations.Append relPartidas
            
            
            ' Crea la clave ajena Apertura en la tabla Partidas
            strSQL = "ALTER TABLE Partidas " & _
                "ADD CONSTRAINT relApert " & _
                "FOREIGN KEY(pt_apertura) " & _
                "REFERENCES Aperturas(ap_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Jugador Blanco en la tabla Partidas
            strSQL = "ALTER TABLE Partidas " & _
                "ADD CONSTRAINT relJugBl " & _
                "FOREIGN KEY(pt_blancas) " & _
                "REFERENCES Jugadores(ju_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Jugador Negro en la tabla Partidas
            strSQL = "ALTER TABLE Partidas " & _
                "ADD CONSTRAINT relJugNg " & _
                "FOREIGN KEY(pt_negras) " & _
                "REFERENCES Jugadores(ju_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Modalidad en la tabla Partidas
            strSQL = "ALTER TABLE Partidas " & _
                "ADD CONSTRAINT relModal " & _
                "FOREIGN KEY(pt_modalidad) " & _
                "REFERENCES Modalidades(mo_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Final en la tabla Partidas
            strSQL = "ALTER TABLE Partidas " & _
                "ADD CONSTRAINT relTipoFin " & _
                "FOREIGN KEY(pt_tipofin) " & _
                "REFERENCES Finales(fi_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Torneo en la tabla Partidas
            strSQL = "ALTER TABLE Partidas " & _
                "ADD CONSTRAINT relTor " & _
                "FOREIGN KEY(pt_torneo) " & _
                "REFERENCES Torneos(to_id)"
            .Execute strSQL
            
            ' Crea la clave ajena Pais en la tabla Torneos
            strSQL = "ALTER TABLE Torneos " & _
                "ADD CONSTRAINT relPais " & _
                "FOREIGN KEY(to_pais) " & _
                "REFERENCES Paises(pa_id)"
            .Execute strSQL
            
            ' Crea la clave alternativa c�digo de apertura
            ' en la tabla Aperturas
            strSQL = "CREATE INDEX ap_codigo_AK " & _
                "ON Aperturas(ap_codigo)"
            .Execute strSQL
            
            strPassword = ""
            Me.Caption = CT_NOMBRE_APP & " [" & _
                        strNombre & "]"
            mobjControlMDI.Mantenimientos_On
        End With
    Else
        MsgBox "Debe grabar la nueva base de datos con un nombre.", _
            vbCritical, "Nueva base de datos"
    End If
        
    Exit Sub
    
Err_NuevaBD:
    If Err.Number = 32755 Then
        Exit Sub
    Else
'        For Each ErrBucle In DBEngine.Errors
            MsgBox "�Error!" & vbCr & _
                "N�mero de error: " & Err.Number & _
                vbCr & Err.Description
'        Next ErrBucle
    End If
    
End Sub

Private Function SeleccionarBD() As String
'**************************************************************************************************
' Nombre.............................: SeleccionarBD
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: String -> Nombre de la base de datos
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Abre un cuadro de di�logo com�n para posibilitar al
'..............................................: usuario elegir el fichero de base de datos para abrirla.
'**************************************************************************************************

    On Error GoTo SinBD
    
    With dlgPadre(0)
        .CancelError = True
        .DialogTitle = "Abrir Base de Datos"
        .ShowOpen
        SeleccionarBD = .filename
    End With
    
GetExit:
    Exit Function
    
SinBD:
    If Err.Number = 32755 Then
        Exit Function
    Else
        MsgBox "Error desconocido al abrir el archivo " & _
            SeleccionarBD
        SeleccionarBD = ""
        Exit Function
    End If
End Function

Public Sub MuestraAyuda(lngContext As Long)
'**********************************************************************************************
' Nombre.............................: MuestraAyuda
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Long -> Identificador del contexto de la ayuda.
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra el tema de ayuda correspondiente al
'..............................................: identificador que se pasa como par�metro.
'**********************************************************************************************

    With dlgPadre(1)
        .HelpFile = "SGBDA Help.hlp"
        .HelpCommand = cdlHelpContext
        .HelpContext = lngContext
        .ShowHelp
    End With

End Sub

Private Sub InicioForm()
'***************************************************************************************
' Nombre..........................: InicioForm
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio..............: 3/8/99
' Descripci�n...................: Crea objetos y configura el inicio del formulario.
'**************************************************************************************

    If App.PrevInstance = True Then
        MsgBox "Ya hay en ejecuci�n una instancia " & _
            "de la aplicaci�n.", vbInformation, _
            "Sistema Gestor de Base de Datos de Ajedrez."
        End
    End If
    frmSplash.SplashOn frmPadre, 4000

'    AbrirBaseDatos
    frmAbiertos = 0
    Top = 0
    Left = 0
    frmSplash.SplashOff
    
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_PRINCIPAL
    
    'Cargamos la tabla de etiquetas NAG.
    LoadNAG

    DoEvents
    
    ' Si se pasa un fichero como par�metro
    ' lo abrimos.
    ' Esto ocurre cuando entramos en la
    ' aplicaci�n a trav�s de un fichero
    ' relacionado.
    Dim strParametro As String
    strParametro = Command$
    If Len(strParametro) > 2 Then
        ' Eliminamos las comillas
        strParametro = Mid$(strParametro, 2, Len(strParametro) - 2)
    End If
    AbrirBaseDatos strParametro

End Sub

Private Sub ShowAyuda()
    If Not ActiveForm Is Nothing Then
        MuestraAyuda ActiveForm.HelpContextID
    Else
        MuestraAyuda HelpContextID
    End If
End Sub

Private Sub ShowForm(frmTarget As Form)
'***************************************************************************************
' Nombre..........................: ShowForm
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Forumulario
' Valor de retorno..........: Ninguno
' �ltimo cambio..............: 16/7/00
' Descripci�n...................: Muestra en primer plano el formulario
' ...........................................: que se pasa como par�metro.
'**************************************************************************************

    With frmTarget
        .Show
        .ZOrder 0
        .WindowState = vbNormal
    End With
End Sub
