VERSION 5.00
Begin VB.Form frmChangePaso 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Establecer/Cambiar Contrase�a"
   ClientHeight    =   3915
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3915
   ScaleWidth      =   4710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCh 
      Cancel          =   -1  'True
      Caption         =   "A&yuda"
      Height          =   375
      Index           =   2
      Left            =   3000
      TabIndex        =   10
      Top             =   3360
      Width           =   1215
   End
   Begin VB.CommandButton cmdCh 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Index           =   0
      Left            =   420
      TabIndex        =   3
      Top             =   3360
      Width           =   1215
   End
   Begin VB.CommandButton cmdCh 
      Caption         =   "&Cancelar"
      Height          =   375
      Index           =   1
      Left            =   1710
      TabIndex        =   4
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Frame fraCh 
      Height          =   2235
      Left            =   120
      TabIndex        =   5
      Top             =   900
      Width           =   4455
      Begin VB.TextBox txtCh 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Index           =   2
         Left            =   1140
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   1680
         Width           =   2835
      End
      Begin VB.TextBox txtCh 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Index           =   0
         Left            =   1140
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   480
         Width           =   2835
      End
      Begin VB.TextBox txtCh 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Index           =   1
         Left            =   1140
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1080
         Width           =   2835
      End
      Begin VB.Label lblCh 
         Caption         =   "Confirme la contrase�a nueva"
         Height          =   195
         Index           =   2
         Left            =   1140
         TabIndex        =   8
         Top             =   1500
         Width           =   2295
      End
      Begin VB.Label lblCh 
         Caption         =   "Contrase�a nueva"
         Height          =   195
         Index           =   1
         Left            =   1140
         TabIndex        =   7
         Top             =   900
         Width           =   1395
      End
      Begin VB.Label lblCh 
         Caption         =   "Contrase�a actual"
         Height          =   195
         Index           =   0
         Left            =   1140
         TabIndex        =   6
         Top             =   300
         Width           =   1395
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   180
         Picture         =   "frmChangePaso.frx":0000
         Top             =   240
         Width           =   480
      End
   End
   Begin VB.Label lblCh 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   435
      Index           =   3
      Left            =   180
      TabIndex        =   9
      Top             =   240
      Width           =   4395
   End
End
Attribute VB_Name = "frmChangePaso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCh_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Aceptar
            ' Comprobamos la contrase�a actual
            If Not txtCh(0) = strPassword Then
                MsgBox "Contrase�a actual incorrecta.", vbExclamation, _
                        "Establecer/Cambiar contrase�a"
                txtCh(0).SetFocus
                Exit Sub
            End If
            ' Comprobamos la coherencia de
            ' la nueva contrase�a.
            If Not txtCh(1) = txtCh(2) Then
                MsgBox "Error al confirmar la nueva contrase�a.", _
                    vbExclamation, "Establecer/Cambiar contrase�a"
                Exit Sub
                txtCh(1).SetFocus
            End If
            dbDatos.NewPassword strPassword, txtCh(1)
            strPassword = txtCh(1)
            MsgBox "La contrase�a ha sido cambiada con �xito.", _
                    vbInformation, _
                    "Establecer/Cambiar contrase�a"
            Unload Me
        Case 1
            ' Cancelar
            Unload Me
        Case 2
            ' Ayuda
            frmPadre.MuestraAyuda HelpContextID
    End Select
End Sub

Private Sub Form_Load()
    ' Mostramos el nombre de la aplicaci�n.
    
    lblCh(3) = dbDatos.Name
    
    HelpContextID = CT_HELP_PASSWORD
End Sub

Private Sub txtCh_GotFocus(Index As Integer)
    txtCh(Index).BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtCh_LostFocus(Index As Integer)
    txtCh(Index).BackColor = CT_COLOR_BLANCO
End Sub
