VERSION 5.00
Begin VB.Form frmFinales 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Finales."
   ClientHeight    =   3375
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6930
   Icon            =   "frmFinales.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3375
   ScaleWidth      =   6930
   Tag             =   "Finales."
   Begin VB.CommandButton cmdBuscar 
      Default         =   -1  'True
      Height          =   780
      Left            =   6000
      Picture         =   "frmFinales.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   180
      Width           =   795
   End
   Begin VB.Frame Frame2 
      Caption         =   "Finales"
      Height          =   2310
      Left            =   150
      TabIndex        =   4
      Top             =   960
      Width           =   6645
      Begin VB.ListBox lstFins 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1980
         Left            =   150
         TabIndex        =   2
         Top             =   225
         Width           =   5985
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de final"
      Height          =   690
      Left            =   150
      TabIndex        =   3
      Top             =   150
      Width           =   5745
      Begin VB.TextBox txtFin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   150
         TabIndex        =   0
         Top             =   225
         Width           =   5460
      End
   End
End
Attribute VB_Name = "frmFinales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strDen As String
    ' Guarda la denominaci�n del �ltimo tipo de modalidad
    ' buscado. Se utiliza para refrescar la lista despu�s
    ' de las modificaciones.
Private RsFin   As Recordset

Public Estado As String
Private ReLoad As Boolean

' Variable para controlar cambios
Private blnChange As Boolean

Private Sub InicioForm()
'**************************************************************************************
' Nombre.........................: InicioForm
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 5/8/99
' Descripci�n.................: Crea objetos y configura el inicio del formulario.
'**************************************************************************************
    ' Dimensionamos el formulario.
    Width = 7020
    Height = 3750

    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_FINALES
    
    ReLoad = False
    frmAbiertos = frmAbiertos + 1
    DisableAllFields
End Sub

Private Sub cmdBuscar_Click()
    Dim strSQL As String

    Clock 1
    strDen = "*" & Trim$(txtFin) & "*"
    If Not (Trim$(txtFin) = "") Then
        ' Buscar por la denominaci�n
        strSQL = "SELECT * FROM finales WHERE fi_den LIKE " & _
            SnglQuote(strDen) & " ORDER BY fi_den"
    Else
        ' Buscar todos
        strSQL = "finales"
    End If
    Set RsFin = dbDatos.OpenRecordset(strSQL)
    If Not RsFin.EOF Then
        DisplayRegs RsFin
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    Clock 0
End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
End Sub

Private Sub FinForm()
'****************************************************************************
' Nombre.......................: FinForm
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 5/8/99
' Descripci�n...............: Pide confirmaci�n de grabaci�n de
' .......................................: cambios ydestruye los objetos.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    Set RsFin = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub DisableAllFields()
'****************************************************************************
' Nombre........................: DisableAllFields
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 5/8/99
' Descripci�n................: Deshabilita los campos del formulario.
'****************************************************************************

    DesActivarTxt txtFin
    DesActivarLstBox lstFins
    cmdBuscar.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub SetSearchControls()
'****************************************************************************
' Nombre.........................: SetSearchControls
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 5/8/99
' Descripci�n.................: Habilita los campos del formulario para que
'..........................................: el usuario pueda hacer b�squedas.
'****************************************************************************
    
    ActivarTxt txtFin
    cmdBuscar.Enabled = True
    DesActivarLstBox lstFins
    txtFin.SetFocus
End Sub

Public Sub Buscar()
    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub DisplayFields()
'*****************************************************************************************
' Nombre.......................: DisplayFields
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 5/8/99
' Descripci�n...............: Muestra en el formulario los datos del registro le�do.
'****************************************************************************************

    ClearFields
    txtFin = CheckForNull(RsFin!fi_den, "STRING")
    blnChange = False
End Sub

Private Sub ClearFields()
'*********************************************************************************************
' Nombre.............................: ClearFields
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Limpia el contenido de los controles del formulario.
'*********************************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    
    txtFin = ""
    txtFin.Tag = ""
    blnChange = False
End Sub

Private Sub SetEditControls()
'****************************************************************************
' Nombre........................: SetEditControls
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 5/8/99
' Descripci�n................: Habilita los controles necesarios para
'.........................................: operaciones de busqueda y alta.
'****************************************************************************

    ActivarTxt txtFin
    txtFin.SetFocus
    cmdBuscar.Enabled = False

End Sub

Public Sub Cancelar()
'******************************************************************************************
' Nombre..........................: Cancelar
' Tipo.................................: Sub
' �mbito............................: P�blico
' Par�metros...................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio.............: 4/8/99
' Descripci�n..................: Cancela la acci�n que se estuviera realizando
' ..........................................: y vuelve al estado de espera.
'******************************************************************************************
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
End Sub

Public Sub Borrar()
'****************************************************************************
' Nombre...............................: Borrar
' Tipo......................................: Sub
' �mbito.................................: P�blico
' Par�metros........................: Ninguno
' Valor de retorno...............: Ninguno
' �ltimo cambio..................: 5/8/99
' Descripci�n.......................: Da de baja un registro.
'****************************************************************************
    Dim Respuesta   As Integer

    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
End Sub

Public Sub Insertar()
    mobjControlMDI.Alta
    SetEditControls
    ClearFields

End Sub

Public Sub Grabar()
'**********************************************************************************************
' Nombre........................: Grabar
' Tipo...............................: Sub
' �mbito..........................: P�blico
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........:5/8/99
' Descripci�n................: Graba un reg. en la BB.DD. con los datos introducidos.
'*********************************************************************************************

    ' Si el objeto RsFin a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If RsFin Is Nothing Then _
        Set RsFin = dbDatos.OpenRecordset("finales", dbOpenDynaset)
    If Not DatosValidos() Then Exit Sub
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            RsFin.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            RsFin.Edit
    End Select

    AsignarValores
    RsFin.Update
    If Estado = "EDICION" Then
        Dim lngItem As Long
        With lstFins
            lngItem = FindItem(lstFins, RsFin!fi_id)
            If lngItem <> -1 Then
                .RemoveItem lngItem
            End If
        End With
'        If LCase(RsFin!fi_den) Like LCase(strDen) Then
            ' Si la modificaci�n no afecta al criterio de b�squeda,
            ' lo mantenemos en la lista
            With lstFins
                .AddItem RsFin!fi_den
                .ItemData(.NewIndex) = RsFin!fi_id
            End With
'        End If
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    ClearFields
    SetSelectControls
    mobjControlMDI.Espera
End Sub

Private Sub AsignarValores()
'************************************************************************************
' Nombre.............................: AsignarValores
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Asigna los valores que el usuario
'..............................................: ha introducido en el formulario al Recordset.
'***********************************************************************************
    RsFin!fi_den = txtFin
End Sub

Private Sub lstFins_Click()
    ' Mostrar el registro seleccionado en la lista.
    Dim lngKey As Long

    Clock 1
    lngKey = lstFins.ItemData(lstFins.ListIndex)
    CheckChanges blnChange
    If RsFin.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        RsFin.Index = "fi_id"
        RsFin.Seek "=", lngKey
    Else
        ' Utilizamos el m�todo FIND si el Recordset es DYNASET o SNAPSHOT
        RsFin.FindFirst "fi_id =" & lngKey
    End If
    SetEditControls
    mobjControlMDI.Edicion
    DisplayFields
    Clock 0

End Sub

Private Sub txtFin_Change()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub txtFin_GotFocus()
    txtFin.BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtFin_LostFocus()
    txtFin = Trim$(txtFin)
    txtFin.BackColor = CT_COLOR_BLANCO
End Sub

Private Sub DisplayRegs(Rs As Recordset)
'******************************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra en la lista los registros seleccionados
'..............................................: en el proceso de b�squeda y que est�n alma-
' .............................................: cenados en el Recordset que se pasa como
' .............................................: par�metro.
'***************************************************************************************

    ActivarLstBox lstFins
    lstFins.Clear
    Do While Not Rs.EOF
        With lstFins
            .AddItem CheckForNull(RsFin!fi_den, "STRING")
            .ItemData(.NewIndex) = CheckForNull(RsFin!fi_id, "LONG")
        End With
        Rs.MoveNext
    Loop
End Sub

Private Sub Delete()
'************************************************************************************
' Nombre.............................: Delete
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Elimina el registro seleccionado.
'***********************************************************************************
    On Error GoTo errDel
    
    With lstFins
        .RemoveItem .ListIndex
    End With
    RsFin.Delete
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number

End Sub

Private Function DatosValidos() As Boolean
'************************************************************************************
' Nombre.............................: DatosValidos
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Booleano
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Valida los datos introducidos por el usuario
'..............................................: antes de ser grabados en la base de datos.
'***********************************************************************************

Dim msg$, cCtrlErr As Control, iCod%, j%

    msg = "": iCod = 0: DatosValidos = True
    If Len(txtFin) = 0 Then
        Set cCtrlErr = txtFin
        msg = "La denominaci�n del final es obligatoria."
        iCod = 1
    ElseIf Len(txtFin) > 50 Then
        Set cCtrlErr = txtFin
        msg = "Longitud m�x. 50 caracteres."
        iCod = 2
    End If

    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
End Function

Private Sub SetSelectControls()
'************************************************************************************
' Nombre.............................: SetSelectControls
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Habilita los controles del formulario necesarios
'..............................................: para permitir la selecci�n de registros.
'***********************************************************************************

    mobjControlMDI.Buscar
    DesActivarTxt txtFin
    ActivarLstBox lstFins
    cmdBuscar.Enabled = False

End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub

