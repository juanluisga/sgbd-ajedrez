VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Begin VB.Form frmTablero 
   Caption         =   "Form1"
   ClientHeight    =   4815
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6735
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4815
   ScaleWidth      =   6735
   Begin VB.CommandButton cmdTablero 
      Height          =   375
      Index           =   3
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   4260
      Width           =   735
   End
   Begin VB.CommandButton cmdTablero 
      Height          =   375
      Index           =   2
      Left            =   2040
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4260
      Width           =   735
   End
   Begin VB.CommandButton cmdTablero 
      Height          =   375
      Index           =   1
      Left            =   1320
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   4260
      Width           =   735
   End
   Begin VB.CommandButton cmdTablero 
      Height          =   375
      Index           =   0
      Left            =   600
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4260
      Width           =   735
   End
   Begin ComctlLib.ImageList ilsTablero 
      Left            =   5340
      Top             =   3840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   65280
      _Version        =   327680
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   18
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":0000
            Key             =   "EB"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":0C52
            Key             =   "EN"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":18A4
            Key             =   "PB"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":2B16
            Key             =   "PN"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":3D88
            Key             =   "CB"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":51DA
            Key             =   "CN"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":66CC
            Key             =   "AB"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":7A7E
            Key             =   "AN"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":8E30
            Key             =   "TB"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":A002
            Key             =   "TN"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":B1D4
            Key             =   "DB"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":C642
            Key             =   "DN"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":DAB0
            Key             =   "RB"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":ECAE
            Key             =   "RN"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":FEAC
            Key             =   "PRIMERO"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":101C6
            Key             =   "ANTERIOR"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":104E0
            Key             =   "SIGUIENTE"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmTablero.frx":107FA
            Key             =   "ULTIMO"
         EndProperty
      EndProperty
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   81
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   71
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   61
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   51
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   41
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   31
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   21
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   11
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3540
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   82
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   72
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   62
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   52
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   42
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   32
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   22
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   12
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   3060
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   83
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   73
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   63
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   53
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   43
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   33
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   23
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   13
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2580
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   84
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   74
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   64
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   54
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   44
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   34
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   24
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   14
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   2100
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   85
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   75
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   65
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   55
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   45
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   35
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   25
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   15
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1620
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   86
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   76
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   66
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   56
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   46
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   36
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   26
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   16
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   1140
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   87
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   77
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   67
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   57
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   47
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   37
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   27
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   17
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   660
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   88
      Left            =   3480
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   78
      Left            =   3000
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   68
      Left            =   2520
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   58
      Left            =   2040
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   48
      Left            =   1560
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   38
      Left            =   1080
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   28
      Left            =   600
      Stretch         =   -1  'True
      Tag             =   "EN"
      Top             =   180
      Width           =   495
   End
   Begin VB.Image imgCasilla 
      Height          =   495
      Index           =   18
      Left            =   120
      Stretch         =   -1  'True
      Tag             =   "EB"
      Top             =   180
      Width           =   495
   End
End
Attribute VB_Name = "frmTablero"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Dim mbytColAnt As Byte, mbytFilAnt As Byte
Dim mobjTablero As New clsTablero

Private Sub Form_Load()

    Height = 5220
    Width = 6855
    
    mobjTablero.Inicializar
    
    ' Inicializa los gr�ficos del tablero
    With ilsTablero
        Set imgCasilla(11).Picture = .Overlay("EN", "TB")
        Set imgCasilla(21).Picture = .Overlay("EB", "CB")
        Set imgCasilla(31).Picture = .Overlay("EN", "AB")
        Set imgCasilla(41).Picture = .Overlay("EB", "DB")
        Set imgCasilla(51).Picture = .Overlay("EN", "RB")
        Set imgCasilla(61).Picture = .Overlay("EB", "AB")
        Set imgCasilla(71).Picture = .Overlay("EN", "CB")
        Set imgCasilla(81).Picture = .Overlay("EB", "TB")
        Set imgCasilla(12).Picture = .Overlay("EB", "PB")
        Set imgCasilla(22).Picture = .Overlay("EN", "PB")
        Set imgCasilla(32).Picture = .Overlay("EB", "PB")
        Set imgCasilla(42).Picture = .Overlay("EN", "PB")
        Set imgCasilla(52).Picture = .Overlay("EB", "PB")
        Set imgCasilla(62).Picture = .Overlay("EN", "PB")
        Set imgCasilla(72).Picture = .Overlay("EB", "PB")
        Set imgCasilla(82).Picture = .Overlay("EN", "PB")
        
        Set imgCasilla(13).Picture = .ListImages("EN").Picture
        Set imgCasilla(23).Picture = .ListImages("EB").Picture
        Set imgCasilla(33).Picture = .ListImages("EN").Picture
        Set imgCasilla(43).Picture = .ListImages("EB").Picture
        Set imgCasilla(53).Picture = .ListImages("EN").Picture
        Set imgCasilla(63).Picture = .ListImages("EB").Picture
        Set imgCasilla(73).Picture = .ListImages("EN").Picture
        Set imgCasilla(83).Picture = .ListImages("EB").Picture
        Set imgCasilla(14).Picture = .ListImages("EB").Picture
        Set imgCasilla(24).Picture = .ListImages("EN").Picture
        Set imgCasilla(34).Picture = .ListImages("EB").Picture
        Set imgCasilla(44).Picture = .ListImages("EN").Picture
        Set imgCasilla(54).Picture = .ListImages("EB").Picture
        Set imgCasilla(64).Picture = .ListImages("EN").Picture
        Set imgCasilla(74).Picture = .ListImages("EB").Picture
        Set imgCasilla(84).Picture = .ListImages("EN").Picture
        Set imgCasilla(15).Picture = .ListImages("EN").Picture
        Set imgCasilla(25).Picture = .ListImages("EB").Picture
        Set imgCasilla(35).Picture = .ListImages("EN").Picture
        Set imgCasilla(45).Picture = .ListImages("EB").Picture
        Set imgCasilla(55).Picture = .ListImages("EN").Picture
        Set imgCasilla(65).Picture = .ListImages("EB").Picture
        Set imgCasilla(75).Picture = .ListImages("EN").Picture
        Set imgCasilla(85).Picture = .ListImages("EB").Picture
        Set imgCasilla(16).Picture = .ListImages("EB").Picture
        Set imgCasilla(26).Picture = .ListImages("EN").Picture
        Set imgCasilla(36).Picture = .ListImages("EB").Picture
        Set imgCasilla(46).Picture = .ListImages("EN").Picture
        Set imgCasilla(56).Picture = .ListImages("EB").Picture
        Set imgCasilla(66).Picture = .ListImages("EN").Picture
        Set imgCasilla(76).Picture = .ListImages("EB").Picture
        Set imgCasilla(86).Picture = .ListImages("EN").Picture
        
        Set imgCasilla(17).Picture = .Overlay("EN", "PN")
        Set imgCasilla(27).Picture = .Overlay("EB", "PN")
        Set imgCasilla(37).Picture = .Overlay("EN", "PN")
        Set imgCasilla(47).Picture = .Overlay("EB", "PN")
        Set imgCasilla(57).Picture = .Overlay("EN", "PN")
        Set imgCasilla(67).Picture = .Overlay("EB", "PN")
        Set imgCasilla(77).Picture = .Overlay("EN", "PN")
        Set imgCasilla(87).Picture = .Overlay("EB", "PN")
        Set imgCasilla(18).Picture = .Overlay("EB", "TN")
        Set imgCasilla(28).Picture = .Overlay("EN", "CN")
        Set imgCasilla(38).Picture = .Overlay("EB", "AN")
        Set imgCasilla(48).Picture = .Overlay("EN", "DN")
        Set imgCasilla(58).Picture = .Overlay("EB", "RN")
        Set imgCasilla(68).Picture = .Overlay("EN", "AN")
        Set imgCasilla(78).Picture = .Overlay("EB", "CN")
        Set imgCasilla(88).Picture = .Overlay("EN", "TN")
    End With
    
    With ilsTablero
        Set cmdTablero(0).Picture = .ListImages("PRIMERO").Picture
        Set cmdTablero(1).Picture = .ListImages("ANTERIOR").Picture
        Set cmdTablero(2).Picture = .ListImages("SIGUIENTE").Picture
        Set cmdTablero(3).Picture = .ListImages("ULTIMO").Picture
    End With
End Sub

Private Sub imgCasilla_Click(Index As Integer)
    Static mbytColAnt As Byte, mbytFilAnt As Byte
    Dim mbytCol As Byte, mbytFil As Byte
    
    mbytCol = Index \ 10
    mbytFil = Index Mod 10
    If mbytColAnt = 0 Then
        ' Ha pinchado en la casilla de origen.
        mbytColAnt = mbytCol
        mbytFilAnt = mbytFil
        imgCasilla(Index).BorderStyle = vbFixedSingle
    Else
        ' Se pincha en la casilla destino
        ' �� Cuidado con pinchar en la misma casilla de origen !!
        If Not (mbytColAnt = mbytCol And mbytFilAnt = mbytFil) Then
            If mobjTablero.Mover(mbytColAnt, mbytFilAnt, _
                mbytCol, mbytFil) Then
                MoverPieza mbytColAnt, mbytFilAnt, mbytCol, mbytFil
            Else
                MsgBox "El movimiento es ilegal."
            End If
        End If
        imgCasilla(mbytColAnt * 10 + mbytFilAnt).BorderStyle = vbBSNone
        mbytColAnt = 0
        mbytFilAnt = 0
    End If
End Sub

Private Sub MoverPieza(X1 As Byte, Y1 As Byte, X2 As Byte, Y2 As Byte)
    Dim strPieza As String
    ' Capturamos la pieza movida para dibujarla en
    ' la casilla de destino
    
    Select Case mobjTablero.Escaque(X2, Y2)
        Case CodPeonBlanco
            strPieza = "PB"
        Case CodTorreBlanca
            strPieza = "TB"
        Case CodCaballoBlanco
            strPieza = "CB"
        Case CodAlfilBlanco
            strPieza = "AB"
        Case CodDamaBlanca
            strPieza = "DB"
        Case CodReyBlanco
            strPieza = "RB"
        Case CodPeonNegro
            strPieza = "PN"
        Case CodTorreNegra
            strPieza = "TN"
        Case CodCaballoNegro
            strPieza = "CN"
        Case CodAlfilNegro
            strPieza = "AN"
        Case CodDamaNegra
            strPieza = "DN"
        Case CodReyNegro
            strPieza = "RN"
    End Select
    
    With ilsTablero
        ' Dejamos como libre la casilla de origen.
        Set imgCasilla(X1 * 10 + Y1).Picture = _
            .ListImages(CStr(imgCasilla(X1 * 10 + Y1).Tag)).Picture
        Set imgCasilla(X2 * 10 + Y2).Picture = _
            .Overlay(CStr(imgCasilla(X2 * 10 + Y2).Tag), strPieza)
    End With
End Sub
