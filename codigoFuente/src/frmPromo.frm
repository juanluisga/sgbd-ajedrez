VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Begin VB.Form frmPromo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Promoci�n"
   ClientHeight    =   2970
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6855
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2970
   ScaleWidth      =   6855
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPromo 
      Height          =   1095
      Index           =   3
      Left            =   5310
      MaskColor       =   &H0000FF00&
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1020
      UseMaskColor    =   -1  'True
      Width           =   1155
   End
   Begin VB.CommandButton cmdPromo 
      Height          =   1095
      Index           =   2
      Left            =   3670
      MaskColor       =   &H0000FF00&
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1020
      UseMaskColor    =   -1  'True
      Width           =   1155
   End
   Begin VB.CommandButton cmdPromo 
      Height          =   1095
      Index           =   1
      Left            =   2030
      MaskColor       =   &H0000FF00&
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1020
      UseMaskColor    =   -1  'True
      Width           =   1155
   End
   Begin VB.CommandButton cmdPromo 
      Height          =   1095
      Index           =   0
      Left            =   390
      MaskColor       =   &H0000FF00&
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1020
      UseMaskColor    =   -1  'True
      Width           =   1155
   End
   Begin ComctlLib.ImageList ilsPromo 
      Left            =   3180
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   43
      ImageHeight     =   39
      MaskColor       =   65280
      _Version        =   327680
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":0000
            Key             =   "DB"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":146E
            Key             =   "TB"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":2640
            Key             =   "AB"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":39F2
            Key             =   "CB"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":4E44
            Key             =   "DN"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":62B2
            Key             =   "TN"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":7484
            Key             =   "AN"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmPromo.frx":8836
            Key             =   "CN"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblPromo 
      Alignment       =   2  'Center
      Caption         =   "Caballo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   5310
      TabIndex        =   8
      Top             =   2280
      Width           =   1155
   End
   Begin VB.Label lblPromo 
      Alignment       =   2  'Center
      Caption         =   "Alfil"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   3670
      TabIndex        =   7
      Top             =   2280
      Width           =   1155
   End
   Begin VB.Label lblPromo 
      Alignment       =   2  'Center
      Caption         =   "Torre"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   2030
      TabIndex        =   6
      Top             =   2280
      Width           =   1155
   End
   Begin VB.Label lblPromo 
      Alignment       =   2  'Center
      Caption         =   "Dama"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   390
      TabIndex        =   5
      Top             =   2280
      Width           =   1155
   End
   Begin VB.Label lblPromo 
      Caption         =   "Seleccione la pieza a promocionar:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   0
      Left            =   180
      TabIndex        =   4
      Top             =   180
      Width           =   6435
   End
End
Attribute VB_Name = "frmPromo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variable con el c�digo de
' casilla de destino
Private mbytCasilla As Byte
Private mPieza As TIPOPIEZA

Public Property Let Bando(ByVal NewValue As TIPOBANDO)
'***************************************************************************************
' Nombre........................: Bando
' Tipo...............................: Property Let
' �mbito..........................: Public
' Par�metros.................: TIPOBANDO -> bando que realiza la promoci�n
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Configura los botones de selecci�n de pieza a
' ........................................: promocionar en funci�n del bando que va ha
'.........................................: realizar dicha operaci�n.
'****************************************************************************************

    With ilsPromo
        Select Case NewValue
            Case BandoBlanco
                cmdPromo(0).Picture = .ListImages("DB").Picture
                cmdPromo(1).Picture = .ListImages("TB").Picture
                cmdPromo(2).Picture = .ListImages("AB").Picture
                cmdPromo(3).Picture = .ListImages("CB").Picture
            Case BandoNegro
                cmdPromo(0).Picture = .ListImages("DN").Picture
                cmdPromo(1).Picture = .ListImages("TN").Picture
                cmdPromo(2).Picture = .ListImages("AN").Picture
                cmdPromo(3).Picture = .ListImages("CN").Picture
        End Select
    End With
End Property

Public Property Get Casilla() As Byte
    Casilla = mbytCasilla
End Property

Public Property Let Casilla(ByVal NewValue As Byte)
    mbytCasilla = NewValue
End Property

Public Property Get Pieza() As TIPOPIEZA
    Pieza = mPieza
End Property
Private Sub cmdPromo_Click(Index As Integer)
    Select Case Index
        Case 0      ' Dama
            Select Case mbytCasilla
                ' Bando Negro
                Case 11
                    mbytCasilla = 101
                Case 21
                    mbytCasilla = 105
                Case 31
                    mbytCasilla = 109
                Case 41
                    mbytCasilla = 113
                Case 51
                    mbytCasilla = 117
                Case 61
                    mbytCasilla = 121
                Case 71
                    mbytCasilla = 125
                Case 81
                    mbytCasilla = 129
                ' Bando Blanco
                Case 18
                    mbytCasilla = 201
                Case 28
                    mbytCasilla = 205
                Case 38
                    mbytCasilla = 209
                Case 48
                    mbytCasilla = 213
                Case 58
                    mbytCasilla = 217
                Case 68
                    mbytCasilla = 221
                Case 78
                    mbytCasilla = 225
                Case 88
                    mbytCasilla = 229
            End Select
            Select Case mbytCasilla
                Case 101, 105, 109, 113, 117, 121, 125, 129
                    mPieza = CodDamaNegra
                Case 201, 205, 209, 213, 217, 221, 225, 229
                    mPieza = CodDamaBlanca
            End Select
            
        Case 1      ' Torre
            Select Case mbytCasilla
                ' Bando Negro
                Case 11
                    mbytCasilla = 103
                Case 21
                    mbytCasilla = 107
                Case 31
                    mbytCasilla = 111
                Case 41
                    mbytCasilla = 115
                Case 51
                    mbytCasilla = 119
                Case 61
                    mbytCasilla = 123
                Case 71
                    mbytCasilla = 127
                Case 81
                    mbytCasilla = 131
                ' Bando Blanco
                Case 18
                    mbytCasilla = 203
                Case 28
                    mbytCasilla = 207
                Case 38
                    mbytCasilla = 211
                Case 48
                    mbytCasilla = 215
                Case 58
                    mbytCasilla = 219
                Case 68
                    mbytCasilla = 223
                Case 78
                    mbytCasilla = 227
                Case 88
                    mbytCasilla = 231
            End Select
            Select Case mbytCasilla
                Case 103, 107, 111, 115, 119, 123, 127, 131
                    mPieza = CodTorreNegra
                Case 203, 207, 211, 215, 219, 223, 227, 231
                    mPieza = CodTorreBlanca
            End Select
            
        Case 2      ' Alfil
            Select Case mbytCasilla
                ' Bando Negro
                Case 11
                    mbytCasilla = 102
                Case 21
                    mbytCasilla = 106
                Case 31
                    mbytCasilla = 110
                Case 41
                    mbytCasilla = 114
                Case 51
                    mbytCasilla = 118
                Case 61
                    mbytCasilla = 122
                Case 71
                    mbytCasilla = 126
                Case 81
                    mbytCasilla = 130
                ' Bando Blanco
                Case 18
                    mbytCasilla = 202
                Case 28
                    mbytCasilla = 206
                Case 38
                    mbytCasilla = 210
                Case 48
                    mbytCasilla = 214
                Case 58
                    mbytCasilla = 218
                Case 68
                    mbytCasilla = 222
                Case 78
                    mbytCasilla = 226
                Case 88
                    mbytCasilla = 230
            End Select
            Select Case mbytCasilla
                Case 102, 106, 110, 114, 118, 122, 126, 130
                    mPieza = CodAlfilNegro
                Case 202, 206, 210, 214, 218, 222, 226, 230
                    mPieza = CodAlfilBlanco
            End Select
        
        Case 3      ' Caballo
            Select Case mbytCasilla
                ' Bando Negro
                Case 11
                    mbytCasilla = 104
                Case 21
                    mbytCasilla = 108
                Case 31
                    mbytCasilla = 112
                Case 41
                    mbytCasilla = 116
                Case 51
                    mbytCasilla = 120
                Case 61
                    mbytCasilla = 124
                Case 71
                    mbytCasilla = 128
                Case 81
                    mbytCasilla = 132
                ' Bando Blanco
                Case 18
                    mbytCasilla = 204
                Case 28
                    mbytCasilla = 208
                Case 38
                    mbytCasilla = 212
                Case 48
                    mbytCasilla = 216
                Case 58
                    mbytCasilla = 220
                Case 68
                    mbytCasilla = 224
                Case 78
                    mbytCasilla = 228
                Case 88
                    mbytCasilla = 232
            End Select
            Select Case mbytCasilla
                Case 104, 108, 112, 116, 120, 124, 128, 132
                    mPieza = CodCaballoNegro
                Case 204, 208, 212, 216, 220, 224, 228, 232
                    mPieza = CodCaballoBlanco
            End Select
            
    End Select
    Me.Hide
End Sub

Private Sub Form_Load()
    HelpContextID = CT_HELP_PROMOCION
End Sub
