Attribute VB_Name = "modPGN"
Option Explicit

' Define constantes para c�digos de token
Private Const CODIDENTIFICADOR  As Byte = 1
Private Const CODENTERO         As Byte = 2
Private Const CODCADENA         As Byte = 3
Private Const CODSAN            As Byte = 4
Private Const CODCOMENTARIO     As Byte = 5
Private Const CODTERMINACION    As Byte = 6
Private Const CODENROQUE        As Byte = 7
Private Const CODABRECORCH      As Byte = 8
Private Const CODCIERRACORCH    As Byte = 9
Private Const CODDOLAR          As Byte = 10
Private Const CODABREPAR        As Byte = 11
Private Const CODCIERRAPAR      As Byte = 12
Private Const CODPUNTOCOMA      As Byte = 13
Private Const CODPUNTO          As Byte = 14
Private Const CODESCAPE         As Byte = 15
Private Const CODCOMODIN        As Byte = 16
Private Const CODCONTROL1       As Byte = 20
Private Const CODCONTROL2       As Byte = 98
Private Const CODCONTROL3       As Byte = 99
Private Const CODCONTROL4       As Byte = 100

' -------------------------------------------------
' Define un tipo para almacenar la informaci�n
' relativa al token le�do.
Type DEFTOKEN
    Codigo  As Byte
    cadena  As String
End Type
 
' Define un tipo que describe los punteros
Type TIPOPUNTERO
    Caracter    As Integer
    Segmento    As Byte
End Type

Type TIPOMOVIMIENTO
    CodigoPieza     As Byte
    CasillaOrigen   As Byte
    CasillaDestino  As Byte
'    ComentaAsoc     As String
End Type

' -------------------------------------------------


' Definici�n de variables
Dim Buffer(1 To 2)      As String
Dim Token               As DEFTOKEN
Dim CadMvtoSAN          As String

Rem Define los punteros de lectura y el buffer donde se almacenan las l�neas del fichero PGN.
Dim PunteroAvance       As TIPOPUNTERO
Dim PunteroComienzo     As TIPOPUNTERO


Rem Define el resto de variables
Dim EvaluaEtiquetas     As Boolean
    Rem *** La variable "EvaluaEtiquetas" sirve para se�alar si el
    Rem explorador est� evaluando la secci�n de etiquetas o la
    Rem de movimientos para diferenciar entre los aut�matas de
    Rem IDENTIFICADOR y MvtoSAN
    
Dim ErrorSAN            As Boolean
    ' Controla si se produce alg�n error de movimiento SAN incorrecto en las secciones
    ' de movimientos.

    
Dim EtiquetaAnterior    As String

' Objeto que almacena en memoria
' la posici�n de la partida.
Private mobjBoard As New clsTablero

Private rsMoves As Recordset

' Tama�o del bloque de caracteres a leer del fichero
Private Const LONGBLOQUE As Integer = 512

' Identificaci�n de la partida que se va a cargar.
Private lngIdPartida As Long


Public Sub AbrirFichero(ByVal lngPartida As Long)
'*****************************************************************************
' Nombre...................: AbrirFichero
' Tipo..........................: Procedimiento
' �mbito.....................: P�blico
' Par�metros............: Long -> C�digo de la partida
' ....................................: a incorporar.
' Valor de retorno....: Ninguno
' �ltimo cambio........: 10/3/00
' Descripci�n.............: Abre el fichero PGN donde se
' .....................................: encuentra la partida a incor-
' .....................................: porar a la base de datos.
'***************************************************************************
    On Error GoTo OpenFileError
    
    frmPartidas.dlgPt.CancelError = True
    frmPartidas.dlgPt.ShowOpen
    If frmPartidas.dlgPt.filename <> "" Then
        lngIdPartida = lngPartida
        PunteroAvance.Caracter = 0
        PunteroAvance.Segmento = 1
        PunteroComienzo.Caracter = 0
        PunteroComienzo.Segmento = 1
        EvaluaEtiquetas = True
        Open frmPartidas.dlgPt.filename For Input As 1
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        PartidaPgn
        Close 1
    End If
    
GetExit:
    Exit Sub
    
OpenFileError:
    If Err.Number = 32755 Then
        Exit Sub
    Else
        MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
            "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
            vbInformation, " " & Err.Source
        Resume GetExit
    End If
    
End Sub

Private Sub PartidaPgn()
'****************************************************************************
' Nombre........................: PartidaPgn
' Tipo...............................: Procedimiento
' �mbito..........................: Privado
' Par�metros.................: Long -> C�digo de la partida
' ........................................:                 a incorporar.
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 10/3/00
' Descripci�n................: Describe la estructura de la
' ........................................: partida en formato PGN: una
' ........................................: secci�n de etiquetas y otra
' ........................................: de movimientos. Antes de comenzar
' ........................................: ha interpretar los movimientos
' ........................................: inicializa la clase mobjBoard
' ........................................: encargada de verificar la validez
' ........................................: de dichos movimientos.
'****************************************************************************
    
    On Error GoTo ControlError
    
    SeccionEtiquetas
    mobjBoard.Inicializar
    SeccionMovtos
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub SeccionEtiquetas()
'**********************************************************************
' Nombre....................: SeccionEtiquetas
' Tipo...........................: Procedimiento
' �mbito......................: Privado
' Par�metros.............: Ninguno
' Valor de retorno....: Ninguno
' �ltimo cambio.......: 10/3/00
' Descripci�n...........: Describe la secci�n de etiquetas:
' ...................................: Str: las siete etiquetas obliga-
' ...................................: torias, y el resto de etiquetas
' ...................................: posibles.
'***********************************************************************
    On Error GoTo ControlError
    
    EvaluaEtiquetas = True
    Str
    RestoEtiquetas
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Str()
'*******************************************************************
' Nombre.......................: Str
' Tipo..............................: Procedimiento
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 10/3/00
' Descripci�n................: Lee las siete etiquetas
' ........................................: obligatorias en toda partida
' ........................................: escrita en formato PGN.
'********************************************************************
    Dim Paso As Boolean
    On Error GoTo ControlError
    
    Paso = False
    Do While (Not Paso)
    
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "event") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
        
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "site") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            ' Mostramos en el formulario de partidas
            ' el lugar donde se disput� la partida.
            frmPartidas.txtPt(9) = Token.cadena
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
        
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "date") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            ' Mostramos en el formulario de partidas
            ' la fecha de celebraci�n de la partida.
            frmPartidas.txtPt(8) = Token.cadena
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
        
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "round") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            frmPartidas.txtPt(11) = Token.cadena
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
        
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "white") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
        
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "black") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
        
        If Token.Codigo = CODABRECORCH Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 1
        End If
        
        If (Token.Codigo = CODIDENTIFICADOR And _
            LCase(Token.cadena) = "result") Then
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 3
        End If
        
        If Token.Codigo = CODCADENA Then
            Select Case Trim$(Token.cadena)
                Case "1-0"
                    frmPartidas.optPt(0).Value = True
                Case "0-1"
                    frmPartidas.optPt(1).Value = True
                Case "1/2-1/2", "0.5-0.5", ".5-.5"
                    frmPartidas.optPt(2).Value = True
                Case "*"
                    frmPartidas.optPt(3).Value = True
            End Select
            
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 4
        End If
        
        If Token.Codigo = CODCIERRACORCH Then
            Paso = True
            Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
        Else
            TrataError 2
        End If
    Loop
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume Next
    
End Sub

Private Sub RestoEtiquetas()
'*********************************************************************
' Nombre............................: RestoEtiquetas
' Tipo...................................: Procedimiento
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 10/3/00
' Descripci�n....................: Lee el resto de etiquetas.
'*********************************************************************
    On Error GoTo ControlError
    If Token.Codigo = CODABRECORCH Then
        Pareja
        RestoEtiquetas
    End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Pareja()
'****************************************************************
' Nombre.....................: Pareja
' Tipo............................: Procedimiento
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 10/3/00
' Descripci�n..............: Lee una etiqueta individual.
'*****************************************************************
    
    On Error GoTo ControlError
    
    If Token.Codigo = CODABRECORCH Then
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    Else
        TrataError 1
    End If
    
    If Token.Codigo = CODIDENTIFICADOR Then
        EtiquetaAnterior = Token.cadena
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    Else
        TrataError 3
    End If
    
    If Token.Codigo = CODCADENA Then
        AsignaValorEtiqueta EtiquetaAnterior
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    Else
        TrataError 4
    End If
    
    If Token.Codigo = CODCIERRACORCH Then
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    Else
        TrataError 2
    End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub AsignaValorEtiqueta(strEtiqueta As String)
'****************************************************************
' Nombre.......................: AsignaValorEtiqueta
' Tipo..............................: Procedimiento
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 10/3/00
' Descripci�n...............: Muestra en el formulario
' .......................................: de partidas los valores de
' .......................................: algunas de las etiquetas.
'*****************************************************************
    With frmPartidas
        Select Case LCase(strEtiqueta)
            Case "white"
                
            Case "black"
                
            Case "event"
                
            Case "round"
                .txtPt(11) = Token.cadena
            Case "site"
                .txtPt(9) = Token.cadena
            Case "date"
                .txtPt(8) = Token.cadena
            Case "result"
                ' revisar
            Case "whiteelo"
                .txtPt(2) = Token.cadena
            Case "blackelo"
                .txtPt(3) = Token.cadena
            Case "eco"
                
            Case "annotator"
                .txtPt(7) = Token.cadena
            Case "fen"
                
            Case "termination"
                
            Case "mode"
                
            
        End Select
    End With
    EtiquetaAnterior = ""
End Sub

Private Sub SeccionMovtos()
'**********************************************************************
' Nombre.........................: SeccionMovtos
' Tipo................................: Procedimiento
' �mbito...........................: Privado
' Par�metros..................: Long -> C�digo de la partida
' .........................................: a incorporar.
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 10/3/00
' Descripci�n.................: Comienza leer la secci�n de
' .........................................: movimientos y borra todos los
' .........................................: que estuvieran almacenados en
' .........................................: base de datos para esa partida
' .........................................: en el caso de que se trate de
' .........................................: una modificaci�n.
'***********************************************************************

    Dim strSQL As String
    On Error GoTo ControlError
    
    EvaluaEtiquetas = False
    ErrorSAN = False
    ' Esto nos evita que los
    ' mvtos. SAN sean evaluados
    ' como Identificadores.
    
    ' Borramos los movimientos existentes para
    ' esa partida, si los hubiera, caso de que
    ' fuera una edici�n de partida.
    strSQL = "SELECT * FROM Movimientos " & _
            "WHERE mv_partida = " & CStr(lngIdPartida)
    Set rsMoves = dbDatos.OpenRecordset(strSQL)
    
    With rsMoves
        Do While Not .EOF
            .Delete
            .MoveNext
        Loop
    End With
    
    Secuencia
    
    Set rsMoves = Nothing
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Secuencia()
'****************************************************
' Nombre.........................: Secuencia
' Tipo................................: Procedimiento
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 10/3/00
' Descripci�n.................:
'****************************************************
    On Error GoTo ControlError
    If Token.Codigo = CODTERMINACION Then
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    Else
        Elemento
        Secuencia
    End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Elemento()
'****************************************************
' Nombre..................: Elemento
' Tipo.........................: Procedimiento
' �mbito....................: Privado
' Par�metros...........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n..........:
'****************************************************

    Dim Paso As Boolean
    Dim pbytTurnoAnt As Byte
    Dim pintMovAnt As Integer
    
    On Error GoTo ControlError
    
    Paso = False
    Do While Not Paso
        Select Case Token.Codigo
            Case CODENTERO
                Rem *** Numeraci�n de movimiento ***
                Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
                Paso = True
                
            Case CODPUNTO
                Rem *** Leemos todos los puntos que vengan ***
                Do While (Token.Codigo = CODPUNTO)
                    Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
                Loop
                Paso = True
                
            Case CODSAN, CODENROQUE
                Rem *** Es un Mvto. SAN (Cdgo. 4) ***
                Rem *** o es un Enroque(Cdgo. 7)  ***
                If Not ErrorSAN Then
                    If Token.cadena <> "9999" Then
                        ' Si el mvto. SAN no es err�neo...
                        Dim bytCO As Byte, bytFO As Byte
                        Dim bytCD As Byte, bytFD As Byte
                        bytCO = CByte(Mid$(Token.cadena, 1, 1))
                        bytFO = CByte(Mid$(Token.cadena, 2, 1))
                        bytCD = CByte(Mid$(Token.cadena, 3, 1))
                        bytFD = CByte(Mid$(Token.cadena, 4, 1))

                        
                        ' Movimiento con todas las coordenadas
                        ' a continuaci�n comprobamos su validez.
                        Dim vntResultadoMv As Variant
                        
                        pbytTurnoAnt = mobjBoard.Turno
                        pintMovAnt = mobjBoard.NumMovto
                        vntResultadoMv = mobjBoard.Mover( _
                            bytCO, bytFO, bytCD, bytFD)
                        If Not vntResultadoMv = False Then
                            ' El movimiento es legal.
                            ' A�adimos el registro
                            ' con el nuevo movimiento.
                            With rsMoves
                                .AddNew
                                !mv_partida = lngIdPartida
                                !mv_nmov = pintMovAnt
                                !mv_bando = pbytTurnoAnt
                                !mv_orig = bytCO * 10 + bytFO
                                !mv_dest = bytCD * 10 + bytFD
                                .Update
                            End With
                        ElseIf vntResultadoMv = False Then
                            ' Se trata de un movimiento err�neo
                            ' y se trata como tal.
                            TrataError 6
                        End If
                                        
                    Else
                        ' Si el mvto. SAN es err�neo a partir de aqu�,
                        ' los siguientes ser�n ignorados.
                        ' Se controla mediante la variable "ErrorSan".
                        ErrorSAN = True
                        TrataError 6
                    End If
                End If
                Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
                Paso = True
                
            Case CODCOMENTARIO
                Rem *** Es un Comentario ***
                Rem ...Tratamiento de comentarios
                Dim strCriterio As String
                
                ' El comentario se refiere al anterior
                ' movimiento registrado.
'                pintMovAnt = mobjBoard.NumMovto - 1
                pbytTurnoAnt = IIf(mobjBoard.Turno = BandoBlanco, 2, 1)
                If pbytTurnoAnt = 1 Then
                    ' El turno anterior es el blanco
                    pintMovAnt = mobjBoard.NumMovto
                ElseIf pbytTurnoAnt = 2 Then
                    ' El turno anterior es el negro
                    pintMovAnt = mobjBoard.NumMovto - 1
                End If
                If pintMovAnt = 0 Then
                    ' Este comentario se refiere a la
                    ' partida en su totalidad.
                    frmPartidas.txtPt(10) = Token.cadena
                Else
                    strCriterio = "mv_partida=" & lngIdPartida & _
                        " AND mv_nmov=" & pintMovAnt & _
                        " AND mv_bando=" & pbytTurnoAnt
                        
                    With rsMoves
                        .FindFirst strCriterio
                        If Not .NoMatch Then
                            .Edit
                            !mv_coment = Token.cadena
                            .Update
                        End If
                    End With
                End If
                
                Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
                Paso = True
                                        
            Case CODDOLAR
                AnotacionGlyph
                Paso = True
                
            Case CODABREPAR
                Variacion
                Paso = True
                
            Case CODCOMODIN
                TrataError 7
                Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
                Paso = True
                
            Case Else
                Rem ****** Error ******
        End Select
    Loop
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
       
End Sub

Private Sub AnotacionGlyph()
'****************************************************
' Nombre....................: AnotacionGlyph
' Tipo...........................: Procedimiento
' �mbito......................: Privado
' Par�metros.............: Ninguno
' Valor de retorno....: Ninguno
' �ltimo cambio........: 10/3/00
' Descripci�n.............:
'****************************************************
    
    Dim mobjDatos As New clsDatos
    Dim strNAG As String
    On Error GoTo ControlError
    
    Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    If Token.Codigo = CODENTERO Then
        ' Aqu� se asignaria el comentario correspondiente al codigo NAG.
'        strNAG = mobjDatos.GetNAG(CByte(Token.cadena))
        If Val(Token.cadena) >= 0 And Val(Token.cadena) < 140 Then
            strNAG = mtNAG(CByte(Token.cadena))
        End If
    End If
    Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume Next
    
    
End Sub

Private Sub Variacion()
'****************************************************
' Nombre..................: Variacion
' Tipo.........................: Procedimiento
' �mbito....................: Privado
' Par�metros...........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n..........:
'****************************************************

    Dim Nparentesis     As Byte
    ' La variable NParentesis almacena el n� de par�ntesis
    ' que permanecen abiertos durante el proceso de lectura
    ' ya que si NParentesis>0 todo lo que se lea ser� alma-
    ' cenado como comentario, ya que ese es el tratamiento que
    ' recibir�n las variaciones.
    On Error GoTo ControlError
    
    EvaluaEtiquetas = True
    Nparentesis = 0
    Do
        Select Case Token.Codigo
            Case CODABREPAR
                Nparentesis = Nparentesis + 1
            Case CODCIERRAPAR
                Nparentesis = Nparentesis - 1
            Case CODSAN, CODENROQUE
                Token.cadena = CadMvtoSAN
        End Select
        Token = Lector(Buffer, PunteroComienzo, PunteroAvance)
    Loop Until Nparentesis = 0 Or Token.Codigo = CODTERMINACION
    If Token.Codigo = CODTERMINACION Then
        TrataError 5
    End If
    EvaluaEtiquetas = False
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Identificador(cadena() As String, Pc As TIPOPUNTERO, _
                            Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                            Aceptado As Integer)
'****************************************************
' Nombre............: Identificador
' Tipo..............: Procedimiento
' �mbito............: Privado
' Par�metros........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n.......:
'****************************************************
                            
    Dim Letra As Byte
    On Error GoTo ControlError
    
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        Letra = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
        If (Letra > 64 And Letra < 91) Or _
            (Letra > 96 And Letra < 123) Then
            Do
                Elemento.cadena = Elemento.cadena & _
                    Mid(cadena(Pa.Segmento), Pa.Caracter, 1)
                IncPuntero Pa, cadena
                Letra = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
            Loop While (Letra > 64 And Letra < 91) Or _
                (Letra > 96 And Letra < 123) Or _
                (IsNumeric(Mid(cadena(Pa.Segmento), Pa.Caracter, 1)))
            Aceptado = 1
            Elemento.Codigo = CODIDENTIFICADOR
        End If
    End If

GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub Entero(cadena() As String, Pc As TIPOPUNTERO, _
                    Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                    Aceptado As Integer)
'****************************************************
' Nombre............: Entero
' Tipo..............: Procedimiento
' �mbito............: Privado
' Par�metros........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n.......:
'****************************************************
                    
    Dim Estado As Integer
    On Error GoTo ControlError
    
    Estado = 0
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If IsNumeric(Mid(cadena(Pa.Segmento), Pa.Caracter, 1)) Then
                        Elemento.cadena = Elemento.cadena & _
                            Mid(cadena(Pa.Segmento), Pa.Caracter, 1)
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                    End If
                    
                Case 1
                    Do While (IsNumeric(Mid(cadena(Pa.Segmento), Pa.Caracter, 1)))
                        Elemento.cadena = Elemento.cadena & _
                            Mid(cadena(Pa.Segmento), Pa.Caracter, 1)
                        IncPuntero Pa, cadena
                    Loop
                    Aceptado = 1
                    Elemento.Codigo = CODENTERO
                    Estado = 255
                    
            End Select
        Loop
    End If

GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub cadena(cadena() As String, Pc As TIPOPUNTERO, _
                    Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                    Aceptado As Integer)
'****************************************************
' Nombre............: Cadena
' Tipo..............: Procedimiento
' �mbito............: Privado
' Par�metros........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n.......:
'****************************************************
                    
    Dim NumCar As Byte
    Dim Aux As TIPOPUNTERO
    On Error GoTo ControlError
    
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        NumCar = 0
        If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = Chr(34)) Then
            IncPuntero Pa, cadena
            Aux = Pa
            NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
            Do While (NumCar > 31) And (NumCar < 255) And (NumCar <> 34)
                Elemento.cadena = Elemento.cadena & Chr(NumCar)
                IncPuntero Pa, cadena
                NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
            Loop
            If (NumCar = 34) Then
                Aceptado = 1
                Elemento.Codigo = CODCADENA
                IncPuntero Pa, cadena
            Else
                ReponePunteros cadena, Pc, Pa, Aux
                Elemento.Codigo = CODCONTROL4
            End If
        End If
   End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Comentario(cadena() As String, Pc As TIPOPUNTERO, _
                        Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                        Aceptado As Integer)
'****************************************************
' Nombre............: Comentario
' Tipo..............: Procedimiento
' �mbito............: Privado
' Par�metros........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n.......:
'****************************************************
                        
    Dim NumCar As Byte
    Dim Aux As TIPOPUNTERO
    On Error GoTo ControlError
    
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        NumCar = 0
        If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "{") Then
            IncPuntero Pa, cadena
            Aux = Pa
            NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
            Do While (NumCar > 31 And NumCar < 255 And NumCar <> 125)
                Elemento.cadena = Elemento.cadena & _
                    Mid(cadena(Pa.Segmento), Pa.Caracter, 1)
                IncPuntero Pa, cadena
                NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
            Loop
            If (NumCar = 125) Then
                Aceptado = 1
                Elemento.Codigo = CODCOMENTARIO
                IncPuntero Pa, cadena
            Else
                ReponePunteros cadena, Pc, Pa, Aux
                Elemento.Codigo = CODCONTROL4
            End If
        End If
    End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Enroque(cadena() As String, Pc As TIPOPUNTERO, _
                    Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                    Aceptado As Integer)
'****************************************************
' Nombre............: Enroque
' Tipo..............: Procedimiento
' �mbito............: Privado
' Par�metros........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n.......:
'****************************************************
                    
    Dim Estado As Integer
    Dim Aux As TIPOPUNTERO
    On Error GoTo ControlError
    
    Estado = 0
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        Aux = Pa
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "O") Then
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                    End If
                        
                Case 1
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-") Then
                        IncPuntero Pa, cadena
                        Estado = 2
                    Else
                        Estado = 255
                    End If
                        
                Case 2
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "O") Then
                        IncPuntero Pa, cadena
                        Estado = 3
                    Else
                        Estado = 255
                    End If
                        
                Case 3
                    If Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-" Then
                        IncPuntero Pa, cadena
                        Estado = 4
                    Else
                        Rem *** Es un enroque corto O-O ***
                        If mobjBoard.Turno = BandoBlanco Then
                            Elemento.cadena = "5171"
                        ElseIf mobjBoard.Turno = BandoNegro Then
                            Elemento.cadena = "5878"
                        End If
                        Select Case Mid(cadena(Pa.Segmento), Pa.Caracter, 1)
                            Case "+", "#"
                                Estado = 6
                            Case Else           '"?", "!"
                                Do While Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "?" Or Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "!"
                                    IncPuntero Pa, cadena
                                Loop
                                Estado = 7
                        End Select
                    End If
                        
                Case 4
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "O") Then
                        Rem *** Enroque largo O-O-O ***
                        IncPuntero Pa, cadena
                        Estado = 5
                        If mobjBoard.Turno = BandoBlanco Then
                            Elemento.cadena = "5131"
                        ElseIf mobjBoard.Turno = BandoNegro Then
                            Elemento.cadena = "5838"
                        End If
                    Else
                        Estado = 255
                    End If
                        
                Case 5
                    Select Case Mid(cadena(Pa.Segmento), Pa.Caracter, 1)
                            Case "+", "#"
                                Call IncPuntero(Pa, cadena)
                                Estado = 6
                            Case Else               '"?", "!"
                                Estado = 7
                    End Select
                
                Case 6
                    Estado = 7
                    
                Case 7
                    Aceptado = 1
                    Elemento.Codigo = CODENROQUE
                    If Aux.Segmento = Pa.Segmento Then
                        CadMvtoSAN = Mid(cadena(Pa.Segmento), _
                                        Aux.Caracter, _
                                        Pa.Caracter - Aux.Caracter)
                    Else
                        CadMvtoSAN = Mid(cadena(Aux.Segmento), _
                                        Aux.Caracter, _
                                        Len(cadena(Aux.Segmento))) & _
                                        Mid(cadena(Pa.Segmento), 1, _
                                        Pa.Caracter)
                    End If
                    Estado = 255
            End Select
        Loop
    End If

GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub Terminacion(cadena() As String, Pc As TIPOPUNTERO, _
                        Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                        Aceptado As Integer)
'****************************************************
' Nombre............: Terminacion
' Tipo..............: Procedimiento
' �mbito............: Privado
' Par�metros........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio.....: 10/3/00
' Descripci�n.......:
'****************************************************
        
    Rem ***************************************************
    Rem Formatos de resultado que se reconocen
    Rem "1-0", "0-1", "1/2-1/2",
    Rem "�-�", "0.5-0.5", ".5-.5" y "*"
    Rem ***************************************************
    
    Dim Estado As Integer
    Dim Aux    As TIPOPUNTERO
    On Error GoTo ControlError
    
    Estado = 0
    Aux = Pa
    Rem ***************************************************
    Rem             Formato "*"
    Rem ***************************************************
    If (Aceptado = 0) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "*") Then
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 1
                    Aceptado = 1
                    Elemento.Codigo = CODTERMINACION
                    Elemento.cadena = "*"
                    Estado = 255
            End Select
        Loop
        Estado = 0
    End If
    
    Rem ***************************************************
    Rem                 Formato "1-0"
    Rem ***************************************************
    If (Aceptado = 0) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "1") Then
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 1
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-") Then
                        IncPuntero Pa, cadena
                        Estado = 2
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 2
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "0") Then
                        IncPuntero Pa, cadena
                        Estado = 3
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 3
                    Aceptado = 1
                    Elemento.Codigo = CODTERMINACION
                    Elemento.cadena = "1-0"
                    Estado = 255
            End Select
        Loop
        Estado = 0
    End If

    Rem ****************************************************
    Rem                 Formato "0-1"
    Rem ****************************************************
    If (Aceptado = 0) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "0") Then
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 1
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-") Then
                        IncPuntero Pa, cadena
                        Estado = 2
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 2
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "1") Then
                        IncPuntero Pa, cadena
                        Estado = 3
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 3
                    Aceptado = 1
                    Elemento.Codigo = CODTERMINACION
                    Elemento.cadena = "0-1"
                    Estado = 255
            End Select
        Loop
        Estado = 0
    End If
    
    Rem ****************************************************
    Rem                 Formato "�-�"
    Rem ****************************************************
    If (Aceptado = 0) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "�") Then
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 1
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-") Then
                        IncPuntero Pa, cadena
                        Estado = 2
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 2
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "�") Then
                        IncPuntero Pa, cadena
                        Estado = 3
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 3
                    Aceptado = 1
                    Elemento.Codigo = CODTERMINACION
                    Elemento.cadena = "�-�"
                    Estado = 255
            End Select
        Loop
        Estado = 0
    End If
    
    Rem ****************************************************
    Rem                 Formato "1/2-1/2"
    Rem ****************************************************
    If (Aceptado = 0) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "1") Then
                        IncPuntero Pa, cadena
                        Estado = 1
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 1
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "/") Then
                        IncPuntero Pa, cadena
                        Estado = 2
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 2
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "2") Then
                        IncPuntero Pa, cadena
                        Estado = 3
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 3
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-") Then
                        IncPuntero Pa, cadena
                        Estado = 4
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 4
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "1") Then
                        IncPuntero Pa, cadena
                        Estado = 5
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 5
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "/") Then
                        IncPuntero Pa, cadena
                        Estado = 6
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 6
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "2") Then
                        IncPuntero Pa, cadena
                        Estado = 7
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 7
                    Aceptado = 1
                    Elemento.Codigo = CODTERMINACION
                    Elemento.cadena = "�-�"
                    Estado = 255
            End Select
        Loop
        Estado = 0
    End If
    
    Rem ****************************************************
    Rem             Formatos "0.5-0.5" y ".5-.5"
    Rem ****************************************************
    If (Aceptado = 0) Then
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    Select Case (Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
                        Case "0"
                            IncPuntero Pa, cadena
                            Estado = 1
                            
                        Case "."
                            Estado = 1
                            
                        Case Else
                            Estado = 255
                            ReponePunteros cadena, Pc, Pa, Aux
                            
                    End Select
                    
                Case 1
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = ".") Then
                        IncPuntero Pa, cadena
                        Estado = 2
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 2
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "5") Then
                        IncPuntero Pa, cadena
                        Estado = 3
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 3
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "-") Then
                        IncPuntero Pa, cadena
                        Estado = 4
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 4
                    Select Case (Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
                        Case "0"
                            IncPuntero Pa, cadena
                            Estado = 5
                        
                        Case "."
                            Estado = 5
                            
                        Case Else
                            Estado = 255
                            ReponePunteros cadena, Pc, Pa, Aux
                    End Select
                    
                Case 5
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = ".") Then
                        IncPuntero Pa, cadena
                        Estado = 6
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 6
                    If (Mid(cadena(Pa.Segmento), Pa.Caracter, 1) = "5") Then
                        IncPuntero Pa, cadena
                        Estado = 7
                    Else
                        Estado = 255
                        ReponePunteros cadena, Pc, Pa, Aux
                    End If
                        
                Case 7
                    Aceptado = 1
                    Elemento.Codigo = CODTERMINACION
                    Elemento.cadena = "�-�"
                    Estado = 255
                    
            End Select
        Loop
    End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub MvtoSAN(cadena() As String, Pco As TIPOPUNTERO, _
                    Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                    Aceptado As Integer)
    Dim Estado As Integer
    Dim Movimiento As TIPOMOVIMIENTO
    Dim NumCar As Byte
    Dim Aux As TIPOPUNTERO
    On Error GoTo ControlError
    
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        Estado = 0
        NumCar = 0
        With Movimiento
            .CodigoPieza = 0
            .CasillaOrigen = 0
            .CasillaDestino = 0
        End With
        NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
        Aux = Pa
        Do While (Estado <> 255)
            Select Case (Estado)
                Case 0
                    Select Case (NumCar)
                        Case 80
                            ' *** Pe�n ***
                            If mobjBoard.Turno = BandoBlanco Then
                                Movimiento.CodigoPieza = CodPeonBlanco
                            ElseIf mobjBoard.Turno = BandoNegro Then
                                Movimiento.CodigoPieza = CodPeonNegro
                            End If
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                        Pa.Caracter, 1))
                            Estado = 1
                            
                        Case 82
                            Rem *** Torre ***
                            If mobjBoard.Turno = BandoBlanco Then
                                Movimiento.CodigoPieza = CodTorreBlanca
                            ElseIf mobjBoard.Turno = BandoNegro Then
                                Movimiento.CodigoPieza = CodTorreNegra
                            End If
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                            Pa.Caracter, 1))
                            Estado = 1
                            
                        Case 78
                            Rem *** Caballo ***
                            If mobjBoard.Turno = BandoBlanco Then
                                Movimiento.CodigoPieza = CodCaballoBlanco
                            ElseIf mobjBoard.Turno = BandoNegro Then
                                Movimiento.CodigoPieza = CodCaballoNegro
                            End If
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                        Pa.Caracter, 1))
                            Estado = 1
                            
                        Case 66
                            Rem *** Alfil ***
                            If mobjBoard.Turno = BandoBlanco Then
                                Movimiento.CodigoPieza = CodAlfilBlanco
                            ElseIf mobjBoard.Turno = BandoNegro Then
                                Movimiento.CodigoPieza = CodAlfilNegro
                            End If
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                        Pa.Caracter, 1))
                            Estado = 1
                            
                        Case 81
                            Rem *** Dama ***
                            If mobjBoard.Turno = BandoBlanco Then
                                Movimiento.CodigoPieza = CodDamaBlanca
                            ElseIf mobjBoard.Turno = BandoNegro Then
                                Movimiento.CodigoPieza = CodDamaNegra
                            End If
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                        Pa.Caracter, 1))
                            Estado = 1
                            
                        Case 75
                            Rem *** Rey ***
                            If mobjBoard.Turno = BandoBlanco Then
                                Movimiento.CodigoPieza = CodReyBlanco
                            ElseIf mobjBoard.Turno = BandoNegro Then
                                Movimiento.CodigoPieza = CodReyNegro
                            End If
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                        Pa.Caracter, 1))
                            Estado = 1
                            
                        Case Else
                            Rem *** Posibles columnas de origen ***
                            Rem No podemos permitir que un mvto. SAN
                            Rem comience por n�mero, seg�n standard
                            If (NumCar > 96 And NumCar < 105) Then
                                Rem *** Son columnas (Mvto. Pe�n) ***
                                Estado = 1
                                If mobjBoard.Turno = BandoBlanco Then
                                    Movimiento.CodigoPieza = CodPeonBlanco
                                ElseIf mobjBoard.Turno = BandoNegro Then
                                    Movimiento.CodigoPieza = CodPeonNegro
                                End If
                            Else
                                Rem *** Error ***
                                Estado = 255
                            End If
                    End Select
                    
                Case 1
                    Rem *** �Columnas de Origen? ***
                    If (NumCar > 96 And NumCar < 105) Then
                        Movimiento.CasillaOrigen = _
                            (Movimiento.CasillaOrigen + NumCar - 96) * 10
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
                        Estado = 2
                    Else
                        Rem *** �Filas de Origen? ***
                        Estado = 2
                    End If
                    
                Case 2
                    Select Case NumCar
                        Case 49 To 56
                            Rem *** Lee las filas ***
                            Movimiento.CasillaOrigen = _
                                Movimiento.CasillaOrigen + NumCar - 48
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                Pa.Caracter, 1))
                            Estado = 3
                            
                        Case 97 To 104
                            Estado = 4
                            
                        Case 120
                            Rem *** (lambda) ***
                            Estado = 3
                        
                        Case Else
                            Rem *** Error ***
                            Estado = 11
                            
                    End Select
                            
                Case 3
                    Select Case NumCar
                        Case 120
                            ' S�mbolo de captura (x)
                            IncPuntero Pa, cadena
                            NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                        Pa.Caracter, 1))
                            Estado = 4
                            
                        Case 97 To 104
                            ' lambda
                            Estado = 4
                            
                        Case Else '32, 160, 61, 43, 35, 33, 63
                            ' Aplicar en casos sin tratamiento
                            ' de ambig�edad
                            Movimiento.CasillaDestino = _
                                Movimiento.CasillaOrigen
                            Movimiento.CasillaOrigen = 0
                            Estado = 6
                        
                    End Select
                    
                Case 4
                    If NumCar > 96 And NumCar < 105 Then
                        Movimiento.CasillaDestino = _
                            (Movimiento.CasillaDestino + NumCar - 96) * 10
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
                        Estado = 5
                    Else
                        ' Error
                        Estado = 11
                    End If
                        
                Case 5
                    If NumCar > 48 And NumCar < 57 Then
                        Movimiento.CasillaDestino = _
                            Movimiento.CasillaDestino + NumCar - 48
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), _
                            Pa.Caracter, 1))
                        Estado = 6
                    Else
                        Rem *** Error ***
                        Estado = 11
                    End If
                    
                Case 6
                    ' S�mbolo "="
                    If NumCar = 61 Then
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), _
                            Pa.Caracter, 1))
                        Estado = 7
                    Else
                        Estado = 8
                    End If
                    
                Case 7
                    ' Pieza a promocionar
                    If NumCar = 82 Or NumCar = 78 Or _
                        NumCar = 66 Or NumCar = 81 Then
                        Rem *** Control de la pieza a promocionar ***
                        Select Case NumCar
                            Case 66
                                Rem *** Alfil ***
                                If mobjBoard.Turno = BandoBlanco Then
                                    Select Case Movimiento.CasillaDestino
                                        Case 18
                                            Movimiento.CasillaDestino = 202
                                        Case 28
                                            Movimiento.CasillaDestino = 206
                                        Case 38
                                            Movimiento.CasillaDestino = 210
                                        Case 48
                                            Movimiento.CasillaDestino = 214
                                        Case 58
                                            Movimiento.CasillaDestino = 218
                                        Case 68
                                            Movimiento.CasillaDestino = 222
                                        Case 78
                                            Movimiento.CasillaDestino = 226
                                        Case 88
                                            Movimiento.CasillaDestino = 230
                                    End Select
                                ElseIf mobjBoard.Turno = BandoNegro Then
                                    Rem *** Promociona el bando negro ***
                                    Select Case Movimiento.CasillaDestino
                                        Case 11
                                            Movimiento.CasillaDestino = 102
                                        Case 21
                                            Movimiento.CasillaDestino = 106
                                        Case 31
                                            Movimiento.CasillaDestino = 110
                                        Case 41
                                            Movimiento.CasillaDestino = 114
                                        Case 51
                                            Movimiento.CasillaDestino = 118
                                        Case 61
                                            Movimiento.CasillaDestino = 122
                                        Case 71
                                            Movimiento.CasillaDestino = 126
                                        Case 81
                                            Movimiento.CasillaDestino = 130
                                    End Select
                                End If
                            
                            Case 78
                                Rem *** Caballo ***
                                If mobjBoard.Turno = BandoBlanco Then
                                    Select Case Movimiento.CasillaDestino
                                        Case 18
                                            Movimiento.CasillaDestino = 204
                                        Case 28
                                            Movimiento.CasillaDestino = 208
                                        Case 38
                                            Movimiento.CasillaDestino = 212
                                        Case 48
                                            Movimiento.CasillaDestino = 216
                                        Case 58
                                            Movimiento.CasillaDestino = 220
                                        Case 68
                                            Movimiento.CasillaDestino = 224
                                        Case 78
                                            Movimiento.CasillaDestino = 228
                                        Case 88
                                            Movimiento.CasillaDestino = 232
                                    End Select
                                ElseIf mobjBoard.Turno = BandoNegro Then
                                    Rem *** promociona el bando negro ***
                                    Select Case Movimiento.CasillaDestino
                                        Case 11
                                            Movimiento.CasillaDestino = 104
                                        Case 21
                                            Movimiento.CasillaDestino = 108
                                        Case 31
                                            Movimiento.CasillaDestino = 112
                                        Case 41
                                            Movimiento.CasillaDestino = 116
                                        Case 51
                                            Movimiento.CasillaDestino = 120
                                        Case 61
                                            Movimiento.CasillaDestino = 124
                                        Case 71
                                            Movimiento.CasillaDestino = 128
                                        Case 81
                                            Movimiento.CasillaDestino = 132
                                    End Select
                                End If
                                
                            Case 81
                                Rem *** Dama ***
                                If mobjBoard.Turno = BandoBlanco Then
                                    Select Case Movimiento.CasillaDestino
                                        Case 18
                                            Movimiento.CasillaDestino = 201
                                        Case 28
                                            Movimiento.CasillaDestino = 205
                                        Case 38
                                            Movimiento.CasillaDestino = 209
                                        Case 48
                                            Movimiento.CasillaDestino = 213
                                        Case 58
                                            Movimiento.CasillaDestino = 217
                                        Case 68
                                            Movimiento.CasillaDestino = 221
                                        Case 78
                                            Movimiento.CasillaDestino = 225
                                        Case 88
                                            Movimiento.CasillaDestino = 229
                                    End Select
                                ElseIf mobjBoard.Turno = BandoNegro Then
                                    Rem *** Promociona el bando negro ***
                                    Select Case Movimiento.CasillaDestino
                                        Case 11
                                            Movimiento.CasillaDestino = 101
                                        Case 21
                                            Movimiento.CasillaDestino = 105
                                        Case 31
                                            Movimiento.CasillaDestino = 109
                                        Case 41
                                            Movimiento.CasillaDestino = 113
                                        Case 51
                                            Movimiento.CasillaDestino = 117
                                        Case 61
                                            Movimiento.CasillaDestino = 121
                                        Case 71
                                            Movimiento.CasillaDestino = 125
                                        Case 81
                                            Movimiento.CasillaDestino = 129
                                    End Select
                                End If
                                
                            Case 82
                                Rem *** Torre ***
                                If mobjBoard.Turno = BandoBlanco Then
                                    Select Case Movimiento.CasillaDestino
                                        Case 18
                                            Movimiento.CasillaDestino = 203
                                        Case 28
                                            Movimiento.CasillaDestino = 207
                                        Case 38
                                            Movimiento.CasillaDestino = 211
                                        Case 48
                                            Movimiento.CasillaDestino = 215
                                        Case 58
                                            Movimiento.CasillaDestino = 219
                                        Case 68
                                            Movimiento.CasillaDestino = 223
                                        Case 78
                                            Movimiento.CasillaDestino = 227
                                        Case 88
                                            Movimiento.CasillaDestino = 231
                                    End Select
                                ElseIf mobjBoard.Turno = BandoNegro Then
                                    Rem *** Promociona el bando negro ***
                                    Select Case Movimiento.CasillaDestino
                                        Case 11
                                            Movimiento.CasillaDestino = 103
                                        Case 21
                                            Movimiento.CasillaDestino = 107
                                        Case 31
                                            Movimiento.CasillaDestino = 111
                                        Case 41
                                            Movimiento.CasillaDestino = 115
                                        Case 51
                                            Movimiento.CasillaDestino = 119
                                        Case 61
                                            Movimiento.CasillaDestino = 123
                                        Case 71
                                            Movimiento.CasillaDestino = 127
                                        Case 81
                                            Movimiento.CasillaDestino = 131
                                    End Select
                                End If
                            Case Else
                                ' *** Error ***
                                Estado = 11
                        End Select
                                               
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                    Pa.Caracter, 1))
                        Estado = 8
                        
                    Else
                        Estado = 9
                    End If
                    
                Case 8
                    Rem *** S�mbolos de jaque y mate "+" y "#" ***
                    If (NumCar = 43 Or NumCar = 35) Then
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                    Pa.Caracter, 1))
                    Else
                        Estado = 9
                    End If
                    
                Case 9
                    Rem *** S�mbolos "!" y "?" ***
                    Do While (NumCar = 33 Or NumCar = 63)
                        IncPuntero Pa, cadena
                        NumCar = Asc(Mid(cadena(Pa.Segmento), _
                                    Pa.Caracter, 1))
                    Loop
                    Estado = 10
                        
                Case 10
                    Aceptado = 1
                    With Movimiento
                        If ((.CasillaOrigen Mod 10) = 0 Or _
                            .CasillaOrigen < 10) Then
                            ' Se averigua la casilla de origen
                            ' en los movimientos que nos vengan
                            ' especificados.
    '                        CompletaCoordenadas Movimiento, mobjBoard.Escaque
                            mobjBoard.CompletaCoordenadas .CodigoPieza, _
                                    .CasillaOrigen, .CasillaDestino
                        End If
                    End With
                    Elemento.Codigo = CODSAN
                    If ((Movimiento.CasillaOrigen Mod 10) = 0 Or _
                        Movimiento.CasillaOrigen < 10) Then
                        ' Se trata de un movimiento err�neo
                        ' y se trata como tal.
                        Elemento.cadena = "9999"
                        CadMvtoSAN = "Error"
                    Else
                        Elemento.cadena = Movimiento.CasillaOrigen & _
                            Movimiento.CasillaDestino
                        If Aux.Segmento = Pa.Segmento Then
                            CadMvtoSAN = Mid(cadena(Pa.Segmento), _
                                Aux.Caracter, Pa.Caracter - Aux.Caracter)
                        Else
                            CadMvtoSAN = Mid(cadena(Aux.Segmento), _
                                Aux.Caracter, Len(cadena(Aux.Segmento))) & _
                                Mid(cadena(Pa.Segmento), 1, Pa.Caracter)

                        End If
                    End If
                    Estado = 255
                    
                Case 11
                    ' Se trata de un movimiento erroneo y no se debe
                    ' considerar como un token de MvtoSAN.
                    ' Lo que se hace entonces es asignarle el valor 99
                    ' a las coordenadas de movimiento (CasillaOrigen y
                    ' CasillaDestino) de modo que as� indicaremos  que
                    ' la partida solo se toma como valida hasta el �ltimo
                    ' movimiento correcto y los dem�s tokens de mvto San
                    ' ser�n tomados como identificadores y no registrados.
                    
                    
                    Aceptado = 1
                    Elemento.Codigo = CODSAN
                    Elemento.cadena = "9999"
                    CadMvtoSAN = "Error"
                    Estado = 255
                    
            End Select
        Loop
    End If

GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume Next

End Sub

Private Sub CaracteresEsp(cadena() As String, Pc As TIPOPUNTERO, _
                            Pa As TIPOPUNTERO, Elemento As DEFTOKEN, _
                            Aceptado As Integer)
    On Error GoTo ControlError
    
    If (Aceptado = 0 And Elemento.Codigo <> CODCONTROL4) Then
        Select Case (Mid(cadena(Pa.Segmento), Pa.Caracter, 1))
            Case "["
                Elemento.Codigo = CODABRECORCH
                Elemento.cadena = "["
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case "]"
                Elemento.Codigo = CODCIERRACORCH
                Elemento.cadena = "]"
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case "$"
                Elemento.Codigo = CODDOLAR
                Elemento.cadena = "$"
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case "("
                Elemento.Codigo = CODABREPAR
                Elemento.cadena = "("
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case ")"
                Elemento.Codigo = CODCIERRAPAR
                Elemento.cadena = ")"
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case ";"
                Elemento.Codigo = CODPUNTOCOMA
                Elemento.cadena = ";"
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case "."
                Elemento.Codigo = CODPUNTO
                Elemento.cadena = "."
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case "%"
                Elemento.Codigo = CODESCAPE
                Elemento.cadena = "%"
                Aceptado = 1
                IncPuntero Pa, cadena
                
            Case Else
                Elemento.Codigo = CODCOMODIN
                Elemento.cadena = Mid(cadena(Pa.Segmento), _
                    Pa.Caracter, 1)
                Aceptado = 1
                IncPuntero Pa, cadena
                
        End Select
    End If
                
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub ReponePunteros(Tira() As String, Pc As TIPOPUNTERO, _
                            Pa As TIPOPUNTERO, Px As TIPOPUNTERO)
    ' Este procedimiento se utiliza para cuando es necesario
    ' avance retroceda hasta el puntero de comienzo porque se
    ' ha intentado leer un token con un aut�mata err�neo.
    ' Se tiene en cuenta si hay que retroceder hasta el buffer
    ' anterior y no se quiere perder la informaci�n almacenada
    ' en el buffer actual.
    On Error GoTo ControlError
    
    If Pa.Segmento = Px.Segmento Then
        Pa = Px
    Else
        Tira(Pa.Segmento) = Mid(Tira(Px.Segmento), _
            Px.Caracter, Len(Tira(Px.Segmento)) + 1 - Px.Caracter) & _
            Tira(Pa.Segmento)
        Pc = Pa
        Px = Pc
    End If
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Function Lector(Buffer() As String, Pco As TIPOPUNTERO, _
                        Pav As TIPOPUNTERO) As DEFTOKEN
    Dim Reconocido      As Integer
    Dim Util            As Byte
    Dim CaracterComienzo    As String * 1
    Dim CaracterAvance      As String * 1
    On Error GoTo ControlError
    
    Reconocido = 0
    Lector.Codigo = CODCONTROL3
    Lector.cadena = ""
    If Pav.Caracter = 0 And Not EOF(1) Then
        CargaBloque Buffer(Pav.Segmento)
        Pav.Caracter = 1
    End If
    
    Rem *** Mientras exista error lexicogr�fico ***
    Do While (Lector.Codigo = CODCONTROL3)
        CaracterAvance = Mid(Buffer(Pav.Segmento), Pav.Caracter, 1)
        Do While (CaracterAvance = " " Or _
                CaracterAvance = Chr(10) Or _
                CaracterAvance = Chr(13) Or _
                CaracterAvance = Chr(9)) And Not EOF(1)
            Rem *** Elimina espacios en blanco ***
            IncPuntero Pav, Buffer
            CaracterAvance = Mid(Buffer(Pav.Segmento), Pav.Caracter, 1)
        Loop
        
        If EOF(1) Then
            Rem *** Final de fichero ***
            Lector.Codigo = CODCONTROL1
        Else
            
            Pco = Pav
            Rem *** Llamadas a los aut�matas ***
            Terminacion Buffer, Pco, Pav, Lector, Reconocido
            If (EvaluaEtiquetas) Then
                Identificador Buffer, Pco, Pav, Lector, Reconocido
            Else
                MvtoSAN Buffer, Pco, Pav, Lector, Reconocido
            End If
            Entero Buffer, Pco, Pav, Lector, Reconocido
            cadena Buffer, Pco, Pav, Lector, Reconocido
            Comentario Buffer, Pco, Pav, Lector, Reconocido
            Enroque Buffer, Pco, Pav, Lector, Reconocido
            CaracteresEsp Buffer, Pco, Pav, Lector, Reconocido
            Rem *********************************
            
        End If
    Loop
  
GetExit:
    Exit Function
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
  
End Function

Private Sub CargaBloque(Bloque As String)
    ' Carga un bloque de caracteres desde disco
    ' y adem�s lo optimiza eliminando caracteres
    ' especiales: tabulaci�n, retroceso, avance
    ' de l�nea y retorno de carro.
    Dim Ncar        As Integer
    Dim CadenaAux   As String
    On Error GoTo ControlError
    
    Select Case LOF(1) - Seek(1)
        Case Is > LONGBLOQUE
            Bloque = Input(LONGBLOQUE, 1)
        Case 1, 0
            Bloque = Input(1, 1)
        Case Else
            Bloque = Input(LOF(1) - Seek(1) - 1, 1)
    End Select
    
    For Ncar = 1 To Len(Bloque)
        Select Case Mid(Bloque, Ncar, 1)
            Case Chr(8), Chr(9), Chr(10), Chr(13)
                ' Introducimos un caracter de espacio
                ' para no invalidar ciertas
                ' separaciones entre caracteres
                CadenaAux = CadenaAux & Space(1)
            Case Else
                CadenaAux = CadenaAux & Mid(Bloque, Ncar, 1)
        End Select
    Next Ncar
    Bloque = CadenaAux
    
GetExit:
    Exit Sub
    
ControlError:
'    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
'        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
'        vbInformation, " " & Err.Source
    Resume Next
'    Resume GetExit

End Sub

Private Sub IncPuntero(Apuntador As TIPOPUNTERO, Tira() As String)
    On Error GoTo ControlError
    With Apuntador
        If .Caracter >= Len(Tira(.Segmento)) Then
            .Caracter = 1
            If .Segmento = 1 Then
                .Segmento = 2
            Else
                .Segmento = 1
            End If
            Call CargaBloque(Tira(.Segmento))
        Else
            .Caracter = .Caracter + 1
        End If
    End With
    
GetExit:
    Exit Sub
    
ControlError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub TrataError(Codigo As Byte)
    Dim strCausa As String
    Dim strBando As String
    
    If mobjBoard.Turno = BandoBlanco Then
        strBando = "blanco"
    ElseIf mobjBoard.Turno = BandoNegro Then
        strBando = "negro"
    End If
    
    Select Case Codigo
        Case 1
            strCausa = "Secci�n de cabecera." & _
                        " Falta el caracter '['."
        Case 2
            strCausa = "Secci�n de cabecera." & _
                        " Falta el caracter ']'."
        Case 3
            strCausa = "Secci�n de cabecera." & _
                        " Falta identificador de etiqueta."
        Case 4
            strCausa = "Secci�n de cabecera." & _
                        " Falta cadena de valor de etiqueta."
        Case 5
            strCausa = "Secci�n de movimientos." & _
                        " Par�ntesis de variante sin cerrar."
        Case 6
            strCausa = "Secci�n de movimientos, " & _
                        "movimiento n� " & mobjBoard.NumMovto & _
                        " bando " & strBando & _
                        ". Movimiento SAN incorrecto."
        Case 7
            strCausa = "Caracter ' " & _
                        Token.cadena & "' no esperado."
    End Select
     
    MsgBox "Error en la partida PGN" & ", " & _
            strCausa & Chr(13) & Chr(10) & Chr(13) & _
            Chr(10), vbCritical, _
            "Error en la partida PGN."
                
    
End Sub

