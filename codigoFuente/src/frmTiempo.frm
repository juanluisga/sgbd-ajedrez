VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Begin VB.Form frmTiempo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Definir el intervalo de reproducción automática."
   ClientHeight    =   2415
   ClientLeft      =   30
   ClientTop       =   330
   ClientWidth     =   4980
   Icon            =   "frmTiempo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2415
   ScaleWidth      =   4980
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdTp 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Index           =   2
      Left            =   3375
      Picture         =   "frmTiempo.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Ayuda"
      Top             =   1560
      Width           =   885
   End
   Begin VB.CommandButton cmdTp 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Index           =   1
      Left            =   2047
      Picture         =   "frmTiempo.frx":0614
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Cancelar"
      Top             =   1560
      Width           =   885
   End
   Begin ComctlLib.Slider sldTp 
      Height          =   420
      Left            =   720
      TabIndex        =   0
      Top             =   288
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   741
      _Version        =   327680
      MouseIcon       =   "frmTiempo.frx":091E
      Min             =   1
      Max             =   20
      SelStart        =   1
      Value           =   1
   End
   Begin VB.CommandButton cmdTp 
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Index           =   0
      Left            =   720
      Picture         =   "frmTiempo.frx":093A
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Aceptar"
      Top             =   1560
      Width           =   885
   End
   Begin VB.Image imgTp 
      Height          =   480
      Index           =   1
      Left            =   4320
      Picture         =   "frmTiempo.frx":0C44
      Stretch         =   -1  'True
      Top             =   180
      Width           =   480
   End
   Begin VB.Image imgTp 
      Height          =   480
      Index           =   0
      Left            =   120
      Picture         =   "frmTiempo.frx":0F4E
      Top             =   180
      Width           =   480
   End
   Begin VB.Label lblTp 
      Caption         =   "Segs."
      Height          =   228
      Left            =   216
      TabIndex        =   7
      Top             =   720
      Width           =   444
   End
   Begin VB.Label lblEscala 
      AutoSize        =   -1  'True
      Caption         =   "20"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Index           =   4
      Left            =   4032
      TabIndex        =   6
      Top             =   720
      Width           =   204
   End
   Begin VB.Label lblEscala 
      AutoSize        =   -1  'True
      Caption         =   "15"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Index           =   3
      Left            =   3168
      TabIndex        =   5
      Top             =   720
      Width           =   204
   End
   Begin VB.Label lblEscala 
      AutoSize        =   -1  'True
      Caption         =   "10"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Index           =   2
      Left            =   2304
      TabIndex        =   4
      Top             =   720
      Width           =   204
   End
   Begin VB.Label lblEscala 
      AutoSize        =   -1  'True
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Index           =   1
      Left            =   1512
      TabIndex        =   3
      Top             =   720
      Width           =   108
   End
   Begin VB.Label lblEscala 
      AutoSize        =   -1  'True
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Index           =   0
      Left            =   864
      TabIndex        =   2
      Top             =   720
      Width           =   108
   End
End
Attribute VB_Name = "frmTiempo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdTp_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Aceptar
            Unload Me
        Case 1
            ' Cancelar
            sldTp.Value = 0
            Unload Me
        Case 2
            ' Ayuda
            frmPadre.MuestraAyuda HelpContextID
    End Select
End Sub

Private Sub Form_Load()

    HelpContextID = CT_HELP_TIEMPO
    
    sldTp.Value = frmPartidas.tmrPt.Interval / 1000
End Sub

Private Sub sldTp_Change()
    frmPartidas.tmrPt.Interval = sldTp.Value * 1000
End Sub
