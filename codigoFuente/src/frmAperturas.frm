VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form frmAperturas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aperturas"
   ClientHeight    =   6510
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8130
   HelpContextID   =   2
   Icon            =   "frmAperturas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6510
   ScaleWidth      =   8130
   Tag             =   "Aperturas"
   Begin TabDlg.SSTab SSTab1 
      Height          =   6495
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   11456
      _Version        =   327680
      TabHeight       =   520
      TabCaption(0)   =   "&Datos Generales"
      TabPicture(0)   =   "frmAperturas.frx":030A
      Tab(0).ControlCount=   4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraApe(2)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdApe(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraApe(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraApe(0)"
      Tab(0).Control(3).Enabled=   0   'False
      TabCaption(1)   =   "&Estad�sticas"
      TabPicture(1)   =   "frmAperturas.frx":0326
      Tab(1).ControlCount=   1
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "chApe"
      Tab(1).Control(0).Enabled=   0   'False
      TabCaption(2)   =   "&Partidas"
      TabPicture(2)   =   "frmAperturas.frx":0342
      Tab(2).ControlCount=   3
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdApe(2)"
      Tab(2).Control(0).Enabled=   -1  'True
      Tab(2).Control(1)=   "cmdApe(1)"
      Tab(2).Control(1).Enabled=   -1  'True
      Tab(2).Control(2)=   "lvwPt"
      Tab(2).Control(2).Enabled=   0   'False
      Begin VB.CommandButton cmdApe 
         Height          =   720
         Index           =   2
         Left            =   -68760
         Picture         =   "frmAperturas.frx":035E
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Actualizar lista"
         Top             =   5640
         Width           =   735
      End
      Begin VB.CommandButton cmdApe 
         Height          =   720
         Index           =   1
         Left            =   -67800
         Picture         =   "frmAperturas.frx":11A0
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Ver partida"
         Top             =   5640
         Width           =   735
      End
      Begin MSChartLib.MSChart chApe 
         Height          =   4995
         Left            =   -74760
         OleObjectBlob   =   "frmAperturas.frx":14AA
         TabIndex        =   8
         Top             =   480
         Width           =   7755
      End
      Begin VB.Frame fraApe 
         Caption         =   "Identificaci�n de la apertura"
         Height          =   900
         Index           =   0
         Left            =   180
         TabIndex        =   14
         Top             =   360
         Width           =   6930
         Begin VB.TextBox txtApe 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   0
            Left            =   150
            TabIndex        =   0
            Top             =   435
            Width           =   1350
         End
         Begin VB.TextBox txtApe 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   1650
            TabIndex        =   1
            Top             =   450
            Width           =   5115
         End
         Begin VB.Label lblApe 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            Height          =   195
            Index           =   0
            Left            =   150
            TabIndex        =   16
            Top             =   225
            Width           =   495
         End
         Begin VB.Label lblApe 
            AutoSize        =   -1  'True
            Caption         =   "Denominaci�n"
            Height          =   195
            Index           =   1
            Left            =   1650
            TabIndex        =   15
            Top             =   225
            Width           =   1020
         End
      End
      Begin VB.Frame fraApe 
         Caption         =   "Datos de la apertura"
         Height          =   2610
         Index           =   1
         Left            =   180
         TabIndex        =   11
         Top             =   1335
         Width           =   7770
         Begin VB.TextBox txtApe 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2040
            Index           =   2
            Left            =   2220
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   450
            Width           =   5385
         End
         Begin VB.ComboBox cboApe 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   180
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   450
            Width           =   1935
         End
         Begin VB.Label lblApe 
            AutoSize        =   -1  'True
            Caption         =   "Tipo"
            Height          =   195
            Index           =   2
            Left            =   150
            TabIndex        =   13
            Top             =   225
            Width           =   315
         End
         Begin VB.Label lblApe 
            AutoSize        =   -1  'True
            Caption         =   "Notas"
            Height          =   195
            Index           =   3
            Left            =   2220
            TabIndex        =   12
            Top             =   225
            Width           =   420
         End
      End
      Begin VB.CommandButton cmdApe 
         Default         =   -1  'True
         Height          =   780
         Index           =   0
         Left            =   7140
         Picture         =   "frmAperturas.frx":34B6
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Width           =   795
      End
      Begin VB.Frame fraApe 
         Caption         =   "Aperturas"
         Height          =   2325
         Index           =   2
         Left            =   180
         TabIndex        =   6
         Top             =   4020
         Width           =   7770
         Begin ComctlLib.ListView lvwApes 
            Height          =   1950
            Left            =   150
            TabIndex        =   5
            Top             =   225
            Width           =   7470
            _ExtentX        =   13176
            _ExtentY        =   3440
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   327680
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "C�digo"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               SubItemIndex    =   1
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Denominaci�n"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               SubItemIndex    =   2
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Tipo"
               Object.Width           =   1764
            EndProperty
         End
      End
      Begin ComctlLib.ListView lvwPt 
         Height          =   5070
         Left            =   -74820
         TabIndex        =   9
         Top             =   480
         Width           =   7770
         _ExtentX        =   13705
         _ExtentY        =   8943
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327680
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Blancas"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Negras"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   2
            SubItemIndex    =   2
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Resultado"
            Object.Width           =   1411
         EndProperty
      End
   End
End
Attribute VB_Name = "frmAperturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Defino un tipo de datos "Criterio" para poder almacenar todos
' los posibles criterios de la �ltima b�squeda y poder compararlos
' a la hora de refrescar la lista con los nuevos datos despu�s de
' una actualizaci�n.
Private Type Criterio
    Id As String
    Den As String
    Tipo As String * 1
End Type

'' ---------------------------------------------------------------------
'' Para poder se�alar un fila completa del ListView
'Private Declare Function SendMessage Lib "user32" Alias _
'        "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal _
'        wParam As Long, lParam As Any) As Long
'Const LVS_EX_FULLROWSELECT = &H20
'Const LVM_FIRST = &H1000
'Const LVM_GETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + &H37
'Const LVM_SETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + &H36
'' ---------------------------------------------------------------------

Private RsApe   As Recordset
Private Busqueda As Criterio
Public Estado As String
Private ReLoad As Boolean
' Variable para controlar cambios
Private blnChange As Boolean


Private Sub InicioForm()
' **********************************************************************************
' Nombre.....................: InicioForm
' Tipo............................: Sub
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio........: 7/8/99
' Descripci�n.............: Crea objetos y configura el inicio del formulario.
' ***********************************************************************************
    Dim lngStyle As Long
    
    ' Dimensionamos el formulario.
    Width = 8220
    Height = 6885
    
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_APERTURAS
    
    ' Para poder se�alar un fila completa del ListView
    lngStyle = SendMessage(lvwApes.hWnd, _
              LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    lngStyle = lngStyle Or LVS_EX_FULLROWSELECT
    Call SendMessage(lvwApes.hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, ByVal lngStyle)
    
    lngStyle = SendMessage(lvwPt.hWnd, _
              LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    lngStyle = lngStyle Or LVS_EX_FULLROWSELECT
    Call SendMessage(lvwPt.hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, ByVal lngStyle)
   
    ReLoad = False
    frmAbiertos = frmAbiertos + 1
    LoadCboTipo
    DisableAllFields
End Sub

Private Sub cboApe_Change()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub cmdApe_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Bot�n "Buscar Ahora".
            ThrowSearch
        Case 1
            ' Hiperv�nculo con
            ' el formulario "Partidas"
            Dim mItem As ListItem
            Set mItem = lvwPt.SelectedItem
            If Not mItem Is Nothing Then
                frmPartidas.IdPartida = _
                    Mid$(mItem.Key, 2, Len(mItem.Key))
            End If
        Case 2
            ' Actualizar lista de partidas
            Clock 1
            ShowResults RsApe!ap_id
            Clock 0
    End Select
End Sub

Private Sub Form_Load()
    InicioForm
End Sub
Private Sub DisableAllFields()
'**********************************************************************************
' Nombre....................: DisableAllFields
' Tipo...........................: Sub
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno.....: Ninguno
' �ltimo cambio.........: 7/8/99
' Descripci�n..............: Deshabilita todos los controles del formulario.
'**********************************************************************************
    Dim ctlTextBox As TextBox
    
    For Each ctlTextBox In txtApe
        DesActivarTxt ctlTextBox
    Next
    DesActivarLstView lvwApes
    DesActivarCbo cboApe
    cmdApe(0).Enabled = False
    With SSTab1
        .TabEnabled(0) = False
        .TabEnabled(1) = False
        .TabEnabled(2) = False
    End With
End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub FinForm()
'****************************************************************************
' Nombre........................: FinForm
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio.............: 5/8/99
' Descripci�n..................: Realiza operaciones al cerrar el formulario:
' ..........................................: grabar cambios, destruir objetos y control de
' ..........................................: formularios abiertos.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    Set RsApe = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub ThrowSearch()
'****************************************************************************
' Nombre.......................: ThrowSearch
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 7/8/99
' Descripci�n................: Lanza el proceso de b�squeda.
'****************************************************************************
    Dim strSQL As String
    
    Clock 1
    With Busqueda
        .Id = txtApe(0) & "*"
        .Den = "*" & txtApe(1) & "*"
        .Tipo = Left$(cboApe.Text, 1) & "*"
    End With
    If Not (Trim$(txtApe(0)) = "") Or Not (Trim$(txtApe(1)) = "") Or _
        Not (cboApe.ListIndex = -1) Then
        ' Buscar por los criterios seleccionados.
        With Busqueda
            strSQL = "SELECT * FROM Aperturas WHERE ap_codigo LIKE " & _
                SnglQuote(.Id) & " AND ap_den LIKE " & SnglQuote(.Den) & _
                " AND ap_tipo LIKE " & SnglQuote(.Tipo) & " ORDER BY ap_den"
        End With
    Else
        ' Buscar todos
        strSQL = "Aperturas"
    End If
    Set RsApe = dbDatos.OpenRecordset(strSQL)
    If Not RsApe.EOF Then
        DisplayRegs RsApe
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    Clock 0
End Sub

Private Sub DisplayRegs(Rs As Recordset)
'****************************************************************************
' Nombre.......................: DisplayRegs
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Recordset
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 7/8/99
' Descripci�n................: Muestra en la lista los registros seleccionados
' ........................................: en el proceso de b�squeda.
'****************************************************************************

    Dim mItem As ListItem
    
    lvwApes.ListItems.Clear
    With Rs
        Do While Not Rs.EOF
            Set mItem = lvwApes.ListItems.Add(, "X" & CStr(!ap_id), !ap_codigo)
            mItem.Tag = CStr(!ap_id)
            mItem.SubItems(1) = !ap_den
            Select Case LCase(CheckForNull(!ap_tipo, "STRING"))
                Case "a"
                    mItem.SubItems(2) = "Abierta"
                Case "s"
                    mItem.SubItems(2) = "Semiabierta"
                Case "c"
                    mItem.SubItems(2) = "Cerrada"
            End Select
            .MoveNext
        Loop
    End With
End Sub

Public Sub Buscar()
'****************************************************************************
' Nombre.......................: Buscar
' Tipo..............................: Sub
' �mbito.........................: Publico
' Par�metros................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 7/8/99
' Descripci�n................: Dispone el proceso de b�squeda.
'****************************************************************************

    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub SetSearchControls()
'****************************************************************************
' Nombre..................: SetSearchControls
' Tipo.........................: Sub
' �mbito....................: Privado
' Par�metros...........: Ninguno
' Valor de retorno..: Ninguno
' �ltimo cambio......: 8/8/99
' Descripci�n...........: Habilita los campos del formulario para que
'....................................: el usuario pueda hacer b�squedas.
'****************************************************************************
    
    ActivarTxt txtApe(0)
    txtApe(0).SetFocus
    ActivarTxt txtApe(1)
    ActivarCbo cboApe
    cmdApe(0).Enabled = True
    DesActivarLstView lvwApes
    DesActivarTxt txtApe(2)
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
        .TabEnabled(2) = False
    End With
End Sub

Private Sub SetSelectControls()
'****************************************************************************
' Nombre.......................: SetSelectControls
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 7/8/99
' Descripci�n................: Dispone los controles de formulario para
' ........................................: que el usuario pueda seleccionar entre
' ........................................: los registros seleccionados en el proceso
' ........................................: de b�squeda.
'****************************************************************************

    Dim ctlTextBox As TextBox
    
    mobjControlMDI.Buscar
    For Each ctlTextBox In txtApe
        DesActivarTxt ctlTextBox
    Next
    DesActivarCbo cboApe
    ActivarLstView lvwApes
    cmdApe(0).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
        .TabEnabled(2) = False
    End With
End Sub

Public Sub Cancelar()
'****************************************************************************
' Nombre.......................: Cancelar
' Tipo..............................: Sub
' �mbito.........................: P�blico
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 4/8/99
' Descripci�n...............: Cancela la acci�n que se estuviera
' ........................................: realizando y vuelve al estado de espera.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
End Sub

Private Sub ClearFields()
'**************************************************************************************
' Nombre.......................: ClearFields
' Tipo..............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 8/8/99
' Descripci�n................: Borra el contenido de los controles del formulario.
'**************************************************************************************
    Dim ctlTextBox As TextBox
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    
    For Each ctlTextBox In txtApe
        ctlTextBox = ""
        ctlTextBox.Tag = ""
    Next
    cboApe.ListIndex = -1
    blnChange = False
End Sub


Private Sub LoadCboTipo()
'*******************************************************************************************
' Nombre.......................: LoadCboTipo
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 8/8/99
' Descripci�n................: Carga el contenido del Combo de tipos de aperturas.
'*******************************************************************************************

    With cboApe
        .AddItem ""
        .AddItem "Abierta"
        .AddItem "Semiabierta"
        .AddItem "Cerrada"
    End With
End Sub

Private Function SearchCboTipo(strInicial As String) As String
'****************************************************************************
' Nombre.......................: SearchCboTipo
' Tipo..............................: Function
' �mbito..........................: Privado
' Par�metros................: strInicial -> Inicial del tipo de apertura
' Valor de retorno.......: Tipo de apertura
' �ltimo cambio..........: 8/8/99
' Descripci�n...............: Busca en el combo de tipos de aperturas
' .......................................: la denominaci�n de la apertura cuya inicial
' .......................................: se pasa como par�metro.
'****************************************************************************

    Dim NumItems As Byte
    Dim Idx As Byte
    
    With cboApe
        NumItems = .ListCount - 1
        Idx = 0
        Do While Idx <= NumItems
            If Left$(.List(Idx), 1) = strInicial Then
                .ListIndex = Idx
                Exit Function
            Else
                Idx = Idx + 1
            End If
        Loop
        .ListIndex = -1
    End With
End Function

Public Sub Borrar()
'****************************************************************************
' Nombre......................: Borrar
' Tipo.............................: Sub
' �mbito........................: P�blico
' Par�metros...............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio..........: 5/8/99
' Descripci�n...............: Da de baja el registro seleccionado.
'****************************************************************************
    Dim Respuesta   As Integer

    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
End Sub


Private Sub Delete()
    
    On Error GoTo errDel
    
    With lvwApes
         .ListItems.Remove .SelectedItem.Index
    End With
    RsApe.Delete
    ClearFields
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number
End Sub

Private Sub lvwApes_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
    ' Ordenar el lvw.
    With lvwApes
        .SortKey = ColumnHeader.Index - 1
        .Sorted = True
    End With
End Sub

Private Sub lvwApes_ItemClick(ByVal Item As ComctlLib.ListItem)
    ' Mostrar el registro seleccionado en la lista.
    
    Clock 1
    CheckChanges blnChange
    If RsApe.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        RsApe.Index = "ap_id"
        RsApe.Seek "=", Mid$(Item.Key, 2, Len(Item.Key))
    Else
        ' Utilizamos el m�todo FIND si el Recordset es DYNASET o SNAPSHOT
        RsApe.FindFirst "ap_id =" & Mid$(Item.Key, 2, Len(Item.Key))
    End If
    SetEditControls
    mobjControlMDI.Edicion
    DisplayFields
    Clock 0

End Sub

Private Sub SetEditControls()
'****************************************************************************
' Nombre......................: SetEditControls
' Tipo.............................: Sub
' �mbito........................: Privado
' Par�metros...............: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 8/8/99
' Descripci�n................: Habilita los controles necesarios para
'.........................................: operaciones de busqueda y alta.
'****************************************************************************
    
'    DesActivarTxt txtApe(0)
    ActivarTxt txtApe(0)
    txtApe(0).SetFocus
    ActivarTxt txtApe(1)
    txtApe(0).SetFocus
    ActivarTxt txtApe(2)
    ActivarCbo cboApe
    cmdApe(0).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = True
        .TabEnabled(2) = True
    End With
End Sub

Private Sub DisplayFields()
'****************************************************************************
' Nombre....................: DisplayFields
' Tipo...........................: Sub
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 8/8/99
' Descripci�n..............: Muestra en el form los datos del registro le�do.
'****************************************************************************

    ClearFields
    With RsApe
        txtApe(0) = CheckForNull(!ap_codigo, "STRING")
        txtApe(1) = CheckForNull(!ap_den, "STRING")
        txtApe(2) = CheckForNull(!ap_info, "STRING")
        SearchCboTipo (Left$(CheckForNull(!ap_tipo, "STRING"), 1))
        ShowStatistics !ap_id ' RsApe!ap_codigo
        ShowResults !ap_id
    End With
    blnChange = False

End Sub

Public Sub Grabar()
'*********************************************************************************************
' Nombre.......................: Grabar
' Tipo..............................: Sub
' �mbito.........................: P�blico
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 8/8/99
' Descripci�n................: Graba un reg. en la BB.DD. con los datos introducidos.
'*********************************************************************************************
    Dim mItem As ListItem
    
    ' Si el objeto RsApe a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If RsApe Is Nothing Then _
        Set RsApe = dbDatos.OpenRecordset("Aperturas", dbOpenDynaset)
    If Not DatosValidos() Then Exit Sub
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            RsApe.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            RsApe.Edit
    End Select

    AsignarValores
    RsApe.Update
    If Estado = "EDICION" Then
            With RsApe
'                Set mItem = lvwApes.SelectedItem
                Set mItem = lvwApes.FindItem(CStr(!ap_id), lvwTag)
                If Not mItem Is Nothing Then
                    mItem.Text = !ap_codigo
                    mItem.SubItems(1) = !ap_den
                    Select Case LCase(CheckForNull(!ap_tipo, "STRING"))
                        Case "a"
                            mItem.SubItems(2) = "Abierta"
                        Case "s"
                            mItem.SubItems(2) = "Semiabierta"
                        Case "c"
                            mItem.SubItems(2) = "Cerrada"
                    End Select
                End If
            End With
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    ClearFields
    SetSelectControls
    mobjControlMDI.Espera
End Sub

Private Function DatosValidos() As Boolean
'*********************************************************************************************
' Nombre.......................: DatosValidos
' Tipo..............................: Function
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Boolean
' �ltimo cambio...........: 8/8/99
' Descripci�n................: Realiza una validaci�n de los datos introducidos antes
' ........................................: de proceder a su grabaci�n, para evitar errores.
'*********************************************************************************************
 
Dim msg$, cCtrlErr As Control, iCod%, j%

    msg = "": iCod = 0: DatosValidos = True
    

    ' Denominaci�n de apertura
    If iCod = 0 And Len(txtApe(1)) = 0 Then
        Set cCtrlErr = txtApe(1)
        msg = "La denominaci�n de apertura es obligatoria."
        iCod = 3
    ElseIf Len(txtApe(1)) > 50 Then
        Set cCtrlErr = txtApe(1)
        msg = "Longitud m�x. 50 caracteres."
        iCod = 4
    End If

    ' Notas de la apertura
    If iCod = 0 And Len(txtApe(2)) > 2500 Then
        Set cCtrlErr = txtApe(2)
        msg = "Longitud m�x. 2500 caracteres."
        iCod = 5
    End If
    
    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
End Function

Private Sub AsignarValores()
'******************************************************************
' Nombre.......................: AsignarValores
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 8/8/99
' Descripci�n...............: Asigna los valores que
' .......................................: el usuario ha introducido en el
'........................................: formulario a la clase.
'*******************************************************************
    With RsApe
        !ap_codigo = txtApe(0)
        !ap_den = txtApe(1)
        !ap_tipo = Left$(cboApe.Text, 1)
        !ap_info = txtApe(2)
    End With
End Sub

Public Sub Insertar()
'*********************************************************************************************
' Nombre.......................: Insertar
' Tipo..............................: Sub
' �mbito.........................: P�blico
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 8/8/99
' Descripci�n................: Dispone al formulario para dar de alta a un registro.
'*********************************************************************************************

    mobjControlMDI.Alta
    SetAddControls
    ClearFields
End Sub

Private Sub txtApe_Change(Index As Integer)
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub txtApe_GotFocus(Index As Integer)
    txtApe(Index).BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtApe_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            ' Si pulsamos letras min�sculas la pasamos a
            ' may�sculas.
            If (KeyAscii > 96 And KeyAscii < 123) Then _
                KeyAscii = KeyAscii - 32
    End Select
End Sub

Private Sub SetAddControls()
'****************************************************************************
' Nombre.......................: SetAddControls
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 12/8/99
' Descripci�n................: Habilita los controles necesarios para
'.........................................: operaciones de busqueda y alta.
'*****************************************************************************
    Dim ctlTextBox As TextBox
    
    For Each ctlTextBox In txtApe
        ActivarTxt ctlTextBox
    Next
    txtApe(0).SetFocus
    ActivarCbo cboApe
    cmdApe(0).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
        .TabEnabled(2) = False
    End With
End Sub

Private Sub txtApe_LostFocus(Index As Integer)
    txtApe(Index) = Trim$(txtApe(Index))
    txtApe(Index).BackColor = CT_COLOR_BLANCO
End Sub


Private Sub ShowStatistics(ByVal lngIdAp As Long)
'****************************************************************
' Nombre......................: ShowStatistics
' Tipo.............................: Sub
' �mbito........................: Privado
' Par�metros...............: Identificador de la apertura
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 29/1/00
' Descripci�n..............: Rellena el control
' ......................................: MSChart encargado de
' ......................................: mostrar las estad�sticas
' ......................................: de la apertura cuyo c�digo
' ......................................: se pasa como par�metro.
'****************************************************************
    Dim i%, j%
    Dim sComun As String
    Dim sSql As String
    
    Dim lngGanadas As Long
    Dim lngTablas As Long
    Dim lngPerdidas As Long
    Dim lngNofin As Long
    
    Dim RsTmp As Recordset
    
    ' Fragmento com�n de todas las consultas
    sComun = "SELECT COUNT(*) FROM Aperturas, Partidas " & _
        "WHERE ap_id = " & lngIdAp & " AND " & _
        "ap_id = pt_apertura AND"

    ' Declara la matriz Variant
    ' (el l�mite inferior no debe ser 0).
    Dim X(1 To 2, 1 To 5) As Variant


    ' Establece las etiquetas de las filas.
    X(1, 2) = "Ganadas"
    X(1, 3) = "Tablas"
    X(1, 4) = "Perdidas"
    X(1, 5) = "Sin Final"

    ' Establece las etiquetas de las columnas.
    X(2, 1) = "Partidas"    ' Totales

    ' Hallamos los valores estrictamente necesarios.
    ' Ganadas
    sSql = sComun & " pt_resultado = 1"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    lngGanadas = RsTmp(0)

    ' Tablas
    sSql = sComun & " pt_resultado = 3"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    lngTablas = RsTmp(0)

    ' Blancas Perdidas
    sSql = sComun & " pt_resultado = 2"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    lngPerdidas = RsTmp(0)

    ' Blancas Sin Final
    sSql = sComun & " pt_resultado = 4"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    lngNofin = RsTmp(0)

    ' Mostramos los resultados en
    ' el control Chart (Gr�ficas).
    
    ' Establece los datos.
    X(2, 2) = lngGanadas ' Totales Ganadas
    X(2, 3) = lngTablas ' Totales Tablas
    X(2, 4) = lngPerdidas ' Totales Perdidas
    X(2, 5) = lngNofin ' Totales Sin Final

    ' Establece los datos del gr�fico.
    chApe = X
    chApe.TitleText = RsApe!ap_den
End Sub

Private Sub ShowResults(ByVal lngIdApe As Long)
'****************************************************************************
' Nombre..........................: ShowResults
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Identificador de la apertura
' Valor de retorno..........: Ninguno
' �ltimo cambio.............: 20/2/00
' Descripci�n..................: Muestra los resultados
' ..........................................: correspondientes a la
' ..........................................: apertura seleccionada.
'****************************************************************************
    Dim rsResult As Recordset
    Dim sSql As String
    Dim mItem As ListItem
    Dim sTanteo As String
    
    sSql = "SELECT Partidas.pt_id As IDPART, Jugadores.ju_apenom As BLANCAS, Jugadores_1.ju_apenom As NEGRAS, Partidas.pt_resultado As CODRESULT " & _
            "FROM (Partidas INNER JOIN Jugadores ON Partidas.pt_blancas = Jugadores.ju_id) INNER JOIN Jugadores AS Jugadores_1 ON Partidas.pt_negras = Jugadores_1.ju_id " & _
            "Where (((Partidas.pt_apertura) = " & CStr(lngIdApe) & ")) " & _
            " ORDER BY 2, 3 "

    Set rsResult = dbDatos.OpenRecordset(sSql)
    lvwPt.ListItems.Clear
    
    With rsResult
        Do While Not .EOF
            Set mItem = lvwPt.ListItems.Add(, "X" & !IDPART, !blancas)
            mItem.SubItems(1) = !negras
            Select Case !CODRESULT
                Case 1
                    sTanteo = "1-0"
                Case 2
                    sTanteo = "0-1"
                Case 3
                    sTanteo = "Tablas"
                Case 4
                    sTanteo = "*"
            End Select
            mItem.SubItems(2) = sTanteo
            .MoveNext
        Loop
        Set rsResult = Nothing
    End With

End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub
