VERSION 5.00
Begin VB.Form frmModalidades 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   2835
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6555
   Icon            =   "frmModalidades.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   2835
   ScaleWidth      =   6555
   Tag             =   "Modalidades."
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de Modalidad"
      Height          =   705
      Left            =   120
      TabIndex        =   4
      Top             =   150
      Width           =   5505
      Begin VB.TextBox txtMod 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   150
         TabIndex        =   0
         Top             =   240
         Width           =   5235
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Modalidades"
      Height          =   1665
      Left            =   120
      TabIndex        =   1
      Top             =   975
      Width           =   6345
      Begin VB.ListBox lstMods 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   150
         Sorted          =   -1  'True
         TabIndex        =   3
         Top             =   240
         Width           =   6045
      End
   End
   Begin VB.CommandButton cmdBuscar 
      Default         =   -1  'True
      Height          =   675
      Left            =   5700
      Picture         =   "frmModalidades.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   240
      Width           =   795
   End
End
Attribute VB_Name = "frmModalidades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private strDen As String
    ' Guarda la denominaci�n del �ltimo tipo de modalidad
    ' buscado. Se utiliza para refrescar el Lview despu�s
    ' de las modificaciones.
Private RsMod   As Recordset

Public Estado As String
Private ReLoad As Boolean
' Variable para controlar cambios
Private blnChange As Boolean

Private Sub InicioForm()
'***************************************************************************************
' Nombre..........................: InicioForm
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio..............: 3/8/99
' Descripci�n...................: Crea objetos y configura el inicio del formulario.
'**************************************************************************************
    Dim lngStyle As Long
    
    ' Dimensionamos el formulario.
    Width = 6675
    Height = 3240
   
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_MODALIDADES
    
    ReLoad = False
    frmAbiertos = frmAbiertos + 1
    DisableAllFields
End Sub

Private Sub cmdBuscar_Click()
    Dim strSQL As String
        
    Clock 1
    strDen = "*" & Trim$(txtMod) & "*"
    If Not (txtMod = "") Then
        ' Buscar por la denominaci�n
        strSQL = "SELECT * FROM modalidades " & _
            "WHERE mo_den LIKE " & SnglQuote(strDen) & _
            " ORDER BY mo_den"
    Else
        ' Buscar todos
        strSQL = "modalidades"
    End If
    Set RsMod = dbDatos.OpenRecordset(strSQL)
    If Not RsMod.EOF Then
        DisplayRegs RsMod
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    Clock 0
End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
End Sub
Private Sub FinForm()
'****************************************************************************************
' Nombre.............................: FinForm
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 4/8/99
' Descripci�n.....................: Destruye los objetos al descargar el formulario.
'****************************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    Set RsMod = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub DisableAllFields()
'***********************************************************************************
' Nombre...............................: DisableAllFields
' Tipo......................................: Sub
' �mbito.................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno...............: Ninguno
' �ltimo cambio..................: 4/8/99
' Descripci�n.......................: Deshabilita los campos del formulario.
'***********************************************************************************

    DesActivarTxt txtMod
    DesActivarLstBox lstMods
    cmdBuscar.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub SetSearchControls()
'**************************************************************************************
' Nombre..............................: SetSearchControls
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio.................: 4/8/99
' Descripci�n......................: Habilita los campos del formulario para que
'...............................................: el usuario pueda hacer b�squedas.
'*************************************************************************************
    
    ActivarTxt txtMod
    txtMod.SetFocus
    cmdBuscar.Enabled = True
    DesActivarLstBox lstMods
    
End Sub

Public Sub Buscar()
    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub DisplayFields()
'***************************************************************************************
' Nombre............................: DisplayFields
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 4/8/99
' Descripci�n....................: Muestra en el form los datos del registro le�do.
'**************************************************************************************

    ClearFields
    txtMod = CheckForNull(RsMod!mo_den, "STRING")
    blnChange = False
End Sub

Private Sub ClearFields()
'**************************************************************************************
' Nombre..............................: ClearFields
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio.................: 4/8/99
' Descripci�n......................: Limpia el contenido de los campos del form.
'**************************************************************************************
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If

    txtMod = ""
    txtMod.Tag = ""
    blnChange = False
End Sub

Private Sub SetEditControls()
'****************************************************************************
' Nombre.......................: SetEditControls
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 4/8/99
' Descripci�n...............: Habilita los controles necesarios para
'.........................................: operaciones de busqueda y alta.
'****************************************************************************

    ActivarTxt txtMod
    txtMod.SetFocus
    cmdBuscar.Enabled = False

End Sub

Public Sub Cancelar()
'***************************************************************************************
' Nombre.........................: Cancelar
' Tipo................................: Sub
' �mbito............................: P�blico
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 4/8/99
' Descripci�n.................: Cancela la acci�n que se estuviera realizando
' .........................................: y vuelve al estado de espera.
'****************************************************************************************
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
End Sub

Public Sub Borrar()
'****************************************************************************
' Nombre.........................: Borrar
' Tipo................................: Sub
' �mbito...........................: P�blico
' Par�metros..................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio.............: 5/8/99
' Descripci�n..................: Da de baja un registro.
'****************************************************************************
    Dim Respuesta   As Integer

    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
End Sub

Public Sub Insertar()
    mobjControlMDI.Alta
    SetEditControls
    ClearFields

End Sub

Public Sub Grabar()
'********************************************************************************************
' Nombre........................: Grabar
' Tipo...............................: Sub
' �mbito..........................: P�blico
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 5/8/99
' Descripci�n................: Graba un reg. en la BB.DD. con los datos introducidos.
'*********************************************************************************************
    Dim mItem As ListItem
    
    ' Hacemos las validaciones de datos.
    If Not DatosValidos() Then Exit Sub
   ' Si el objeto RsMod a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If RsMod Is Nothing Then _
        Set RsMod = dbDatos.OpenRecordset("modalidades", dbOpenDynaset)
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            RsMod.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            RsMod.Edit
    End Select
    
    AsignarValores
    RsMod.Update
    If Estado = "EDICION" Then
        Dim lngItem As Long
        With lstMods
            lngItem = FindItem(lstMods, RsMod!mo_id)
            If lngItem <> -1 Then
                .RemoveItem lngItem
            End If
        End With
'        If LCase(RsMod!mo_den) Like LCase(strDen) Then
            ' Si la modificaci�n no afecta al criterio de b�squeda,
            ' lo mantenemos en la lista
            With lstMods
                .AddItem RsMod!mo_den
                .ItemData(.NewIndex) = RsMod!mo_id
            End With
'        End If
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    ClearFields
    SetSelectControls
    mobjControlMDI.Espera
End Sub

Private Sub AsignarValores()
'************************************************************************************
' Nombre..............................: AsignarValores
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio.................: 5/8/99
' Descripci�n......................:  Asigna los valores que el usuario
'...............................................: ha introducido en el formulario al registro.
'***********************************************************************************
    RsMod!mo_den = txtMod
End Sub

Private Sub lstMods_Click()
    ' Mostrar el registro seleccionado en la lista.
    Dim lngKey As Long

    Clock 1
    lngKey = lstMods.ItemData(lstMods.ListIndex)
    CheckChanges blnChange
    If RsMod.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        RsMod.Index = "mo_id"
        RsMod.Seek "=", lngKey
    Else
        ' Utilizamos el m�todo FIND si el Recordset es DYNASET o SNAPSHOT
        RsMod.FindFirst "mo_id =" & lngKey
    End If
    SetEditControls
    mobjControlMDI.Edicion
    DisplayFields
    Clock 0

End Sub

Private Sub txtMod_Change()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub txtMod_GotFocus()
    txtMod.BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtMod_LostFocus()
    txtMod = Trim$(txtMod)
    txtMod.BackColor = CT_COLOR_BLANCO
End Sub

Private Sub DisplayRegs(Rs As Recordset)
 '************************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra en la lista de selecci�n los registros
'..............................................: recuperados en el proceso de b�squeda.
'***********************************************************************************

    ActivarLstBox lstMods
    lstMods.Clear
    Do While Not Rs.EOF
        With lstMods
            .AddItem CheckForNull(RsMod!mo_den, "STRING")
            .ItemData(.NewIndex) = CheckForNull(RsMod!mo_id, "LONG")
        End With
        Rs.MoveNext
    Loop

End Sub

Private Sub Delete()

    On Error GoTo errDel
    
    With lstMods
        .RemoveItem .ListIndex
    End With
    RsMod.Delete
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number

End Sub

Private Function DatosValidos() As Boolean
'****************************************************************************
' Nombre........................: DatosValidos
' Tipo...............................: Function
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Booleano
' �ltimo cambio...........: 18/8/99
' Descripci�n................: Valida los datos
' ........................................: referidos al jugador.
'****************************************************************************

Dim msg$, cCtrlErr As Control, iCod%, j%

    msg = "": iCod = 0: DatosValidos = True
    If Len(txtMod) = 0 Then
        Set cCtrlErr = txtMod
        msg = "La descripci�n de la modalidad es obligatoria."
        iCod = 1
    ElseIf Len(txtMod) > 50 Then
        Set cCtrlErr = txtMod
        msg = "Longitud m�x. 50 caracteres."
        iCod = 2
    End If

    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
End Function


Private Sub SetSelectControls()
'************************************************************************************
' Nombre.............................: SetSelectControls
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Habilita los controles del formulario necesarios
'..............................................: para que el usuario pueda seleccionar entre los
' .............................................: registros recuperados en la b�squeda.
'***********************************************************************************

    mobjControlMDI.Buscar
    DesActivarTxt txtMod
    ActivarLstBox lstMods
    cmdBuscar.Enabled = False

End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub


