VERSION 5.00
Begin VB.Form frmAbout 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Acerca de SGBD Ajedrez"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraAbout 
      Height          =   2955
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4395
      Begin VB.Label lblAbout 
         Alignment       =   2  'Center
         Caption         =   "Juan Luis Garc�a Rodr�guez"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   180
         TabIndex        =   2
         Top             =   2340
         Width           =   4035
      End
      Begin VB.Label lblAbout 
         Alignment       =   2  'Center
         Caption         =   "Sistema Gestor de Base de Datos de Ajedrez"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   615
         Index           =   0
         Left            =   180
         TabIndex        =   1
         Top             =   1440
         Width           =   4095
      End
      Begin VB.Image imgAbout 
         Height          =   900
         Left            =   1777
         Picture         =   "frmAbout.frx":030A
         Stretch         =   -1  'True
         Top             =   180
         Width           =   900
      End
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

