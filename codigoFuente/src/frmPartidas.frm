VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmPartidas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Partidas"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9150
   Icon            =   "frmPartidas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   9150
   Tag             =   "Partidas."
   Begin TabDlg.SSTab SSTab1 
      Height          =   7035
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   12409
      _Version        =   327680
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "&Datos"
      TabPicture(0)   =   "frmPartidas.frx":030A
      Tab(0).ControlCount=   13
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Line1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraPt(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraPt(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraPt(2)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "fraPt(4)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "fraPt(3)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "fraPt(5)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdPt(3)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lstPt(2)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lstPt(3)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lstPt(4)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lstPt(1)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lstPt(0)"
      Tab(0).Control(12).Enabled=   0   'False
      TabCaption(1)   =   "&Tablero"
      TabPicture(1)   =   "frmPartidas.frx":0326
      Tab(1).ControlCount=   28
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ilsPt"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblPt(12)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lblPt(13)"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lblPt(14)"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "lblPt(15)"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "lblPt(16)"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "lblPt(17)"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "lblPt(18)"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "lblPt(19)"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "lblPt(20)"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "lblPt(21)"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "lblPt(22)"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "lblPt(23)"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "lblPt(24)"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "lblPt(25)"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "lblPt(26)"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "lblPt(27)"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "lblPt(28)"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "lblPt(29)"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).Control(19)=   "lblPt(30)"
      Tab(1).Control(19).Enabled=   0   'False
      Tab(1).Control(20)=   "lblPt(31)"
      Tab(1).Control(20).Enabled=   0   'False
      Tab(1).Control(21)=   "dlgPt"
      Tab(1).Control(21).Enabled=   0   'False
      Tab(1).Control(22)=   "fraPt(6)"
      Tab(1).Control(22).Enabled=   0   'False
      Tab(1).Control(23)=   "txtPt(12)"
      Tab(1).Control(23).Enabled=   -1  'True
      Tab(1).Control(24)=   "lstPt(5)"
      Tab(1).Control(24).Enabled=   -1  'True
      Tab(1).Control(25)=   "fraPt(7)"
      Tab(1).Control(25).Enabled=   0   'False
      Tab(1).Control(26)=   "fraPt(8)"
      Tab(1).Control(26).Enabled=   0   'False
      Tab(1).Control(27)=   "tmrPt"
      Tab(1).Control(27).Enabled=   0   'False
      Begin VB.ListBox lstPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   360
         Sorted          =   -1  'True
         TabIndex        =   3
         Top             =   1140
         Visible         =   0   'False
         Width           =   5115
      End
      Begin VB.ListBox lstPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   360
         Sorted          =   -1  'True
         TabIndex        =   7
         Top             =   1800
         Visible         =   0   'False
         Width           =   5115
      End
      Begin VB.Timer tmrPt 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   -74880
         Top             =   5280
      End
      Begin VB.Frame fraPt 
         Height          =   855
         Index           =   8
         Left            =   -70380
         TabIndex        =   77
         Top             =   5400
         Width           =   4275
         Begin VB.CommandButton cmdPt 
            Height          =   615
            Index           =   14
            Left            =   2280
            Picture         =   "frmPartidas.frx":0342
            Style           =   1  'Graphical
            TabIndex        =   83
            ToolTipText     =   "Incorporar partida PGN"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            Height          =   615
            Index           =   13
            Left            =   1560
            Picture         =   "frmPartidas.frx":064C
            Style           =   1  'Graphical
            TabIndex        =   82
            ToolTipText     =   "Imprimir la partida"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            Height          =   615
            Index           =   12
            Left            =   3300
            Picture         =   "frmPartidas.frx":0956
            Style           =   1  'Graphical
            TabIndex        =   52
            ToolTipText     =   "Grabar Comentario"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            Caption         =   "&Auto"
            Height          =   615
            Index           =   11
            Left            =   840
            Picture         =   "frmPartidas.frx":0C60
            Style           =   1  'Graphical
            TabIndex        =   81
            ToolTipText     =   "Modo de Reproducci�n"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            Height          =   615
            Index           =   10
            Left            =   120
            Picture         =   "frmPartidas.frx":152A
            Style           =   1  'Graphical
            TabIndex        =   78
            ToolTipText     =   "Girar Tablero"
            Top             =   180
            Width           =   615
         End
      End
      Begin VB.Frame fraPt 
         Height          =   855
         Index           =   7
         Left            =   -74220
         TabIndex        =   72
         Top             =   5400
         Width           =   3015
         Begin VB.CommandButton cmdPt 
            DownPicture     =   "frmPartidas.frx":1834
            Height          =   615
            Index           =   7
            Left            =   2280
            Picture         =   "frmPartidas.frx":1C76
            Style           =   1  'Graphical
            TabIndex        =   76
            ToolTipText     =   "Final"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            DownPicture     =   "frmPartidas.frx":1F80
            Height          =   615
            Index           =   6
            Left            =   1560
            Picture         =   "frmPartidas.frx":23C2
            Style           =   1  'Graphical
            TabIndex        =   75
            ToolTipText     =   "Adelante"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            DownPicture     =   "frmPartidas.frx":26CC
            Height          =   615
            Index           =   5
            Left            =   840
            Picture         =   "frmPartidas.frx":2B0E
            Style           =   1  'Graphical
            TabIndex        =   74
            ToolTipText     =   "Atr�s"
            Top             =   180
            Width           =   615
         End
         Begin VB.CommandButton cmdPt 
            DownPicture     =   "frmPartidas.frx":2E18
            Height          =   615
            Index           =   4
            Left            =   120
            Picture         =   "frmPartidas.frx":325A
            Style           =   1  'Graphical
            TabIndex        =   73
            ToolTipText     =   "Comienzo"
            Top             =   180
            Width           =   615
         End
      End
      Begin VB.ListBox lstPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Index           =   5
         ItemData        =   "frmPartidas.frx":3564
         Left            =   -70380
         List            =   "frmPartidas.frx":3566
         TabIndex        =   54
         Top             =   1020
         Width           =   4275
      End
      Begin VB.ListBox lstPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   360
         Sorted          =   -1  'True
         TabIndex        =   24
         Top             =   4380
         Visible         =   0   'False
         Width           =   4635
      End
      Begin VB.ListBox lstPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   360
         Sorted          =   -1  'True
         TabIndex        =   20
         Top             =   3480
         Visible         =   0   'False
         Width           =   4635
      End
      Begin VB.ListBox lstPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   360
         Sorted          =   -1  'True
         TabIndex        =   16
         Top             =   2820
         Visible         =   0   'False
         Width           =   4635
      End
      Begin VB.TextBox txtPt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1995
         Index           =   12
         Left            =   -70380
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   51
         Top             =   2760
         Width           =   4275
      End
      Begin VB.Frame fraPt 
         ClipControls    =   0   'False
         Height          =   4095
         Index           =   6
         Left            =   -74700
         TabIndex        =   49
         Top             =   660
         Width           =   3975
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   18
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":3568
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   28
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":36BA
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   38
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":380C
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   48
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":395E
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   58
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":3AB0
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   68
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":3C02
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   78
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":3D54
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   88
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":3EA6
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   180
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   17
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":3FF8
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   27
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":414A
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   37
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":429C
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   47
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":43EE
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   57
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":4540
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   67
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":4692
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   77
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":47E4
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   87
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":4936
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   660
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   16
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":4A88
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   26
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":4BDA
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   36
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":4D2C
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   46
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":4E7E
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   56
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":4FD0
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   66
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":5122
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   76
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":5274
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   86
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":53C6
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1140
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   15
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":5518
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   25
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":566A
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   35
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":57BC
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   45
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":590E
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   55
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":5A60
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   65
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":5BB2
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   75
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":5D04
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   85
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":5E56
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   1620
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   14
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":5FA8
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   24
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":60FA
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   34
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":624C
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   44
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":639E
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   54
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":64F0
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   64
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":6642
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   74
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":6794
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   84
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":68E6
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   13
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":6A38
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   23
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":6B8A
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   33
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":6CDC
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   43
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":6E2E
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   53
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":6F80
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   63
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":70D2
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   73
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":7224
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   83
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":7376
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   2580
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   12
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":74C8
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   22
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":761A
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   32
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":776C
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   42
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":78BE
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   52
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":7A10
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   62
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":7B62
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   72
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":7CB4
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   82
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":7E06
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3060
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   11
            Left            =   60
            MouseIcon       =   "frmPartidas.frx":7F58
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   21
            Left            =   540
            MouseIcon       =   "frmPartidas.frx":80AA
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   31
            Left            =   1020
            MouseIcon       =   "frmPartidas.frx":81FC
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   41
            Left            =   1500
            MouseIcon       =   "frmPartidas.frx":834E
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   51
            Left            =   1980
            MouseIcon       =   "frmPartidas.frx":84A0
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   61
            Left            =   2460
            MouseIcon       =   "frmPartidas.frx":85F2
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   71
            Left            =   2940
            MouseIcon       =   "frmPartidas.frx":8744
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EN"
            Top             =   3540
            Width           =   495
         End
         Begin VB.Image imgCasilla 
            Height          =   495
            Index           =   81
            Left            =   3420
            MouseIcon       =   "frmPartidas.frx":8896
            MousePointer    =   99  'Custom
            Stretch         =   -1  'True
            Tag             =   "EB"
            Top             =   3540
            Width           =   495
         End
      End
      Begin VB.CommandButton cmdPt 
         Height          =   615
         Index           =   3
         Left            =   8100
         Picture         =   "frmPartidas.frx":89E8
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   5520
         Width           =   735
      End
      Begin VB.Frame fraPt 
         Caption         =   " Partidas "
         Height          =   1815
         Index           =   5
         Left            =   180
         TabIndex        =   48
         Top             =   5100
         Width           =   7695
         Begin ComctlLib.ListView lvwPt 
            Height          =   1515
            Left            =   120
            TabIndex        =   31
            Top             =   240
            Width           =   7455
            _ExtentX        =   13150
            _ExtentY        =   2672
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   327680
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmPartidas.frx":8CF2
            NumItems        =   3
            BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Blancas"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               SubItemIndex    =   1
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Negras"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               SubItemIndex    =   2
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "Resultado"
               Object.Width           =   1411
            EndProperty
         End
      End
      Begin VB.Frame fraPt 
         Caption         =   " Notas "
         Height          =   1395
         Index           =   3
         Left            =   5700
         TabIndex        =   47
         Top             =   3600
         Width           =   3195
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1095
            Index           =   10
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   29
            Top             =   240
            Width           =   2955
         End
      End
      Begin VB.Frame fraPt 
         Caption         =   " Otros "
         Height          =   1395
         Index           =   4
         Left            =   180
         TabIndex        =   42
         Top             =   3600
         Width           =   5175
         Begin VB.TextBox txtPt 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   11
            Left            =   4380
            MaxLength       =   5
            MultiLine       =   -1  'True
            TabIndex        =   27
            Top             =   960
            Width           =   435
         End
         Begin ComCtl2.UpDown updPt 
            Height          =   360
            Left            =   4815
            TabIndex        =   28
            Top             =   960
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   635
            _Version        =   327680
            BuddyControl    =   "txtPt(11)"
            BuddyDispid     =   196614
            BuddyIndex      =   11
            OrigLeft        =   4800
            OrigTop         =   960
            OrigRight       =   5040
            OrigBottom      =   1275
            Max             =   255
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   9
            Left            =   1860
            TabIndex        =   26
            Top             =   960
            Width           =   1920
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   8
            Left            =   180
            TabIndex        =   25
            Top             =   960
            Width           =   1095
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   6
            Left            =   180
            TabIndex        =   22
            Top             =   420
            Width           =   4635
         End
         Begin VB.CommandButton cmdPt 
            Caption         =   "..."
            Height          =   360
            Index           =   2
            Left            =   4800
            TabIndex        =   23
            Top             =   420
            Width           =   255
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Ronda"
            Height          =   195
            Index           =   11
            Left            =   4380
            TabIndex        =   50
            Top             =   780
            Width           =   480
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Lugar"
            Height          =   195
            Index           =   10
            Left            =   1860
            TabIndex        =   46
            Top             =   780
            Width           =   405
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Fecha"
            Height          =   195
            Index           =   9
            Left            =   180
            TabIndex        =   45
            Top             =   780
            Width           =   450
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Torneo"
            Height          =   195
            Index           =   7
            Left            =   180
            TabIndex        =   44
            Top             =   240
            Width           =   510
         End
      End
      Begin VB.Frame fraPt 
         Caption         =   " Caracter�sticas "
         Height          =   1515
         Index           =   2
         Left            =   180
         TabIndex        =   38
         Top             =   2040
         Width           =   8715
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   7
            Left            =   5640
            TabIndex        =   21
            Top             =   1080
            Width           =   2955
         End
         Begin VB.CommandButton cmdPt 
            Caption         =   "..."
            Height          =   315
            Index           =   1
            Left            =   4800
            TabIndex        =   19
            Top             =   1080
            Width           =   255
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   5
            Left            =   180
            TabIndex        =   18
            Top             =   1080
            Width           =   4635
         End
         Begin VB.CommandButton cmdPt 
            Caption         =   "..."
            Height          =   360
            Index           =   0
            Left            =   4800
            TabIndex        =   15
            Top             =   420
            Width           =   255
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   4
            Left            =   180
            TabIndex        =   14
            Top             =   420
            Width           =   4635
         End
         Begin VB.ComboBox cboPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "frmPartidas.frx":8D0E
            Left            =   5640
            List            =   "frmPartidas.frx":8D10
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   17
            Top             =   420
            Width           =   2955
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Comentarista"
            Height          =   195
            Index           =   8
            Left            =   5640
            TabIndex        =   43
            Top             =   900
            Width           =   915
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Final"
            Height          =   195
            Index           =   6
            Left            =   5640
            TabIndex        =   41
            Top             =   240
            Width           =   915
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Modalidad"
            Height          =   195
            Index           =   5
            Left            =   180
            TabIndex        =   40
            Top             =   900
            Width           =   735
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Apertura"
            Height          =   195
            Index           =   4
            Left            =   180
            TabIndex        =   39
            Top             =   240
            Width           =   600
         End
      End
      Begin VB.Frame fraPt 
         Caption         =   " Resultado "
         Height          =   1575
         Index           =   1
         Left            =   7020
         TabIndex        =   37
         Top             =   360
         Width           =   1875
         Begin VB.OptionButton optPt 
            Caption         =   "Todos"
            Height          =   255
            Index           =   4
            Left            =   420
            TabIndex        =   13
            Top             =   1260
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.OptionButton optPt 
            Caption         =   "Indeterminado"
            Height          =   255
            Index           =   3
            Left            =   420
            TabIndex        =   12
            Top             =   1005
            Width           =   1335
         End
         Begin VB.OptionButton optPt 
            Caption         =   "Tablas"
            Height          =   255
            Index           =   2
            Left            =   420
            TabIndex        =   11
            Top             =   750
            Width           =   915
         End
         Begin VB.OptionButton optPt 
            Caption         =   "0 - 1"
            Height          =   255
            Index           =   1
            Left            =   420
            TabIndex        =   10
            Top             =   495
            Width           =   735
         End
         Begin VB.OptionButton optPt 
            Caption         =   "1 - 0"
            Height          =   255
            Index           =   0
            Left            =   420
            TabIndex        =   9
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.Frame fraPt 
         Caption         =   " Jugadores "
         Height          =   1575
         Index           =   0
         Left            =   180
         TabIndex        =   32
         Top             =   360
         Width           =   6795
         Begin VB.CommandButton cmdPt 
            Caption         =   "..."
            Height          =   360
            Index           =   9
            Left            =   5280
            TabIndex        =   6
            Top             =   1080
            Width           =   255
         End
         Begin VB.CommandButton cmdPt 
            Caption         =   "..."
            Height          =   360
            Index           =   8
            Left            =   5280
            TabIndex        =   2
            Top             =   420
            Width           =   255
         End
         Begin VB.TextBox txtPt 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   3
            Left            =   5640
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   8
            Top             =   1080
            Width           =   915
         End
         Begin VB.TextBox txtPt 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   2
            Left            =   5640
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   4
            Top             =   420
            Width           =   915
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   180
            TabIndex        =   5
            Top             =   1080
            Width           =   5115
         End
         Begin VB.TextBox txtPt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   0
            Left            =   180
            TabIndex        =   1
            Top             =   420
            Width           =   5115
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "ELO"
            Height          =   195
            Index           =   3
            Left            =   5640
            TabIndex        =   36
            Top             =   900
            Width           =   315
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "ELO"
            Height          =   195
            Index           =   2
            Left            =   5640
            TabIndex        =   35
            Top             =   240
            Width           =   315
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Negras"
            Height          =   195
            Index           =   1
            Left            =   180
            TabIndex        =   34
            Top             =   900
            Width           =   510
         End
         Begin VB.Label lblPt 
            AutoSize        =   -1  'True
            Caption         =   "Blancas"
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   33
            Top             =   240
            Width           =   570
         End
      End
      Begin MSComDlg.CommonDialog dlgPt 
         Left            =   -74880
         Top             =   6300
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "pgn"
         DialogTitle     =   "Abrir archivo de partidas PGN"
         Filter          =   "Ficheros PGN (*.PGN)|*.pgn|Todos los archivos(*.*)|*.*"
         Flags           =   528388
      End
      Begin VB.Label lblPt 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   31
         Left            =   -74670
         TabIndex        =   80
         Top             =   360
         Width           =   3930
      End
      Begin VB.Label lblPt 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   30
         Left            =   -74670
         TabIndex        =   79
         Top             =   5100
         Width           =   3930
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   29
         Left            =   -74880
         TabIndex        =   71
         Top             =   1020
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   28
         Left            =   -74880
         TabIndex        =   70
         Top             =   1500
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   -74880
         TabIndex        =   69
         Top             =   1980
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   -74880
         TabIndex        =   68
         Top             =   2460
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   -74880
         TabIndex        =   67
         Top             =   2940
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   -74880
         TabIndex        =   66
         Top             =   3420
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   -74880
         TabIndex        =   65
         Top             =   3900
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   -74880
         TabIndex        =   64
         Top             =   4380
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "h"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   -71100
         TabIndex        =   63
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "g"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   -71595
         TabIndex        =   62
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "f"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   -72030
         TabIndex        =   61
         Top             =   4800
         Width           =   75
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "e"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   -72510
         TabIndex        =   60
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "d"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   -73005
         TabIndex        =   59
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "c"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   -73485
         TabIndex        =   58
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "b"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   -73980
         TabIndex        =   57
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   -74460
         TabIndex        =   56
         Top             =   4800
         Width           =   120
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "Movimientos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   13
         Left            =   -70380
         TabIndex        =   55
         Top             =   780
         Width           =   1320
      End
      Begin VB.Label lblPt 
         AutoSize        =   -1  'True
         Caption         =   "Comentarios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   -70380
         TabIndex        =   53
         Top             =   2520
         Width           =   1320
      End
      Begin VB.Line Line1 
         BorderWidth     =   3
         X1              =   180
         X2              =   8880
         Y1              =   5040
         Y2              =   5040
      End
      Begin ComctlLib.ImageList ilsPt 
         Left            =   -74940
         Top             =   5700
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   8224125
         _Version        =   327680
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   14
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":8D12
               Key             =   "EB"
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":9564
               Key             =   "EN"
            EndProperty
            BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":9DB6
               Key             =   "PB"
            EndProperty
            BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":B028
               Key             =   "PN"
            EndProperty
            BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":C29A
               Key             =   "CB"
            EndProperty
            BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":D6EC
               Key             =   "CN"
            EndProperty
            BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":EBDE
               Key             =   "AB"
            EndProperty
            BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":FF90
               Key             =   "AN"
            EndProperty
            BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":11342
               Key             =   "TB"
            EndProperty
            BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":12514
               Key             =   "TN"
            EndProperty
            BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":136E6
               Key             =   "DB"
            EndProperty
            BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":14B54
               Key             =   "DN"
            EndProperty
            BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":15FC2
               Key             =   "RB"
            EndProperty
            BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPartidas.frx":171C0
               Key             =   "RN"
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmPartidas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Estado As String

Private ReLoad As Boolean

' Variable para controlar cambios
Private blnChange As Boolean


' RecordSet que almacena los datos
' de la partida.
Private rsPt As Recordset

' RecordSet que almacena los movimientos
' de la partida.
Private rsMv As Recordset

Private strDen As String

'Private strClavesAp() As String

' Objeto que almacena en memoria
' la posici�n de la partida.
Private mobjTablero As New clsTablero

' Objeto para acceder a datos.
Private mobjDatos As New clsDatos

' Matriz que almacena las posiciones de la partida.
Private mMatrizPosiciones() As TIPOPIEZA
'  Esta matriz tendr� 3 dimensiones:
'
'      1�- (1,8) N� de columnas.
'      2�- (1,8) N� de filas.
'      3�- (n) N� de posiciones de la partida

' Variable que indica el �ndice de la posici�n actual de
' la matriz de posiciones donde se almacena el tablero.
Private mintPosicion As Integer

' Matriz donde se almacenan los marcadores de registros
' del recordset de movimientos (rsMv).
Private mMatrizMarcadores() As String


Public Property Let IdPartida(ByVal IdPt As Long)
    ' Cuando a esta propiedad se le pasa desde otro
    ' formulario el valor de un identificador de
    ' partida, dicha partida ser� mostrada en este
    ' formulario.
    Dim strSQL As String
    
'    InicioTablero
    strSQL = "SELECT * FROM Partidas WHERE pt_id = " & IdPt
    Set rsPt = dbDatos.OpenRecordset(strSQL)
    DoEvents
    Me.WindowState = vbNormal
    Me.ZOrder 0
    mobjControlMDI.Edicion
    SetEditControls
    DisplayFields
    GetMoves rsPt!pt_id
End Property

Private Sub InicioForm()
'*************************************************************************************
' Nombre........................: InicioForm
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 10/9/99
' Descripci�n................: Crea objetos y configura el inicio del form.
'************************************************************************************
    Dim lngStyle As Long
    On Error GoTo InicioError
    
    ' Dimensionamos el formulario.
    Width = 9270
    Height = 7470
    
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_PARTIDAS
    
    ' Para poder se�alar un fila completa del ListView
    lngStyle = SendMessage(lvwPt.hWnd, _
              LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    lngStyle = lngStyle Or LVS_EX_FULLROWSELECT
    Call SendMessage(lvwPt.hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, ByVal lngStyle)

    ReLoad = False
    frmAbiertos = frmAbiertos + 1

    DisableAllFields
    
GetExit:
    Exit Sub
    
InicioError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub cboPt_Change()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub cmdPt_Click(Index As Integer)
    On Error GoTo CmdClickError
    
    Select Case Index
        Case 0      ' Ayuda de campo Apertura.
            ListaAperturas lstPt(2), txtPt(4)
        Case 1      ' Ayuda de campo Modalidad.
            ListaModalidades lstPt(3), txtPt(5)
        Case 2      ' Ayuda de campo Torneo.
            ListaTorneos lstPt(4), txtPt(6)
        Case 3      ' Buscar Ahora
            ThrowSearch
        Case 4      ' Primero.
            Primero
        Case 5      ' Atr�s.
            Atras
        Case 6      ' Adelante.
            Adelante
        Case 7      ' �ltimo
            Ultimo
        Case 8      ' Ayuda de campo Jug. Blancas.
            ListaJugadores lstPt(0), txtPt(0)
        Case 9      ' Ayuda de campo Jug. Negras.
            ListaJugadores lstPt(1), txtPt(1)
        Case 10     ' Girar tablero.
            GirarTablero
        Case 11     ' Modo de reproducci�n
            CambiaModoRep
        Case 12     ' Graba el comentario.
            SaveComent
        Case 13     ' Imprimir Partida.
            Dim intMsg As Integer
            intMsg = MsgBox("�Preparado para imprimir partida?", vbYesNo, _
                            "Imprimir Partida.")
            If intMsg = vbYes Then
                PrintMatch
            End If
        Case 14     ' Incorporar partida PGN.
            CargarPartidaPGN
    End Select
    
GetExit:
    Exit Sub
    
CmdClickError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub InicioTablero()
'****************************************************************************
' Nombre.........................: InicioTablero
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 10/9/99
' Descripci�n.................: Configura la pesta�a correspondiente
' .........................................: al tablero.
'****************************************************************************
    On Error GoTo InicioTableroError

    ' Inicializamos la lista de movimientos.
    lstPt(5).Clear
    ' Inicializa los gr�ficos del tablero
    With ilsPt
        Set imgCasilla(11).Picture = .Overlay("EN", "TB")
        Set imgCasilla(21).Picture = .Overlay("EB", "CB")
        Set imgCasilla(31).Picture = .Overlay("EN", "AB")
        Set imgCasilla(41).Picture = .Overlay("EB", "DB")
        Set imgCasilla(51).Picture = .Overlay("EN", "RB")
        Set imgCasilla(61).Picture = .Overlay("EB", "AB")
        Set imgCasilla(71).Picture = .Overlay("EN", "CB")
        Set imgCasilla(81).Picture = .Overlay("EB", "TB")
        Set imgCasilla(12).Picture = .Overlay("EB", "PB")
        Set imgCasilla(22).Picture = .Overlay("EN", "PB")
        Set imgCasilla(32).Picture = .Overlay("EB", "PB")
        Set imgCasilla(42).Picture = .Overlay("EN", "PB")
        Set imgCasilla(52).Picture = .Overlay("EB", "PB")
        Set imgCasilla(62).Picture = .Overlay("EN", "PB")
        Set imgCasilla(72).Picture = .Overlay("EB", "PB")
        Set imgCasilla(82).Picture = .Overlay("EN", "PB")

        Set imgCasilla(13).Picture = .ListImages("EN").Picture
        Set imgCasilla(23).Picture = .ListImages("EB").Picture
        Set imgCasilla(33).Picture = .ListImages("EN").Picture
        Set imgCasilla(43).Picture = .ListImages("EB").Picture
        Set imgCasilla(53).Picture = .ListImages("EN").Picture
        Set imgCasilla(63).Picture = .ListImages("EB").Picture
        Set imgCasilla(73).Picture = .ListImages("EN").Picture
        Set imgCasilla(83).Picture = .ListImages("EB").Picture
        Set imgCasilla(14).Picture = .ListImages("EB").Picture
        Set imgCasilla(24).Picture = .ListImages("EN").Picture
        Set imgCasilla(34).Picture = .ListImages("EB").Picture
        Set imgCasilla(44).Picture = .ListImages("EN").Picture
        Set imgCasilla(54).Picture = .ListImages("EB").Picture
        Set imgCasilla(64).Picture = .ListImages("EN").Picture
        Set imgCasilla(74).Picture = .ListImages("EB").Picture
        Set imgCasilla(84).Picture = .ListImages("EN").Picture
        Set imgCasilla(15).Picture = .ListImages("EN").Picture
        Set imgCasilla(25).Picture = .ListImages("EB").Picture
        Set imgCasilla(35).Picture = .ListImages("EN").Picture
        Set imgCasilla(45).Picture = .ListImages("EB").Picture
        Set imgCasilla(55).Picture = .ListImages("EN").Picture
        Set imgCasilla(65).Picture = .ListImages("EB").Picture
        Set imgCasilla(75).Picture = .ListImages("EN").Picture
        Set imgCasilla(85).Picture = .ListImages("EB").Picture
        Set imgCasilla(16).Picture = .ListImages("EB").Picture
        Set imgCasilla(26).Picture = .ListImages("EN").Picture
        Set imgCasilla(36).Picture = .ListImages("EB").Picture
        Set imgCasilla(46).Picture = .ListImages("EN").Picture
        Set imgCasilla(56).Picture = .ListImages("EB").Picture
        Set imgCasilla(66).Picture = .ListImages("EN").Picture
        Set imgCasilla(76).Picture = .ListImages("EB").Picture
        Set imgCasilla(86).Picture = .ListImages("EN").Picture

        Set imgCasilla(17).Picture = .Overlay("EN", "PN")
        Set imgCasilla(27).Picture = .Overlay("EB", "PN")
        Set imgCasilla(37).Picture = .Overlay("EN", "PN")
        Set imgCasilla(47).Picture = .Overlay("EB", "PN")
        Set imgCasilla(57).Picture = .Overlay("EN", "PN")
        Set imgCasilla(67).Picture = .Overlay("EB", "PN")
        Set imgCasilla(77).Picture = .Overlay("EN", "PN")
        Set imgCasilla(87).Picture = .Overlay("EB", "PN")
        Set imgCasilla(18).Picture = .Overlay("EB", "TN")
        Set imgCasilla(28).Picture = .Overlay("EN", "CN")
        Set imgCasilla(38).Picture = .Overlay("EB", "AN")
        Set imgCasilla(48).Picture = .Overlay("EN", "DN")
        Set imgCasilla(58).Picture = .Overlay("EB", "RN")
        Set imgCasilla(68).Picture = .Overlay("EN", "AN")
        Set imgCasilla(78).Picture = .Overlay("EB", "CN")
        Set imgCasilla(88).Picture = .Overlay("EN", "TN")
    End With
    
    ' Inicializamos el marcador de posiciones
    mintPosicion = 0
    ' Inicializamos la matriz de posiciones para comenzar a introducir los
    ' movimientos recogidos de la BB.DD.
    ReDim mMatrizPosiciones(1 To 8, 1 To 8, 0 To 0)
    ' Inicializamos el tablero memorizado en la clase que se encargar� de
    ' validar las jugadas seg�n se vayan leyendo.
    mobjTablero.Inicializar
    ' En la posici�n cero de la matriz de posiciones almacenamos la confi-
    ' guraci�n inicial de la partida
    Dim iCol As Byte, iFil As Byte
    For iCol = 1 To 8
        For iFil = 1 To 8
            mMatrizPosiciones(iCol, iFil, 0) = mobjTablero.Escaque(iCol, iFil)
        Next iFil
    Next iCol
    With lstPt(5)
        .AddItem "INICIO", 0
        .ItemData(.NewIndex) = 15
    End With
    
    
GetExit:
    Exit Sub

InicioTableroError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub imgCasilla_Click(Index As Integer)
'****************************************************************************
' Nombre......................: imgCasilla_Click
' Tipo.............................: Evento Click de un control Image
' �mbito........................: Privado
' Par�metros...............: Indice de un control Image
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 10/9/99
' Descripci�n...............: Controla la acci�n de pulsar sobre una
' .......................................: casilla para realizar un movimiento.
'****************************************************************************

    Static pbytColAnt As Byte, pbytFilAnt As Byte
    Dim pbytCol As Byte, pbytFil As Byte
    Dim strComentario As String
    Dim vntResultadoMv As Variant
    Dim pbytTurnoAnt As TIPOBANDO, pintNMovAnt As Integer
    Dim strFind As String
    On Error GoTo ImgClickError

    pbytCol = Index \ 10
    pbytFil = Index Mod 10
    If pbytColAnt = 0 Then
        ' Ha pinchado en la casilla de origen.
        pbytColAnt = pbytCol
        pbytFilAnt = pbytFil
        imgCasilla(Index).BorderStyle = vbFixedSingle
    Else
        ' Se pincha en la casilla destino
        ' �� Cuidado con pinchar en la misma casilla de origen !!
        If Not (pbytColAnt = pbytCol And pbytFilAnt = pbytFil) Then
            ' Antes de insertar un nuevo movimiento, debemos asignar
            ' nuevos valores para las propiedades n� de movimiento y
            ' turno de movimiento, todo ello se hace a partir del
            ' n�mero de posici�n en la que se encuentre la partida.
            mobjTablero.ParametrizaPosicion mintPosicion + 1, _
                mMatrizPosiciones(), lstPt(5).ItemData(mintPosicion)
            ' Guardamos el turno y numero de movimiento
            ' para poder buscar el primer registro para
            ' desechar cuando se introduzca un nuevo movimiento.
            pbytTurnoAnt = mobjTablero.Turno
            pintNMovAnt = mobjTablero.NumMovto
            vntResultadoMv = _
                mobjTablero.Mover(pbytColAnt, pbytFilAnt, pbytCol, pbytFil)
            If Not vntResultadoMv = False Then
                ' El movimiento es legal.
                
                ' Comprobamos si se trata de un
                ' movimiento de promoci�n de pe�n
                If (mobjTablero.Escaque(pbytCol, pbytFil) = _
                    CodPeonBlanco And pbytFil = 8) Or _
                    (mobjTablero.Escaque(pbytCol, pbytFil) = _
                    CodPeonNegro And pbytFil = 1) Then
                    ' Promoci�n de pe�n blanco
                    With frmPromo
                        .Bando = pbytTurnoAnt   'mobjTablero.Turno
                        .Casilla = Index
                        .Show vbModal
                        ' Colocamos en el tablero la pieza
                        ' a la que a promocionado el pe�n.
                        mobjTablero.PutPiece pbytCol, pbytFil, .Pieza
                        pbytCol = .Casilla \ 10
                        pbytFil = .Casilla Mod 10
                    End With
                End If
                
                ' Si es una partida nueva creamos  el primer registro
                ' ficticio que posteriormente ser� machacado por la
                ' informaci�n que nos interesa almacenar
                If rsMv Is Nothing Then
                    Set rsMv = dbDatos.OpenRecordset("Movimientos", dbOpenDynaset)
                    With rsMv
                        .AddNew
                        !mv_partida = rsPt!pt_id
                        !mv_nmov = pintNMovAnt
                        !mv_bando = pbytTurnoAnt
                        !mv_orig = 0
                        !mv_dest = 0
                        .Update
                    End With
                End If
                
                ' Truncamos el recordset RsMv para introducir
                ' el nuevo movimiento, obviamente este pasar�
                ' a ser el �ltimo.
                strFind = "mv_partida =" & rsPt!pt_id & _
                        "AND mv_nmov =" & pintNMovAnt & _
                        " AND mv_bando =" & pbytTurnoAnt

                With rsMv
                    .FindFirst strFind
                    If Not .NoMatch Then
                        ' Si estamos insertando un movimiento a la
                        ' mitad de una partida borramos el resto
                        ' de registros del recordset ya que se
                        ' corresponder�n con movimientos no v�lidos
                        ' a partir de ahora, por lo tanto se desechan.
                        Do While Not .EOF
                            .Delete
                            .MoveNext
                        Loop
                        ' Tambi�n eliminamos dichos movimientos
                        ' de la lista del formulario
                        Dim z As Integer
                        For z = lstPt(5).ListCount - 1 To mintPosicion + 1 Step -1
                            lstPt(5).RemoveItem z
                        Next z
                    End If
                    ' A�adimos el registro con el nuevo movimento.
                    .AddNew
                    !mv_partida = rsPt!pt_id
                    !mv_nmov = pintNMovAnt
                    !mv_bando = pbytTurnoAnt
                    !mv_orig = CByte(pbytColAnt * 10 + pbytFilAnt)
                    !mv_dest = CByte(pbytCol * 10 + pbytFil)
                    .Update
                    ' Nos movemos al registro nuevo.
                    .Bookmark = .LastModified
                End With
                AddMove mintPosicion + 1, CStr(vntResultadoMv)
                Adelante
                txtPt(12).SetFocus
'                MostrarTablero mintPosicion + 1
            ElseIf vntResultadoMv = False Then
                ' La jugada es ilegal
                MsgBox "El movimiento no es correcto", vbCritical
            End If
        End If
        imgCasilla(pbytColAnt * 10 + pbytFilAnt).BorderStyle = vbBSNone
        pbytColAnt = 0
        pbytFilAnt = 0
    End If

GetExit:
    Exit Sub

ImgClickError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
    LoadCboFins cboPt
End Sub

Private Sub FinForm()
'****************************************************************************
' Nombre............................: FinForm
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 10/9/99
' Descripci�n....................: Destruye los objetos.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    Set mobjTablero = Nothing
    Set rsPt = Nothing
    Set rsMv = Nothing
    Set mobjDatos = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub DisableAllFields()
'****************************************************************************
' Nombre...........................: DisableAllFields
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 16/8/99
' Descripci�n...................: Deshabilita los controles del formulario.
'****************************************************************************

    Dim ctlTxt As TextBox
    Dim ctlOpt As OptionButton
    Dim ctlCmd As CommandButton
    On Error GoTo DisableAllError
    
    For Each ctlTxt In txtPt
        DesActivarTxt ctlTxt
    Next
    For Each ctlOpt In optPt
        DesActivarOpt ctlOpt
    Next
    optPt(4).Visible = False
    For Each ctlCmd In cmdPt
        DesActCmd ctlCmd
    Next
    cmdPt(3).Enabled = False
    cboPt.ListIndex = -1
    DesActivarCbo cboPt
    DesActivarLstView lvwPt
    updPt.Enabled = False
    With SSTab1
        .Tab = 0
        .TabEnabled(0) = False
        .TabEnabled(1) = False
    End With
    
    
GetExit:
    Exit Sub
    
DisableAllError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub Buscar()
    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub SetSearchControls()
'***************************************************************************************
' Nombre.........................: SetSearchControls
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio.............: 16/8/99
' Descripci�n.................: Habilita los campos del formulario para que el
'..........................................: usuario pueda introducir los filtros de b�squeda.
'**************************************************************************************

    ActCmd cmdPt(3)
    With SSTab1
        .Tab = 0
        .TabEnabled(0) = True
        .TabEnabled(1) = False
    End With
    
    optPt(4).Visible = True

    ActivarTxt txtPt(0)
    ActivarTxt txtPt(1)
    ActivarTxt txtPt(2)
    ActivarTxt txtPt(3)
    ActivarTxt txtPt(4)
    ActivarTxt txtPt(5)
    ActivarTxt txtPt(6)
    ActivarTxt txtPt(7)
    ActivarTxt txtPt(8)
    ActivarTxt txtPt(9)
    ActivarTxt txtPt(11)
    DesActivarTxt txtPt(10)
    
    ActCmd cmdPt(0)
    ActCmd cmdPt(1)
    ActCmd cmdPt(2)
    ActCmd cmdPt(8)
    ActCmd cmdPt(9)
    
    ActivarOpt optPt(0)
    ActivarOpt optPt(1)
    ActivarOpt optPt(2)
    ActivarOpt optPt(3)
    ActivarOpt optPt(4)
    optPt(4).Value = True
    ActivarCbo cboPt
    
    updPt.Enabled = True
    
    DesActivarLstView lvwPt
    txtPt(0).SetFocus
End Sub

Public Sub Cancelar()
'*************************************************************************************
' Nombre..............................: Cancelar
' Tipo.....................................: Sub
' �mbito................................: P�blico
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio..................: 4/10/99
' Descripci�n.......................: Cancela la acci�n que se estuviera
' ...............................................: realizando y vuelve al estado de espera.
'**************************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
End Sub

Private Sub ClearFields()
'****************************************************************************************
' Nombre............................: ClearFields
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio................: 4/10/99
' Descripci�n.....................: Borra el contenido de los campos del form.
'****************************************************************************************
    Dim ctlTxt As TextBox
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    
    For Each ctlTxt In txtPt
        ctlTxt.Text = ""
        ctlTxt.Tag = ""
    Next
    optPt(3).Value = True
    blnChange = False
End Sub

Public Sub Insertar()
    mobjControlMDI.Alta
'    SetEditControls
    SetAddControls
    ' Cuando queremos dar de alta una
    ' nueva partida, en principio solo
    ' se tendr� acceso a la pesta�a de
    ' los datos generales, una vez gra-
    ' bados estos, se tendr� acceso a
    ' la pesta�a del tablero.
'    SSTab1.TabEnabled(1) = False
    ClearFields
    ' Mostramos en el tablero gr�fico la
    ' configuraci�n inicial de las piezas.
    InicioTablero
    ' Inicializamos el RecordSet de movimientos.
    Set rsMv = Nothing
End Sub

Private Sub SetEditControls()
'****************************************************************************
' Nombre........................: SetEditControls
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Habilita los controles
' ........................................: necesarios para
'.........................................: operaciones de busqueda y alta.
'****************************************************************************

    Dim ctlTxt As TextBox
    Dim ctlOpt As OptionButton
    Dim ctlCmd As CommandButton
    
    For Each ctlTxt In txtPt
        ActivarTxt ctlTxt
    Next
    For Each ctlOpt In optPt
        ActivarOpt ctlOpt
    Next
    For Each ctlCmd In cmdPt
        ActCmd ctlCmd
    Next
    ActivarCbo cboPt
    ActivarLstView lvwPt
    DesActCmd cmdPt(3)
    With SSTab1
        .Tab = 0
        .TabEnabled(0) = True
        .TabEnabled(1) = True
    End With
    optPt(4).Visible = False
    updPt.Enabled = True

End Sub

Private Sub ThrowSearch()
'****************************************************************************
' Nombre..........................: ThrowSearch
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio.............: 5/10/99
' Descripci�n..................: Lanza el proceso de b�squeda.
'****************************************************************************
    Dim strSQL As String
    
    Dim bytRslt As Byte
    
    On Error GoTo ThrowSearchError
    
    Clock 1
    strSQL = ""
    
    ' Buscar por los criterios seleccionados.
    ' Jugador con Blancas
    If Not txtPt(0).Tag = "" Then
        strSQL = "pt_blancas = " & Val(txtPt(0).Tag) & " AND "
    End If
    
    ' Jugador con Negras
    If Not txtPt(1).Tag = "" Then
        strSQL = strSQL & "pt_negras = " & _
                Val(txtPt(1).Tag) & " AND "
    End If
    
    ' ELO Blancas
    If Not Trim$(txtPt(2)) = "" Then
        strSQL = strSQL & "pt_elobl >= " & _
            Val(txtPt(2)) & " AND "
    End If
    
    ' ELO Negras
    If Not Trim$(txtPt(3)) = "" Then
        strSQL = strSQL & "pt_elong >= " & _
            Val(txtPt(3)) & " AND "
    End If
    
    ' Resultado
    If Not optPt(4).Value Then
        If optPt(0).Value = True Then
            bytRslt = 1
        ElseIf optPt(1).Value = True Then
            bytRslt = 2
        ElseIf optPt(2).Value = True Then
            bytRslt = 3
        ElseIf optPt(3).Value = True Then
            bytRslt = 4
        End If
        strSQL = strSQL & "pt_resultado = " & bytRslt & " AND "
    End If
    
    ' Apertura
    If Not txtPt(4).Tag = "" Then
        strSQL = strSQL & "pt_apertura = " & _
                Val(txtPt(4).Tag) & " AND "
    End If
            
    ' Tipo de Final
    If Not cboPt.ListIndex = -1 Then
        strSQL = strSQL & "pt_tipofin = " & _
                Val(cboPt.ItemData(cboPt.ListIndex)) & " AND "
    End If
    
    ' Modalidad
    If Not txtPt(5).Tag = "" Then
        strSQL = strSQL & "pt_modalidad = " & _
                Val(txtPt(5).Tag) & " AND "
    End If
    
    ' Comentarista
    strSQL = strSQL & "pt_anotador LIKE " & _
            SnglQuote("*" & txtPt(7) & "*") & " AND "
            
    ' Torneo
    If Not txtPt(6).Tag = "" Then
        strSQL = strSQL & "pt_torneo = " & _
                Val(txtPt(6)) & " AND "
    End If
    
    ' Fecha
    If IsDate(txtPt(8)) Then
        strSQL = strSQL & "pt_fecha = " & _
                CDate(txtPt(8)) & " AND "
    End If
    
    ' Lugar
    strSQL = strSQL & "pt_lugar LIKE " & _
            SnglQuote("*" & txtPt(9) & "*") & " AND "
            
    ' Ronda
    If IsNumeric(txtPt(11)) Then
        strSQL = strSQL & "pt_ronda = " & _
                Val(txtPt(11)) & " AND "
    End If
    
    If Len(strSQL) > 0 Then
        ' Quitamos el �ltimo AND
        strSQL = Left$(strSQL, Len(strSQL) - 5)
    End If
    
    If strSQL = "" Then
        ' Buscar todos
        strSQL = "Partidas"
    Else
        strSQL = "SELECT * FROM Partidas WHERE " & strSQL
    End If
    
    Set rsPt = dbDatos.OpenRecordset(strSQL)
    If Not rsPt.EOF Then
        DisplayRegs rsPt
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    
GetExit:
    Clock 0
    Exit Sub
    
ThrowSearchError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub SetSelectControls()
'************************************************************************************
' Nombre.............................: SetSelectControls
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Habilita los controles del formulario necesarios
'..............................................: para que el usuario pueda seleccionar entre los
' .............................................: registros recuperados en la b�squeda.
'***********************************************************************************

    Dim ctlTxt As TextBox
    Dim ctlOpt As OptionButton
    Dim ctlCmd As CommandButton
        
    mobjControlMDI.Buscar
    
    For Each ctlTxt In txtPt
        DesActivarTxt ctlTxt
    Next
    For Each ctlOpt In optPt
        DesActivarOpt ctlOpt
    Next
    optPt(4).Visible = False
    For Each ctlCmd In cmdPt
        DesActCmd ctlCmd
    Next
    updPt.Enabled = False
    cboPt.ListIndex = -1
    DesActivarCbo cboPt
    ActivarLstView lvwPt
    ActCmd cmdPt(3)
    cmdPt(3).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
        .Tab = 0
    End With
End Sub

Private Sub DisplayRegs(Rs As Recordset)
'************************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra en la lista de selecci�n los registros
'..............................................: recuperados en el proceso de b�squeda.
'***********************************************************************************

    Dim mItem As ListItem
    On Error GoTo DisplayRegsError
    
    lvwPt.ListItems.Clear
    With Rs
        Do While Not Rs.EOF
            ' Las claves (Key) de los items del lvw no pueden empezar
            ' por un n�mero, por lo que le colocamos una X delante.
            ' Es un poco chapuza pero no hay otra forma de salvar ese bug.
            Set mItem = lvwPt.ListItems.Add(, "X" & CStr(!pt_id), _
            mobjDatos.GetJugador(!pt_blancas))
            mItem.Tag = CStr(!pt_id)
            mItem.SubItems(1) = mobjDatos.GetJugador(!pt_negras)
            Select Case !pt_resultado
                Case 1      ' (1-0)
                    mItem.SubItems(2) = "1-0"
                Case 2      ' (0-1)
                    mItem.SubItems(2) = "0-1"
                Case 3      ' Tablas
                    mItem.SubItems(2) = "�-�"
                Case 4      ' Indeterminado
                    mItem.SubItems(2) = "*"
            End Select
            .MoveNext
        Loop
    End With
    
GetExit:
    Exit Sub
    
DisplayRegsError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub Grabar()
'****************************************************************************
' Nombre.........................: Grabar
' Tipo................................: Sub
' �mbito...........................: P�blico
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 7/10/99
' Descripci�n.................: Graba un reg. en la BB.DD.
' .........................................: con los datos introducidos.
'****************************************************************************
    Dim mItem As ListItem
    On Error GoTo GrabarError
    
    Clock 1
    ' Validamos datos.
    If Not DatosValidos() Then
        Clock 0
        Exit Sub
    End If
    ' Si el objeto RsTor a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If rsPt Is Nothing Then _
        Set rsPt = dbDatos.OpenRecordset("Partidas", dbOpenDynaset)
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            rsPt.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            rsPt.Edit
    End Select

    AsignarValores
    rsPt.Update
    If Estado = "EDICION" Then
'        With lvwPt
''            If Not .SelectedItem Is Nothing Then
'            Set mItem = lvwPt.FindItem(CStr(!pt_id), lvwTag)
'            If Not mItem Is Nothing Then
'                .ListItems.Remove (mItem.Index)
'            End If
'        End With
        If lvwPt.ListItems.Count > 0 Then
            With rsPt
    '                Set mItem = lvwApes.SelectedItem
                Set mItem = lvwPt.FindItem(CStr(!pt_id), lvwTag)
                If Not mItem Is Nothing Then
                    mItem.Text = mobjDatos.GetJugador(!pt_blancas)
                    mItem.SubItems(1) = mobjDatos.GetJugador(!pt_negras)
                    Select Case !pt_resultado
                        Case 1
                            mItem.SubItems(2) = "1-0"
                        Case 2
                            mItem.SubItems(2) = "0-1"
                        Case 3
                            mItem.SubItems(2) = "�-�"
                        Case 4      ' Indeterminado
                            mItem.SubItems(2) = "*"
                    End Select
                End If
            End With
        End If
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    
    ' Si hemos grabado los datos generales tendremos
    ' acceso a la pesta�a del tablero.
    If SSTab1.Tab = 0 Then
        SSTab1.TabEnabled(1) = True
        ' Recuperamos la �ltima partida
        ' grabada.
        Dim sSql As String
        sSql = "SELECT * FROM Partidas " & _
            "WHERE pt_id=(SELECT MAX(pt_id) FROM Partidas)"
        Set rsPt = dbDatos.OpenRecordset(sSql)
        mobjControlMDI.Edicion
    Else
        ClearFields
        SetSelectControls
        mobjControlMDI.Espera
    End If
GetExit:
    Clock 0
    Exit Sub
    
GrabarError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Function DatosValidos() As Boolean
'****************************************************************************
' Nombre...........................: DatosValidos
' Tipo..................................: Funci�n
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Booleano
' �ltimo cambio..............: 7/10/99
' Descripci�n...................: Valida los datos de la partida antes
' ...........................................: de grabar reg. en la tabla "Partidas".
'****************************************************************************

    Dim msg$, cCtrlErr As Control, iCod%, j%
    Dim FechaValida As Variant
    On Error GoTo ValidarError

    msg = "": iCod = 0: DatosValidos = True
    
    ' Jugador Blanco
    If Not IsNumeric(txtPt(0).Tag) Then
        Set cCtrlErr = txtPt(0)
        msg = "Es obligatorio especificar el jugador del bando blanco."
        iCod = 1
    End If
    
    ' ELO Blanco
    If iCod = 0 Then
        If Not IsNumeric(txtPt(2)) Then
            If Not Trim$(txtPt(2)) = "" Then
                Set cCtrlErr = txtPt(2)
                msg = "El campo ELO es num�rico."
                iCod = 2
            End If
        Else
            If CDbl(txtPt(2)) < 0 Or CDbl(txtPt(2)) > 9999 Then
                Set cCtrlErr = txtPt(2)
                msg = "El valor ELO tiene que estar comprendido" & _
                    " entre 0 y 9.999."
                iCod = 3
            End If
        End If
    End If
    
    ' Jugador Negro
    If iCod = 0 Then
        If Not IsNumeric(txtPt(1).Tag) Then
            Set cCtrlErr = txtPt(1)
            msg = "Es obligatorio especificar el jugador del bando negro."
            iCod = 3
        End If
    End If
    
    ' ELO Negro
    If iCod = 0 Then
        If Not IsNumeric(txtPt(3)) Then
            If Not Trim$(txtPt(3)) = "" Then
                Set cCtrlErr = txtPt(3)
                msg = "El campo ELO es num�rico."
                iCod = 4
            End If
        Else
            If CDbl(txtPt(3)) < 0 Or CDbl(txtPt(3)) > 9999 Then
                Set cCtrlErr = txtPt(3)
                msg = "El valor ELO tiene que estar comprendido" & _
                    " entre 0 y 9.999."
                iCod = 5
            End If
        End If
    End If
    
    ' Comentarista
    If iCod = 0 And Len(txtPt(7)) > 35 Then
        Set cCtrlErr = txtPt(7)
        msg = "Longitud m�xima: 35 caracteres."
        iCod = 5
    End If
    
    ' Fecha
    If iCod = 0 Then
        FechaValida = Validar(txtPt(8))
        If FechaValida = False Then
            Set cCtrlErr = txtPt(8)
            msg = "Formato de fecha 'dd/mm/aaaa'."
            iCod = 6
        ElseIf FechaValida = True Then
            ' Fecha nula
            txtPt(8) = ""
        Else
            txtPt(8) = FechaValida
        End If
    End If
    
    ' Lugar
    If iCod = 0 And Len(txtPt(9)) > 20 Then
        Set cCtrlErr = txtPt(9)
        msg = "Longitud m�xima: 20 caracteres."
        iCod = 7
    End If
    
    ' Notas
    If iCod = 0 And Len(txtPt(10)) > 2500 Then
        Set cCtrlErr = txtPt(10)
        msg = "Longitud m�xima: 2.500 caracteres."
        iCod = 8
    End If
    
    ' Ronda
    If iCod = 0 Then
        If Trim$(txtPt(11)) = "" Then
            txtPt(11) = "0"
        ElseIf Not IsNumeric(txtPt(11)) Then
            Set cCtrlErr = txtPt(11)
            msg = "La ronda es un tipo num�rico."
            iCod = 9
        ElseIf CDbl(txtPt(11)) < 0 Or CDbl(txtPt(11)) > 255 Then
            Set cCtrlErr = txtPt(11)
            msg = "Intervalo de valores permitido: 0..255."
            iCod = 9
        End If
    End If
    
    ' Controlamos que el mismo jugador
    ' no figure en los dos bandos.
    If iCod = 0 Then
        If Val(txtPt(0).Tag) = Val(txtPt(1).Tag) Then
            Set cCtrlErr = txtPt(1)
            msg = "No se permite el mismo jugador en los dos bandos."
            iCod = 10
        End If
    End If
    
    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
    
GetExit:
    Exit Function
    
ValidarError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    DatosValidos = False
    Resume GetExit

End Function

Private Sub AsignarValores()
'****************************************************************************
' Nombre.......................: AsignarValores
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros...............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 7/10/99
' Descripci�n..............: Asigna los valores que el usuario
'.......................................: ha introducido en el formulario a la clase.
'****************************************************************************
    On Error GoTo AsignarError
    
    With rsPt
        !pt_blancas = Val(txtPt(0).Tag)
        !pt_negras = Val(txtPt(1).Tag)
        !pt_torneo = IIf(txtPt(6).Tag = "", Null, Val(txtPt(6).Tag))
        !pt_lugar = txtPt(9)
        !pt_fecha = IIf(Trim$(txtPt(8)) = "", Null, txtPt(8))
        If optPt(0) Then
            !pt_resultado = 1
        ElseIf optPt(1) Then
            !pt_resultado = 2
        ElseIf optPt(2) Then
            !pt_resultado = 3
        ElseIf optPt(3) Then
            !pt_resultado = 4
        End If
        !pt_elobl = IIf(Trim$(txtPt(2)) = "", Null, txtPt(2))
        !pt_elong = IIf(Trim$(txtPt(3)) = "", Null, txtPt(3))
        !pt_apertura = IIf(txtPt(4).Tag = "", Null, Val(txtPt(4).Tag))
        !pt_anotador = txtPt(7)
        If Not cboPt.ListIndex = -1 Then
            !pt_tipofin = cboPt.ItemData(cboPt.ListIndex)
        Else
            !pt_tipofin = Null
        End If
        !pt_modalidad = IIf(txtPt(5).Tag = "", Null, Val(txtPt(5).Tag))
        !pt_notas = txtPt(10)
        !pt_ronda = txtPt(11)
    End With
    
GetExit:
    Exit Sub
    
AsignarError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Public Sub Borrar()
'****************************************************************************
' Nombre.........................: Borrar
' Tipo................................: Sub
' �mbito...........................: P�blico
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio............: 7/10/99
' Descripci�n................: Da de baja un registro.
'****************************************************************************
    Dim Respuesta   As Integer
    On Error GoTo BorrarError
    
    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
    
GetExit:
    Exit Sub
    
BorrarError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub Delete()
    
    On Error GoTo errDel
    
    With lvwPt
        If Not .SelectedItem Is Nothing Then
            .ListItems.Remove .SelectedItem.Index
        End If
    End With
    rsPt.Delete
    ClearFields
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number
    Resume Next
End Sub

Private Sub lstPt_Click(Index As Integer)
    On Error GoTo LstClickError
    
    Select Case Index
        Case 5
            ' Lista de movimientos.
            MostrarTablero lstPt(5).ListIndex
            ConfigButtons
            If lstPt(5).ListIndex = 0 Then
                ' Si estamos en la posici�n inicial
                ' no se permite introducir comentario.
                DesActivarTxt txtPt(12)
            Else
                ActivarTxt txtPt(12)
            End If
    End Select
    
GetExit:
    Exit Sub
    
LstClickError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub lstPt_DblClick(Index As Integer)
    On Error GoTo LstDblClickError

    Select Case Index
        Case 0      ' Ayuda de campo Jug. Blancas
            With lstPt(Index)
                txtPt(0) = .List(.ListIndex)
                txtPt(0).Tag = .ItemData(.ListIndex)
                .Visible = False
                ' Etiqueta del tablero
                lblPt(30).Caption = txtPt(0)
            End With
            txtPt(2).SetFocus
        Case 1      ' Ayuda de campo Jug. Negras
            With lstPt(Index)
                txtPt(1) = .List(.ListIndex)
                txtPt(1).Tag = .ItemData(.ListIndex)
                .Visible = False
                ' Etiqueta del tablero
                lblPt(31).Caption = txtPt(1)
            End With
            txtPt(3).SetFocus
        Case 2      ' Ayuda de campo Aperturas
            With lstPt(Index)
                txtPt(4) = .List(.ListIndex)
                txtPt(4).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            cboPt.SetFocus
        Case 3      ' Ayuda de campo Modalidades
            With lstPt(Index)
                txtPt(5) = .List(.ListIndex)
                txtPt(5).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            txtPt(7).SetFocus
        Case 4      ' Ayuda de campo Torneos
            With lstPt(Index)
                txtPt(6) = .List(.ListIndex)
                txtPt(6).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            txtPt(8).SetFocus
    End Select
    
GetExit:
    Exit Sub
    
LstDblClickError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub lstPt_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo LstKeyPressError

    Select Case Index
        Case 0      ' Ayuda de campo Jug. Blancas.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstPt(Index).Visible = False
                    txtPt(0).Tag = ""
                    txtPt(0) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstPt_DblClick 0
            End Select
            
        Case 1      ' Ayuda de campo Jug. Negras.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstPt(Index).Visible = False
                    txtPt(1).Tag = ""
                    txtPt(1) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstPt_DblClick 1
            End Select
            
        Case 2      ' Ayuda de campo Aperturas.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstPt(Index).Visible = False
                    txtPt(4).Tag = ""
                    txtPt(4) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstPt_DblClick 2
            End Select
            
        Case 3      ' Ayuda de campo Modalidades
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstPt(Index).Visible = False
                    txtPt(5).Tag = ""
                    txtPt(5) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstPt_DblClick 3
            End Select
            
        Case 4      ' Ayuda de campo Torneos
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstPt(Index).Visible = False
                    txtPt(6).Tag = ""
                    txtPt(6) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstPt_DblClick 4
            End Select
    End Select
    
GetExit:
    Exit Sub
    
LstKeyPressError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub lstPt_LostFocus(Index As Integer)
    Select Case Index
        Case 0, 1, 2, 3, 4
            lstPt(Index).Visible = False
    End Select
End Sub

Private Sub lvwPt_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
    ' Ordenar el lvw
    With lvwPt
        .SortKey = ColumnHeader.Index - 1
        .Sorted = True
    End With

End Sub

Private Sub lvwPt_ItemClick(ByVal Item As ComctlLib.ListItem)
    On Error GoTo ItemClickError
    
    Clock 1
    CheckChanges blnChange
    ' Mostramos en el tablero gr�fico la
    ' configuraci�n inicial de las piezas.

    If rsPt.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        rsPt.Index = "pt_id"
        rsPt.Seek "=", CLng(Val(Mid$(Item.Key, 2, Len(Item.Key) - 1)))
    Else
        ' Utilizamos el m�todo FIND si el Recordset es DYNASET o SNAPSHOT
        rsPt.FindFirst "pt_id =" & CLng(Val(Mid$(Item.Key, 2, Len(Item.Key) - 1)))
    End If
    SetEditControls
    mobjControlMDI.Edicion
    DisplayFields
    GetMoves rsPt!pt_id
    
GetExit:
    Clock 0
    Exit Sub
    
ItemClickError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub optPt_Click(Index As Integer)
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    ' Inicializamos el bot�n de Reproducci�n para
    ' que no d� problemas si anteriormente estaba
    ' reproduci�ndose una partida en modo autom�-
    ' tico

    If PreviousTab = 1 And cmdPt(11).Caption = "&Manual" Then
        cmdPt_Click 11
    End If
End Sub

Private Sub tmrPt_Timer()
'****************************************************************************
' Nombre........................: tmrPt_Timer
' Tipo...............................: Evento Sub
' �mbito..........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 14/1/00
' Descripci�n...............: Cuando se desencadene el evento
' .......................................: timer se avanza una posici�n si
' .......................................: es posible.
'****************************************************************************

    If cmdPt(6).Enabled Then
        Adelante
    Else
        cmdPt_Click 11
    End If
    
End Sub

Private Sub txtPt_Change(Index As Integer)
    Select Case Index
        Case 0, 1, 4, 5, 6
            If txtPt(Index) = "" Then txtPt(Index).Tag = ""
    End Select
    If Not Estado = "BUSCAR" And Not Index = 12 Then blnChange = True
End Sub

Private Sub txtPt_GotFocus(Index As Integer)
    Select Case Index
        Case 0, 1, 4, 5, 6
            With txtPt(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
    txtPt(Index).BackColor = CT_COLOR_CRUDO
End Sub

Private Sub DisplayFields()
'*************************************************************************************
' Nombre.......................: DisplayFields
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 12/10/99
' Descripci�n...............: Muestra en el form los datos del registro le�do.
'*************************************************************************************
    Dim strSQL As String
    On Error GoTo DisplayFieldsError

    ClearFields
    With rsPt
        txtPt(0) = mobjDatos.GetJugador(CheckForNull(!pt_blancas, "String"))
        txtPt(0).Tag = !pt_blancas
        ' Etiqueta de tablero.
        lblPt(30).Caption = txtPt(0)
        txtPt(1) = mobjDatos.GetJugador(CheckForNull(!pt_negras, "String"))
        txtPt(1).Tag = !pt_negras
        ' Etiqueta de tablero.
        lblPt(31).Caption = txtPt(1)
        txtPt(2) = IIf(IsNull(!pt_elobl), "", Format$(!pt_elobl, "#,###"))
        txtPt(3) = IIf(IsNull(!pt_elong), "", Format$(!pt_elong, "#,###"))
        Select Case CheckForNull(!pt_resultado, "Byte")
            Case 1      ' 1-0
                optPt(0).Value = True
            Case 2      ' 0-1
                optPt(1).Value = True
            Case 3      ' 1/2 - 1/2
                optPt(2).Value = True
            Case 4      ' Indeterminado
                optPt(3).Value = True
            Case Else   ' Otros casos excepcionales
                optPt(3).Value = True
        End Select
        txtPt(4) = mobjDatos.GetApertura(CheckForNull(!pt_apertura, "Long"))
        txtPt(4).Tag = IIf(IsNull(!pt_apertura), "", !pt_apertura)
        SearchCboFinal CheckForNull(!pt_tipofin, "Long")
        txtPt(5) = mobjDatos.GetModalidad(CheckForNull(!pt_modalidad, "Long"))
        txtPt(5).Tag = IIf(IsNull(!pt_modalidad), "", !pt_modalidad)
        txtPt(6) = mobjDatos.GetTorneo(CheckForNull(!pt_torneo, "Long"))
        txtPt(6).Tag = IIf(IsNull(!pt_torneo), "", !pt_torneo)
        txtPt(7) = CheckForNull(!pt_anotador, "String")
        txtPt(8) = IIf(IsNull(!pt_fecha), "", Format$(!pt_fecha, "dd/mm/yyyy"))
        txtPt(9) = CheckForNull(!pt_lugar, "String")
        txtPt(11) = CheckForNull(!pt_ronda, "Byte")
        txtPt(10) = CheckForNull(!pt_notas, "String")
    End With
    blnChange = False
    
GetExit:
    Exit Sub
    
DisplayFieldsError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub SearchCboFinal(lngKey As Long)
'****************************************************************************
' Nombre.........................: SearchCboFinal
' Tipo................................: Function
' �mbito...........................: Privado
' Par�metros.................: Long -> Id. Tipo Final
' Valor de retorno.........: String ->Den. Tipo Final
' �ltimo cambio............: 12/10/99
' Descripci�n.................: Busca en el combo de tipos de Finales
' .........................................: la denominaci�n del final cuya clave
' .........................................: se pasa como par�metro.
'****************************************************************************
    Dim Idx As Byte
    On Error GoTo SearchCboFinalError
    
    With cboPt
        Idx = 0
        Do While Idx <= .ListCount - 1
            If .ItemData(Idx) = lngKey Then
                .ListIndex = Idx
                Exit Sub
            Else
                Idx = Idx + 1
            End If
        Loop
            .ListIndex = -1
    End With
    
GetExit:
    Exit Sub
    
SearchCboFinalError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub txtPt_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 2, 3, 11  ' ELO's y Ronda
            Select Case KeyAscii
                Case vbKeyReturn
                    KeyAscii = vbKeyShift
            End Select
        Case 0
            ' Ayuda de campo Blancas
            If KeyAscii = vbKeyReturn Then cmdPt_Click 8
        Case 1
            ' Ayuda de campo Negras
            If KeyAscii = vbKeyReturn Then cmdPt_Click 9
        Case 4
            ' Ayuda de campo Apertura
            If KeyAscii = vbKeyReturn Then cmdPt_Click 0
        Case 5
            ' Ayuda de campo Modalidad
            If KeyAscii = vbKeyReturn Then cmdPt_Click 1
        Case 6
            ' Ayuda de campo Torneo
            If KeyAscii = vbKeyReturn Then cmdPt_Click 2
    End Select
End Sub

Private Sub txtPt_LostFocus(Index As Integer)
    Select Case Index
        Case 2, 3, 11  ' ELO's y Ronda
            If IsNumeric(txtPt(Index)) Then
                txtPt(Index) = Format$(txtPt(Index), "#,##0")
            End If
        Case 8  ' Fecha
            If IsNumeric(txtPt(Index)) And _
                Len(txtPt(Index)) = 8 Then
                txtPt(Index) = Format$(txtPt(Index), "00/00/0000")
            End If
    End Select
    txtPt(Index).BackColor = CT_COLOR_BLANCO
End Sub

Private Sub GetMoves(lngIdPart As Long)
'****************************************************************************
' Nombre........................: GetMoves
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Long -> Id. de partida
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/11/99
' Descripci�n................: Recupera de la BB.DD. los movimientos
' ........................................: que forman parte de la partida cuyo
' ........................................: identificativo se pasa como par�metro.
'****************************************************************************
    Dim strSQL As String
    Dim iPos As Integer
    Dim vntResultadoMv As Variant
    On Error GoTo GetMovesError
    
    ' Recuperamos de la tabla "Movimientos" todos aquellos correspondientes
    ' a la partida seleccionada.
    strSQL = "SELECT * FROM Movimientos WHERE mv_partida = " & lngIdPart
    Set rsMv = dbDatos.OpenRecordset(strSQL, dbOpenDynaset)
    ' Inicializamos la matriz de posiciones para comenzar a introducir los
    ' movimientos recogidos de la BB.DD.
    iPos = 0
    InicioTablero

    ' Cargamos los movimientos registrados para la partida.
    With rsMv
        Do While Not .EOF
            ' Para todos los movimientos validaremos su legalidad a trav�s
            ' de la clase "mobjTablero" y a su vez las posiciones de piezas
            ' se ir�n almacenando en la matriz de posiciones.
            vntResultadoMv = _
                mobjTablero.Mover(!mv_orig \ 10, !mv_orig Mod 10, !mv_dest \ 10, !mv_dest Mod 10)
            If Not vntResultadoMv = False Then
                ' Si el movimiento es legal se almacena en la matriz de posiciones
                iPos = iPos + 1
                AddMove iPos, CStr(vntResultadoMv)
            End If
            ' Leemos el siguiente movimiento
            .MoveNext
        Loop
    End With
    ' Una vez cargados todos los movimientos nos situaremos
    ' en la primera posici�n registrada.
    Primero
    
GetExit:
    Exit Sub
    
GetMovesError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit
    
End Sub

Private Sub MostrarTablero(PosicionNueva As Integer)
'******************************************************************************************
' Nombre........................: MostrarTablero
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros................:  Integer
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 22/11/99
' Descripci�n...............: Muestra sobre el tablero de pantalla la posici�n
' .......................................: correspondiente al movimiento pasado como
' .......................................: par�metro, actualizando el contador de movimiento.
'******************************************************************************************
    Dim iCol As Byte, iFil As Byte
    Dim bytCasilla As Byte
    On Error GoTo MostrarPosicionError
    
    For iCol = 1 To 8
        For iFil = 1 To 8
            If Not mMatrizPosiciones(iCol, iFil, mintPosicion) = _
                    mMatrizPosiciones(iCol, iFil, PosicionNueva) Then
                    ' Actualizamos solo las casillas que han cambiado
                    ' respecto a la �ltima posici�n mostrada en pantalla
                bytCasilla = iCol * 10 + iFil
                With ilsPt
                    Select Case mMatrizPosiciones(iCol, iFil, PosicionNueva)
                    
                        Case CodPeonBlanco
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "PB")
                                
                        Case CodTorreBlanca
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "TB")
                        
                        Case CodCaballoBlanco
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "CB")
                        
                        Case CodAlfilBlanco
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "AB")
                        
                        Case CodDamaBlanca
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "DB")
    
                        Case CodReyBlanco
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "RB")
                                
                        Case CodPeonNegro
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "PN")
                                
                        Case CodTorreNegra
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "TN")
                                
                        Case CodCaballoNegro
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "CN")
                                
                        Case CodAlfilNegro
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "AN")
                                
                        Case CodDamaNegra
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "DN")
                                
                        Case CodReyNegro
                            Set imgCasilla(bytCasilla).Picture = _
                                .Overlay(CStr(imgCasilla(bytCasilla).Tag), "RN")
                                
                        Case CODCASILLAVACIA
                            Set imgCasilla(bytCasilla).Picture = _
                                .ListImages(CStr(imgCasilla(bytCasilla).Tag)).Picture
                                
                    End Select
                End With
            End If
        Next iFil
    Next iCol
    
    ' Si no es la posici�n inicial, mostramos el
    ' comentario asociado al movimiento realizado.
    If Not PosicionNueva = 0 Then
        rsMv.Bookmark = mMatrizMarcadores(PosicionNueva)
        txtPt(12) = CheckForNull(rsMv!mv_coment, "String")
    Else
        txtPt(12) = ""
    End If
    ' Actualizamos el marcador de posiciones.
    mintPosicion = PosicionNueva
    
GetExit:
    Exit Sub
    
MostrarPosicionError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub Primero()
'****************************************************************************************
' Nombre......................: Primero
' Tipo.............................: Sub
' �mbito........................: Privado
' Par�metros...............: -
' Valor de retorno.......: -
' �ltimo cambio..........: 23/11/99
' Descripci�n...............: Muestra sobre el tablero de pantalla
' .......................................: la posici�n correspondiente al primer movimiento.
'****************************************************************************************
    ' Se�alamos en la lista de movimientos
    ' de pantalla.
    lstPt(5).ListIndex = 0

    ConfigButtons
End Sub

Private Sub Atras()
'***************************************************************************************
' Nombre........................: Atras
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 23/11/99
' Descripci�n................: Muestra sobre el tablero de pantalla la posici�n
' ........................................: correspondiente al movimiento anterior.
'****************************************************************************************
    ' Se�alamos en la lista de movimientos
    ' de pantalla.
    lstPt(5).ListIndex = mintPosicion - 1

    ConfigButtons
End Sub

Private Sub Adelante()
'***************************************************************************************
' Nombre........................: Adelante
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 23/11/99
' Descripci�n................: Muestra sobre el tablero de pantalla la posici�n
' ........................................: correspondiente al movimiento anterior.
'***************************************************************************************
    ' Se�alamos en la lista de movimientos
    ' de pantalla.
    lstPt(5).ListIndex = mintPosicion + 1
    
    ConfigButtons
End Sub

Private Sub Ultimo()
'****************************************************************************************
' Nombre.........................: Ultimo
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: -
' Valor de retorno.........: -
' �ltimo cambio............: 23/11/99
' Descripci�n.................: Muestra sobre el tablero de pantalla la posici�n
' .........................................: correspondiente al ultimo movimiento.
'****************************************************************************************
    ' Se�alamos en la lista de movimientos
    ' de pantalla.
    lstPt(5).ListIndex = UBound(mMatrizPosiciones, 3)

    ConfigButtons
End Sub

Private Sub ConfigButtons()
'*****************************************************************************************
' Nombre............................: ConfigButtons
' Tipo...................................: Sub
' �mbito..............................: Privado
' Par�metros.....................: Ninguno
' Valor de retorno............: Ninguno
' �ltimo cambio...............: 29/11/99
' Descripci�n....................: Configura los botones de navegaci�n teniendo
' ............................................: en cuenta las posibilidades de desplazamiento
' ............................................: a trav�s de la matriz de posiciones.
'*****************************************************************************************
    ' Inicialmente, habilitamos todos los botones de navegaci�n.
    ActCmd cmdPt(4)
    ActCmd cmdPt(5)
    ActCmd cmdPt(6)
    ActCmd cmdPt(7)
    
    If mintPosicion = LBound(mMatrizPosiciones, 3) Then
        ' Si estamos en la 1� posici�n de la matriz de posiciones
        ' deshabilitaremos los botones de retroceso:
        ' PRIMERO
        DesActCmd cmdPt(4)
        ' ANTERIOR
        DesActCmd cmdPt(5)
    End If
    If mintPosicion = UBound(mMatrizPosiciones, 3) Then
        ' Si estamos en la �ltima posici�n de la matriz de
        ' posiciones, deshabilitaremos los botones de avance:
        ' SIGUIENTE
        DesActCmd cmdPt(6)
        'ULTIMO
        DesActCmd cmdPt(7)
    End If
End Sub

Private Sub AddMove(Position As Integer, Notation As String)
'**************************************************************************************
' Nombre....................: AddMove
' Tipo...........................: Sub
' �mbito......................: Privado
' Par�metros.............: Integer, string
' Valor de retorno.....: -
' �ltimo cambio........: 30/11/99
' Descripci�n.............: A�ade un movimiento a la partida en proceso,
' .....................................: para lo cual a�ade el tablero resultante en la
' .....................................: matriz de posiciones, la l�nea de movimiento en
' .....................................: la lista de pantalla y el marcador de la l�nea de
' .....................................: movimiento en el recordset rsMv.
'***************************************************************************************
    Dim iCol As Byte, iFil As Byte
    Dim strMovLst As String
    On Error GoTo AddMoveError
    
    ReDim Preserve mMatrizPosiciones(1 To 8, 1 To 8, 0 To Position)
    ' En este punto habr�a que traspasar la actual informaci�n de mobjTablero.Escaque
    ' a  mMatrizPosiciones(mobjTablero.Turno,-,-,mobjTablero.NumMovto)
    For iCol = 1 To 8
        For iFil = 1 To 8
            mMatrizPosiciones(iCol, iFil, Position) = mobjTablero.Escaque(iCol, iFil)
        Next iFil
    Next iCol
    ' Incluimos la notaci�n del movimiento en la
    ' lista mostrada por pantalla.
    If mobjTablero.Turno = BandoNegro Then
        ' El anterior movimiento ha sido del bando blanco,
        ' tenemos que incluir el n�mero de movimiento.
        strMovLst = CStr(mobjTablero.NumMovto & "." & vbTab & Notation)
    Else
        ' El anterior movimiento ha sido del bando negro.
        strMovLst = CStr(vbTab & Notation)
    End If
    With lstPt(5)
        ' Mostramos en el control ListBox el movimiento introducido
        .AddItem strMovLst, Position
        ' En la propiedad ITEMDATA del control ListBox almacenamos
        ' la posibilidad de enroque de ambos bandos codificada.
        .ItemData(.NewIndex) = mobjTablero.CodificarEnroques
    End With
    ' Guardamos el marcador al registro.
    ReDim Preserve mMatrizMarcadores(1 To Position)
    mMatrizMarcadores(Position) = rsMv.Bookmark
    
GetExit:
    Exit Sub
    
AddMoveError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub GirarTablero()
'***************************************************************************
' Nombre.......................: GirarTablero
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio..........: 11/12/99
' Descripci�n...............: Cambia de posici�n los controles que
' .......................................: que forman el tablero en pantalla
' .......................................: para girarlo.
'***************************************************************************

    Dim C1, F1, C2, F2 As Byte
    Dim Aux1, Aux2 As Integer
    Dim Tmp As String
    
    ' Giramos las casillas.
    fraPt(6).Visible = False
    C1 = 1
    F1 = 1
    C2 = 8
    F2 = 8
    Do While (F1 < 5 And F2 > 4)
        Do While (C1 < 9 And C2 > 0)
            Aux1 = imgCasilla(C1 * 10 + F1).Left
            Aux2 = imgCasilla(C1 * 10 + F1).Top
            imgCasilla(C1 * 10 + F1).Left = _
                imgCasilla(C2 * 10 + F2).Left
            imgCasilla(C1 * 10 + F1).Top = _
                imgCasilla(C2 * 10 + F2).Top
            imgCasilla(C2 * 10 + F2).Left = Aux1
            imgCasilla(C2 * 10 + F2).Top = Aux2
            C1 = C1 + 1
            C2 = C2 - 1
        Loop
        F1 = F1 + 1
        F2 = F2 - 1
        C1 = 1
        C2 = 8
    Loop
    fraPt(6).Visible = True
    
    ' Giramos las etiquetas de columnas.
    C1 = 14
    C2 = 21
    Do While (C1 < 18 And C2 > 17)
        Tmp = lblPt(C1).Caption
        lblPt(C1).Caption = lblPt(C2).Caption
        lblPt(C2).Caption = Tmp
        C1 = C1 + 1
        C2 = C2 - 1
    Loop
    
    ' Giramos las etiquetas de filas.
    C1 = 22
    C2 = 29
    Do While (C1 < 26 And C2 > 25)
        Tmp = lblPt(C1).Caption
        lblPt(C1).Caption = lblPt(C2).Caption
        lblPt(C2).Caption = Tmp
        C1 = C1 + 1
        C2 = C2 - 1
    Loop
    
    ' Giramos las etiquetas de nombre de jugadores
    Aux1 = lblPt(30).Left
    Aux2 = lblPt(30).Top
    lblPt(30).Left = lblPt(31).Left
    lblPt(30).Top = lblPt(31).Top
    lblPt(31).Left = Aux1
    lblPt(31).Top = Aux2
    
End Sub

Private Sub CambiaModoRep()
'****************************************************************************************
' Nombre.....................: CambiaModoRep
' Tipo............................: Sub
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 11/12/99
' Descripci�n..............: Sirve para conmutar el modo de reproducci�n
' ......................................: de las partidas: autom�tico-manual. En el caso
' ......................................: del autom�tico muestra el formulario "frmTiempo"
' ......................................: de forma modal para que el usuario pueda
' ......................................: configurar el intervalo de tiempo deseado.
'****************************************************************************************

    If cmdPt(11).Caption = "&Auto" Then
        frmTiempo.Show vbModal
        cmdPt(11).Caption = "&Manual"
        cmdPt(11).ToolTipText = "Cambia a modo de reproducci�n manual."
        tmrPt.Enabled = True
    ElseIf cmdPt(11).Caption = "&Manual" Then
        cmdPt(11).Caption = "&Auto"
        cmdPt(11).ToolTipText = "Cambia a modo de reproducci�n autom�tico."
        tmrPt.Enabled = False
    End If
    
End Sub

Private Sub SetAddControls()
'****************************************************************************
' Nombre........................: SetAddControls
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Habilita los controles
' ........................................: necesarios para
'.........................................: operaciones de alta.
'****************************************************************************

    Dim ctlTxt As TextBox
    Dim ctlOpt As OptionButton
    Dim ctlCmd As CommandButton
    
    For Each ctlTxt In txtPt
        ActivarTxt ctlTxt
    Next
    For Each ctlOpt In optPt
        ActivarOpt ctlOpt
    Next
'    optPt(3).Value = True
    For Each ctlCmd In cmdPt
        ActCmd ctlCmd
    Next
    ActivarCbo cboPt
    ActivarLstView lvwPt
    DesActCmd cmdPt(3)
    cmdPt(3).Enabled = False
    With SSTab1
        .Tab = 0
        .TabEnabled(0) = True
        .TabEnabled(1) = False
    End With
    DesActCmd cmdPt(4)
    DesActCmd cmdPt(5)
    DesActCmd cmdPt(6)
    DesActCmd cmdPt(7)
    optPt(4).Visible = False
    updPt.Enabled = True
End Sub

Private Sub SaveComent()
'****************************************************************************
' Nombre......................: SaveComent
' Tipo.............................: Sub
' �mbito........................: Privado
' Par�metros...............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 29/02/00
' Descripci�n..............: Graba el comentario introducido para
' ......................................: la posici�n actualmente mostrada en
' ......................................: pantalla.
'*****************************************************************************
    
    With rsMv
        If Not .EOF Then
            .Edit
            !mv_coment = txtPt(12)
            .Update
        End If
    End With
End Sub

Private Sub PrintMatch()
'********************************************************************
' Nombre.........................: PrintMatch
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: -
' Valor de retorno.........: -
' �ltimo cambio............: 7/3/00
' Descripci�n.................: Imprime la partida actualmente
' .........................................: seleccionada.
'********************************************************************

    Dim strLinea As String
    Dim idxLst As Long
    Dim Y As Variant
    Dim sResultado As String

    
    ' Imprime el encabezado.
    Printer.Font.Size = 12
    Printer.Font.Bold = True
    
    ' Establece la posici�n de la l�nea.
    Y = Printer.CurrentY + 10
    ' Dibuja una l�nea atravesando la p�gina.
    Printer.Line (0, Y)-(Printer.ScaleWidth, Y)
    
    Select Case ValorOptResult()
        Case 0
            sResultado = "1-0"
        Case 1
            sResultado = "0-1"
        Case 2
            sResultado = "Tablas"
        Case 3
            sResultado = "Indeterminado"
    End Select
    
    Printer.Print
    ' Imprime el nombre del Jugador Blanco.
    Printer.Print txtPt(0).Text
    ' Imprime el nombre del jugador negro.
    Printer.Print txtPt(1).Text
    
    Printer.Print
    Printer.Print sResultado
    Printer.Font.Size = 10
    Printer.Font.Bold = False
    
    Printer.Print
    ' Imprime el lugar y la fecha.
    Printer.Print txtPt(9) & txtPt(8)
    ' Imprime el torneo y la ronda.
    Printer.Print txtPt(6) & _
        IIf(Trim$(txtPt(11)) = "", "", " (" & txtPt(11) & "� ronda)")
    ' Imprime la apertura.
    Printer.Print txtPt(4)
    ' Establece la posici�n de la l�nea.
    Y = Printer.CurrentY + 10
    ' Dibuja una l�nea atravesando la p�gina.
    Printer.Line (0, Y)-(Printer.ScaleWidth, Y)
        
    Printer.Print
    Printer.Print
    
    
    With lstPt(5)
        For idxLst = 1 To .ListCount - 1 Step 4
            strLinea = .List(idxLst)
            strLinea = strLinea & _
                IIf(idxLst + 1 = .ListCount, "", .List(idxLst + 1))
            strLinea = strLinea & vbTab & vbTab
            strLinea = strLinea & _
                IIf(idxLst + 2 = .ListCount, "", .List(idxLst + 2))
            strLinea = strLinea & _
                IIf(idxLst + 3 = .ListCount, "", .List(idxLst + 3))
            Printer.Print strLinea
        Next idxLst
    End With
    
    Printer.EndDoc
    
End Sub

Private Function ValorOptResult() As Byte
'*********************************************************************************
' Nombre.........................: ValorOptResult
' Tipo................................: Function
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Byte -> C�digo del resultado seleccionado
' .........................................:                en el control "OptionButton"
' �ltimo cambio............: 7/3/00
' Descripci�n.................: Devuelve �ndice del bot�n
' .........................................: de opciones que tiene la
' .........................................: propiedad Value = True.
'**********************************************************************************
    If optPt(0) Then
        ValorOptResult = 0
    ElseIf optPt(1) Then
        ValorOptResult = 1
    ElseIf optPt(2) Then
        ValorOptResult = 2
    ElseIf optPt(3) Then
        ValorOptResult = 3
    End If
End Function

Private Sub CargarPartidaPGN()
'****************************************************************************
' Nombre........................: CargarPartidaPGN
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Abre un fichero con una partida en formato PGN
' ........................................: e incorpora todos los movimientos y parte de los
'.........................................: datos de forma diferida a la partida actualmente
' ........................................: seleccionada.
'****************************************************************************
    AbrirFichero rsPt!pt_id
    GetMoves rsPt!pt_id
End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub


