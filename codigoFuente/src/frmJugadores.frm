VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form frmJugadores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Jugadores"
   ClientHeight    =   6405
   ClientLeft      =   45
   ClientTop       =   300
   ClientWidth     =   9270
   Icon            =   "frmJugadores.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6405
   ScaleWidth      =   9270
   Tag             =   "Jugadores."
   Begin TabDlg.SSTab SSTab1 
      Height          =   6345
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9270
      _ExtentX        =   16351
      _ExtentY        =   11192
      _Version        =   327680
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "&Datos Jugador"
      TabPicture(0)   =   "frmJugadores.frx":030A
      Tab(0).ControlCount=   22
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblJug(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblJug(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblJug(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblJug(4)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblJug(5)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblJug(6)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblJug(3)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblJug(11)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtJug(0)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtJug(1)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtJug(2)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtJug(4)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtJug(6)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "fraJug(1)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtJug(3)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "txtJug(5)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cmdJug(0)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "cmdJug(1)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "lstJug(1)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "fraJug(2)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "txtJug(11)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "chkJug"
      Tab(0).Control(21).Enabled=   0   'False
      TabCaption(1)   =   "&Resultados"
      TabPicture(1)   =   "frmJugadores.frx":0326
      Tab(1).ControlCount=   3
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "msgJug"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraJug(0)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "chJug"
      Tab(1).Control(2).Enabled=   0   'False
      Begin MSChartLib.MSChart chJug 
         Height          =   3435
         Left            =   -74820
         OleObjectBlob   =   "frmJugadores.frx":0342
         TabIndex        =   32
         Top             =   480
         Width           =   8895
      End
      Begin VB.Frame fraJug 
         Caption         =   " Tipo de Gr�fica "
         Height          =   915
         Index           =   0
         Left            =   -69480
         TabIndex        =   34
         Top             =   4500
         Width           =   2895
         Begin VB.OptionButton optJug 
            Caption         =   "&Tartas"
            Height          =   255
            Index           =   1
            Left            =   1680
            TabIndex        =   36
            Top             =   360
            Width           =   915
         End
         Begin VB.OptionButton optJug 
            Caption         =   "&Barras"
            Height          =   255
            Index           =   0
            Left            =   300
            TabIndex        =   35
            Top             =   360
            Value           =   -1  'True
            Width           =   915
         End
      End
      Begin MSFlexGridLib.MSFlexGrid msgJug 
         Height          =   1455
         Left            =   -74220
         TabIndex        =   33
         Top             =   4320
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   2566
         _Version        =   327680
         Rows            =   6
         Cols            =   4
         AllowBigSelection=   0   'False
         ScrollBars      =   0
         BorderStyle     =   0
      End
      Begin VB.CheckBox chkJug 
         Caption         =   "M�quina         o Programa"
         Height          =   600
         Left            =   4200
         TabIndex        =   5
         Top             =   1080
         Width           =   1035
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   3120
         TabIndex        =   4
         Top             =   1245
         Width           =   870
      End
      Begin VB.Frame fraJug 
         Caption         =   "Jugadores"
         Height          =   1800
         Index           =   2
         Left            =   180
         TabIndex        =   30
         Top             =   4380
         Width           =   8130
         Begin VB.ListBox lstJug 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1500
            Index           =   0
            ItemData        =   "frmJugadores.frx":2783
            Left            =   75
            List            =   "frmJugadores.frx":2785
            Sorted          =   -1  'True
            TabIndex        =   17
            Top             =   240
            Width           =   7980
         End
      End
      Begin VB.ListBox lstJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         ItemData        =   "frmJugadores.frx":2787
         Left            =   2700
         List            =   "frmJugadores.frx":2789
         Sorted          =   -1  'True
         TabIndex        =   9
         Top             =   2220
         Visible         =   0   'False
         Width           =   2460
      End
      Begin VB.CommandButton cmdJug 
         Default         =   -1  'True
         Height          =   750
         Index           =   1
         Left            =   8400
         Picture         =   "frmJugadores.frx":278B
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   4440
         Width           =   765
      End
      Begin VB.CommandButton cmdJug 
         Caption         =   "..."
         Height          =   360
         Index           =   0
         Left            =   4845
         TabIndex        =   8
         Top             =   1830
         Width           =   315
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   2700
         TabIndex        =   7
         Top             =   1830
         Width           =   2160
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1740
         Index           =   3
         Left            =   5295
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   600
         Width           =   3825
      End
      Begin VB.Frame fraJug 
         Caption         =   "Datos de contacto"
         Height          =   2130
         Index           =   1
         Left            =   150
         TabIndex        =   18
         Top             =   2220
         Width           =   4590
         Begin VB.TextBox txtJug 
            Height          =   360
            Index           =   10
            Left            =   2640
            TabIndex        =   13
            Top             =   1095
            Width           =   1680
         End
         Begin VB.TextBox txtJug 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   360
            Index           =   9
            Left            =   330
            MouseIcon       =   "frmJugadores.frx":2A95
            MousePointer    =   99  'Custom
            TabIndex        =   14
            Top             =   1680
            Width           =   3990
         End
         Begin VB.TextBox txtJug 
            Height          =   360
            Index           =   8
            Left            =   330
            TabIndex        =   12
            Top             =   1095
            Width           =   2175
         End
         Begin VB.TextBox txtJug 
            Height          =   360
            Index           =   7
            Left            =   330
            TabIndex        =   11
            Top             =   510
            Width           =   3990
         End
         Begin VB.Label lblJug 
            AutoSize        =   -1  'True
            Caption         =   "Tel�fono"
            Height          =   195
            Index           =   10
            Left            =   2640
            TabIndex        =   28
            Top             =   900
            Width           =   630
         End
         Begin VB.Label lblJug 
            AutoSize        =   -1  'True
            Caption         =   "e-mail"
            Height          =   195
            Index           =   9
            Left            =   330
            TabIndex        =   27
            Top             =   1500
            Width           =   405
         End
         Begin VB.Label lblJug 
            AutoSize        =   -1  'True
            Caption         =   "Poblaci�n"
            Height          =   195
            Index           =   8
            Left            =   330
            TabIndex        =   26
            Top             =   900
            Width           =   705
         End
         Begin VB.Label lblJug 
            AutoSize        =   -1  'True
            Caption         =   "Direcci�n postal"
            Height          =   195
            Index           =   7
            Left            =   330
            TabIndex        =   25
            Top             =   300
            Width           =   1140
         End
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   150
         TabIndex        =   6
         Top             =   1830
         Width           =   2415
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Index           =   4
         Left            =   5295
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   15
         Top             =   2580
         Width           =   3825
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   1755
         TabIndex        =   3
         Top             =   1245
         Width           =   1230
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   150
         TabIndex        =   2
         Top             =   1245
         Width           =   1470
      End
      Begin VB.TextBox txtJug 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   150
         TabIndex        =   1
         Top             =   600
         Width           =   4995
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "ELO"
         Height          =   195
         Index           =   11
         Left            =   3120
         TabIndex        =   31
         Top             =   1020
         Width           =   315
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Pais"
         Height          =   195
         Index           =   3
         Left            =   2700
         TabIndex        =   29
         Top             =   1620
         Width           =   300
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Comentarios"
         Height          =   195
         Index           =   6
         Left            =   5280
         TabIndex        =   24
         Top             =   2400
         Width           =   870
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Palmar�s"
         Height          =   195
         Index           =   5
         Left            =   5280
         TabIndex        =   23
         Top             =   420
         Width           =   645
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Club"
         Height          =   195
         Index           =   4
         Left            =   150
         TabIndex        =   22
         Top             =   1620
         Width           =   315
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Nac."
         Height          =   195
         Index           =   2
         Left            =   1755
         TabIndex        =   21
         Top             =   1020
         Width           =   840
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Categor�a"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   20
         Top             =   1020
         Width           =   705
      End
      Begin VB.Label lblJug 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   19
         Top             =   420
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmJugadores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private RsJug   As Recordset
Private Datos As New clsDatos
Private strNom As String
    ' Guarda el nombre del �ltimo jugador
    ' buscado. Se utiliza para refrescar la lista despu�s
    ' de las modificaciones.

Public Estado As String
Private ReLoad As Boolean
' Variable para controlar cambios
Private blnChange As Boolean

Private Sub InicioForm()
'***********************************************************
' Nombre.....................: InicioForm
' Tipo............................: Sub
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno.....: Ninguno
' �ltimo cambio........: 16/8/99
' Descripci�n.............: Crea objetos y configura
' .....................................: el inicio del formulario.
'***********************************************************
    ' Dimensionamos el formulario.
    Width = 9360
    Height = 6780
    
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_JUGADORES
    
    ReLoad = False
    ConfigGrid
    frmAbiertos = frmAbiertos + 1
    DisableAllFields
End Sub

Private Sub cmdJug_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Ayuda de campo
            ListaPaises lstJug(1), txtJug(5)
        Case 1
            ' Bot�n "Buscar Ahora"
            ThrowSearch
    End Select
End Sub

Private Sub chkJug_Click()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub FinForm()
'************************************************************************************
' Nombre......................: FinForm
' Tipo..............................: Sub
' �mbito........................: Privado
' Par�metros..............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio........: 16/8/99
' Descripci�n.............: Destruye los objetos al descargar el formulario.
'***********************************************************************************
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    Set RsJug = Nothing
    Set Datos = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub DisableAllFields()
'****************************************************************************
' Nombre...........................: DisableAllFields
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 16/8/99
' Descripci�n...................: Deshabilita los controles del formulario.
'****************************************************************************
    Dim ctlTextBox As TextBox
    Dim ctlOptBoton As OptionButton
    
    For Each ctlTextBox In txtJug
        DesActivarTxt ctlTextBox
    Next
    DesActivarChk chkJug
    DesActivarLstBox lstJug(0)
    DesActCmd cmdJug(0)
    cmdJug(1).Enabled = False
    With SSTab1
        .Tab = 0
        .TabEnabled(0) = False
        .TabEnabled(1) = False
    End With
'    SSTab1.Enabled = False
End Sub

Private Sub ThrowSearch()
'****************************************************************************
' Nombre.......................: ThrowSearch
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Lanza el proceso de b�squeda.
'****************************************************************************
    Dim strSQL As String
    
    Clock 1
    strNom = "*" & txtJug(0) & "*"
    If Not (Trim$(txtJug(0)) = "") Then
        ' Buscar por los criterios seleccionados.
        strSQL = "SELECT * FROM Jugadores " & _
                "WHERE ju_apenom LIKE " & _
                SnglQuote(strNom) & _
                " ORDER BY ju_apenom"
    Else
        ' Buscar todos
        strSQL = "Jugadores"
    End If
    Set RsJug = dbDatos.OpenRecordset(strSQL)
    If Not RsJug.EOF Then
        DisplayRegs RsJug
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    Clock 0
End Sub

Private Sub DisplayRegs(Rs As Recordset)
'************************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra en la lista de selecci�n los registros
'..............................................: recuperados en el proceso de b�squeda.
'***********************************************************************************

    ActivarLstBox lstJug(0)
    lstJug(0).Clear
    Do While Not Rs.EOF
        With lstJug(0)
            .AddItem CheckForNull(RsJug!ju_apenom, "STRING")
            .ItemData(.NewIndex) = CheckForNull(RsJug!ju_id, "LONG")
        End With
        Rs.MoveNext
    Loop
End Sub

Private Sub SetSelectControls()
'************************************************************************************
' Nombre.............................: SetSelectControls
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Habilita los controles del formulario necesarios
'..............................................: para que el usuario pueda seleccionar entre los
' .............................................: registros recuperados en la b�squeda.
'***********************************************************************************

    Dim ctlTextBox As TextBox
    Dim ctlOptBoton As OptionButton

    mobjControlMDI.Buscar
    For Each ctlTextBox In txtJug
        DesActivarTxt ctlTextBox
    Next
    DesActivarChk chkJug
    DesActCmd cmdJug(0)
    ActivarLstBox lstJug(0)
    cmdJug(1).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
    End With

End Sub

Public Sub Buscar()
    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub SetSearchControls()
'***************************************************************************************
' Nombre.........................: SetSearchControls
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: Ninguno
' Valor de retorno.........: Ninguno
' �ltimo cambio.............: 16/8/99
' Descripci�n.................: Habilita los campos del formulario para que el
'..........................................: usuario pueda introducir los filtros de b�squeda.
'**************************************************************************************

    ActivarTxt txtJug(0)
    txtJug(0).SetFocus
    cmdJug(1).Enabled = True
    DesActivarLstBox lstJug(0)
    With SSTab1
        .TabEnabled(0) = True
        .TabEnabled(1) = False
    End With

End Sub

Public Sub Cancelar()
'****************************************************************************
' Nombre.......................: Cancelar
' Tipo..............................: Sub
' �mbito.........................: P�blico
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 4/8/99
' Descripci�n................: Cancela la acci�n que se estuviera realizando
' ........................................:  y vuelve al estado de espera.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
    
End Sub

Private Sub ClearFields()
'******************************************************************************************
' Nombre...........................: ClearFields
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 5/8/99
' Descripci�n...................: Limpia el contenido de los campos del formulario.
'*****************************************************************************************
    Dim ctlTextBox As TextBox
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    
    For Each ctlTextBox In txtJug
        ctlTextBox = ""
        ctlTextBox.Tag = ""
    Next
    chkJug.Value = 0
    blnChange = False
End Sub

Private Sub lstJug_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Lista de Jugadores.
            ShowJug
    End Select
End Sub

Private Sub SetEditControls()
'****************************************************************************
' Nombre........................: SetEditControls
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Habilita los controles
' ........................................: necesarios para
'.........................................: operaciones de busqueda y alta.
'****************************************************************************
    Dim ctlTextBox As TextBox
    Dim ctlOptBoton As OptionButton
    
    For Each ctlTextBox In txtJug
        ActivarTxt ctlTextBox
    Next
    txtJug(0).SetFocus
    ActivarChk chkJug
    ActCmd cmdJug(0)
    cmdJug(1).Enabled = False
    With SSTab1
        .TabEnabled(0) = True
        If Estado = "ALTA" Then
            .TabEnabled(1) = False
        Else
            .TabEnabled(1) = True
        End If
    End With

End Sub

Private Sub DisplayFields()
'****************************************************************************
' Nombre......................: DisplayFields
' Tipo.............................: Sub
' �mbito........................: Privado
' Par�metros...............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio..........: 17/8/99
' Descripci�n...............: Muestra en el formulario los datos
' .......................................: del registro le�do.
'****************************************************************************

    ClearFields
    With RsJug
        txtJug(0) = CheckForNull(!ju_apenom, "STRING")
        txtJug(1) = CheckForNull(!ju_titulo, "STRING")
        txtJug(2) = IIf(IsNull(!ju_fnac), "", Format$(!ju_fnac, "dd/mm/yyyy"))
        txtJug(3) = CheckForNull(!ju_palmares, "STRING")
        txtJug(4) = CheckForNull(!ju_comen, "STRING")
        txtJug(5) = Datos.GetPais(CheckForNull(!ju_pais, "LONG"))
        txtJug(5).Tag = IIf(IsNull(!ju_pais), "", !ju_pais)
        txtJug(6) = CheckForNull(!ju_club, "STRING")
        txtJug(7) = CheckForNull(!ju_dir, "STRING")
        txtJug(8) = CheckForNull(!ju_pob, "STRING")
        txtJug(9) = CheckForNull(!ju_email, "STRING")
        txtJug(10) = CheckForNull(!ju_tlf, "STRING")
        txtJug(11) = IIf(IsNull(!ju_elo), "", Format$(!ju_elo, "#,###"))
        Select Case UCase(CheckForNull(!ju_tipo, "STRING"))
            Case "H"
                chkJug.Value = 0
            Case "M"
                chkJug.Value = 1
        End Select
    End With
    blnChange = False
End Sub

Public Sub Borrar()
'****************************************************************************
' Nombre.............................: Borrar
' Tipo....................................: Sub
' �mbito...............................: P�blico
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Da de baja un registro.
'****************************************************************************
    Dim Respuesta   As Integer

    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
End Sub

Private Sub Delete()

    On Error GoTo errDel
    
    With lstJug(0)
        .RemoveItem .ListIndex
    End With
    RsJug.Delete
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number
    
End Sub

Public Sub Insertar()
    mobjControlMDI.Alta
    SetEditControls
    ClearFields

End Sub

Public Sub Grabar()
'*************************************************************
' Nombre....................: Grabar
' Tipo...........................: Sub
' �mbito......................: P�blico
' Par�metros.............: Ninguno
' Valor de retorno....: Ninguno
' �ltimo cambio.......: 17/8/99
' Descripci�n............: Graba un reg. en la BB.DD.
' ....................................: con los datos introducidos.
'*************************************************************

    ' Si el objeto RsJug a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If RsJug Is Nothing Then _
        Set RsJug = dbDatos.OpenRecordset("Jugadores", dbOpenDynaset)
    If Not DatosValidos() Then Exit Sub
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            RsJug.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            RsJug.Edit
    End Select

    AsignarValores
    RsJug.Update
    If Estado = "EDICION" Then
        Dim lngItem As Long
        With lstJug(0)
            lngItem = FindItem(lstJug(0), RsJug!ju_id)
            If lngItem <> -1 Then
                .RemoveItem lngItem
            End If
        End With
'        If LCase(RsJug!ju_apenom) Like LCase(strNom) Then
            ' Si la modificaci�n no afecta al criterio de b�squeda,
            ' lo mantenemos en la lista
            With lstJug(0)
                .AddItem RsJug!ju_apenom
                .ItemData(.NewIndex) = RsJug!ju_id
            End With
'        End If
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    ClearFields
    SetSelectControls
    mobjControlMDI.Espera
End Sub

Private Function DatosValidos() As Boolean
'****************************************************************************
' Nombre........................: DatosValidos
' Tipo...............................: Function
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Booleano
' �ltimo cambio...........: 18/8/99
' Descripci�n................: Valida los datos
' ........................................: referidos al jugador.
'****************************************************************************

    Dim msg$, cCtrlErr As Control, iCod%, j%
    Dim FechaValida As Variant
    
    msg = "": iCod = 0: DatosValidos = True
    
    ' Nombre del jugador
    If Len(txtJug(0)) = 0 Then
        Set cCtrlErr = txtJug(0)
        msg = "El nombre del jugador es obligatorio."
        iCod = 1
    ElseIf Len(txtJug(0)) > 50 Then
        Set cCtrlErr = txtJug(0)
        msg = "Longitud m�x. 50 caracteres."
        iCod = 2
    End If

    ' T�tulo
    If iCod = 0 And Len(txtJug(1)) > 20 Then
        Set cCtrlErr = txtJug(1)
        msg = "Longitud m�x. 20 caracteres."
        iCod = 3
    End If
    
    ' Fecha de nacimiento
    If iCod = 0 Then
        FechaValida = Validar(txtJug(2))
        If FechaValida = False Then
            Set cCtrlErr = txtJug(2)
            msg = "El formato de fecha es 'dd/mm/aaaa'."
            iCod = 4
        ElseIf FechaValida = True Then
            ' Fecha nula
            txtJug(2) = ""
        Else
            txtJug(2) = FechaValida
        End If
    End If
    
    ' Palmar�s
    If iCod = 0 And Len(txtJug(3)) > 2500 Then
        Set cCtrlErr = txtJug(3)
        msg = "Longitud m�x. 2500 caracteres."
        iCod = 5
    End If
    
    ' Notas
    If iCod = 0 And Len(txtJug(4)) > 2500 Then
        Set cCtrlErr = txtJug(4)
        msg = "Longitud m�x. 2500 caracteres."
        iCod = 6
    End If
    
    ' Club
    If iCod = 0 And Len(txtJug(6)) > 20 Then
        Set cCtrlErr = txtJug(6)
        msg = "Longitud m�x. 20 caracteres."
        iCod = 7
    End If
    
    ' Direcci�n Postal
    If iCod = 0 And Len(txtJug(7)) > 35 Then
        Set cCtrlErr = txtJug(7)
        msg = "Longitud m�x. 35 caracteres."
        iCod = 8
    End If
    
    ' Poblaci�n
    If iCod = 0 And Len(txtJug(8)) > 20 Then
        Set cCtrlErr = txtJug(8)
        msg = "Longitud m�x. 20 caracteres."
        iCod = 9
    End If
    
    ' E-Mail
    If iCod = 0 And Len(txtJug(9)) > 35 Then
        Set cCtrlErr = txtJug(9)
        msg = "Longitud m�x. 35 caracteres."
        iCod = 10
    End If
    
    ' Tel�fono
    If iCod = 0 And Len(txtJug(10)) > 15 Then
        Set cCtrlErr = txtJug(10)
        msg = "Longitud m�x. 15 caracteres."
        iCod = 11
    End If
    
    ' ELO
    If iCod = 0 Then
        If Not IsNumeric(txtJug(11)) Then
            If Not Trim$(txtJug(11)) = "" Then
                Set cCtrlErr = txtJug(11)
                msg = "El campo ELO es num�rico."
                iCod = 12
            End If
        Else
            If CDbl(txtJug(11)) < 0 Or CDbl(txtJug(11)) > 9999 Then
                Set cCtrlErr = txtJug(11)
                msg = "El valor ELO tiene que estar comprendido" & _
                    " entre 0 y 9.999."
                iCod = 13
            End If
        End If
    End If
    
    
    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
    
End Function

Private Sub AsignarValores()
'****************************************************************************
' Nombre...........................: AsignarValores
' Tipo..................................: Sub
' �mbito.............................: Privado
' Par�metros....................: Ninguno
' Valor de retorno...........: Ninguno
' �ltimo cambio..............: 18/8/99
' Descripci�n...................:  Asigna los valores que el usuario ha
'............................................: introducido en el formulario al Recordset.
'****************************************************************************

    With RsJug
        !ju_apenom = txtJug(0)
        !ju_titulo = txtJug(1)
        !ju_fnac = IIf(Trim$(txtJug(2)) = "", Null, Format(txtJug(2), "dd/mm/yyyy"))
        !ju_palmares = txtJug(3)
        !ju_comen = txtJug(4)
        !ju_pais = IIf(txtJug(5).Tag = "", Null, Val(txtJug(5).Tag))
        !ju_club = txtJug(6)
        !ju_dir = txtJug(7)
        !ju_pob = txtJug(8)
        !ju_email = txtJug(9)
        !ju_tlf = txtJug(10)
        !ju_elo = IIf(Trim$(txtJug(11)) = "", Null, txtJug(11))
        Select Case chkJug.Value
            Case 0
                !ju_tipo = "H"
            Case 1
                !ju_tipo = "M"
        End Select
    End With
    
End Sub


Private Sub ShowJug()
'*************************************************************************
' Nombre.....................: ShowJug
' Tipo............................: Sub
' �mbito.......................: Privado
' Par�metros..............: Ninguno
' Valor de retorno.....: Ninguno
' �ltimo cambio.........: 18/8/99
' Descripci�n..............: Localiza el registro seleccionado
' ......................................: en la lista.
'**************************************************************************
    Dim lngKey As Long

    Clock 1
    lngKey = lstJug(0).ItemData(lstJug(0).ListIndex)
    CheckChanges blnChange
    If RsJug.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        RsJug.Index = "ju_id"
        RsJug.Seek "=", lngKey
    Else
        ' Utilizamos el m�todo FIND si el
        ' Recordset es DYNASET o SNAPSHOT
        RsJug.FindFirst "ju_id =" & lngKey
    End If
    
    ShowStatistics lngKey
    mobjControlMDI.Edicion
    SetEditControls
    DisplayFields
    Clock 0

End Sub

Private Sub lstJug_DblClick(Index As Integer)
    Select Case Index
        Case 1
            ' Ayuda de campo de paises.
            With lstJug(1)
                txtJug(5) = .List(.ListIndex)
                txtJug(5).Tag = .ItemData(.ListIndex)
                .Visible = False
                txtJug(3).SetFocus
            End With
    End Select
End Sub

Private Sub lstJug_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 1
            ' Ayuda de campo de paises.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista
                    lstJug(1).Visible = False
                    txtJug(5).Tag = ""
                    txtJug(5) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstJug_DblClick 1
            End Select
    End Select
End Sub

Private Sub lstJug_LostFocus(Index As Integer)
    Select Case Index
        Case 1
            lstJug(Index).Visible = False
    End Select
End Sub

Private Sub optJug_Click(Index As Integer)
    Select Case Index
        Case 0 ' Gr�fica de Barras
            chJug.chartType = VtChChartType2dBar
        Case 1 ' Gr�fica de Tarta
            chJug.chartType = VtChChartType2dPie
    End Select
End Sub

Private Sub txtJug_Change(Index As Integer)
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub txtJug_DblClick(Index As Integer)
    If Index = 9 And Len(txtJug(9)) > 0 Then
        HyperJump "mailto:" & txtJug(9)
    End If
End Sub

Private Sub txtJug_GotFocus(Index As Integer)
    Select Case Index
        Case 5
            With txtJug(5)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
    txtJug(Index).BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtJug_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 5
            ' Ayuda de campo de pa�s
            If KeyAscii = vbKeyReturn Then
                cmdJug_Click 0
            End If
    End Select
End Sub

Private Sub txtJug_LostFocus(Index As Integer)
    Select Case Index
        Case 2
            ' Fecha Nacimiento
            If IsNumeric(txtJug(Index)) And _
                Len(txtJug(Index)) = 8 Then
                txtJug(Index) = Format$(txtJug(Index), "00/00/0000")
            End If
        Case 11
            ' ELO
            If IsNumeric(txtJug(Index)) Then
                txtJug(Index) = Format$(txtJug(Index), "#,###")
            End If
        Case Else
            txtJug(Index) = Trim$(txtJug(Index))
    End Select
    txtJug(Index).BackColor = CT_COLOR_BLANCO
End Sub

Private Sub ConfigGrid()
'*******************************************************************
' Nombre........................: ConfigGrid
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 15/1/00
' Descripci�n................: Configura el formato del
' ........................................: control MSFlexGrid de esta-
' ........................................: d�sticas de partidas.
'********************************************************************

    With msgJug
        .TextMatrix(1, 0) = "Jugadas"
        .Col = 0
        .Row = 1
        .CellFontBold = True
        .TextMatrix(2, 0) = "Ganadas"
        .Col = 0
        .Row = 2
        .CellFontBold = True
        .TextMatrix(3, 0) = "Tablas"
        .Col = 0
        .Row = 3
        .CellFontBold = True
        .TextMatrix(4, 0) = "Perdidas"
        .Col = 0
        .Row = 4
        .CellFontBold = True
        .TextMatrix(5, 0) = "Sin final"
        .Col = 0
        .Row = 5
        .CellFontBold = True
        .TextMatrix(0, 1) = "Totales"
        .Col = 1
        .Row = 0
        .CellFontBold = True
        .CellAlignment = 4
        .TextMatrix(0, 2) = "Blancas"
        .Col = 2
        .Row = 0
        .CellFontBold = True
        .CellAlignment = 4
        .TextMatrix(0, 3) = "Negras"
        .Col = 3
        .Row = 0
        .CellFontBold = True
        .CellAlignment = 4
    End With
End Sub

Private Sub ShowStatistics(ByVal lngIdJug As Long)
'*****************************************************************
' Nombre.........................: ShowStatistics
' Tipo................................: Sub
' �mbito...........................: Privado
' Par�metros..................: Long
' Valor de retorno.........: Ninguno
' �ltimo cambio.............: 15/1/00
' Descripci�n..................: Rellena los controles
' ..........................................: MSFlexGrid y
' ..........................................: MSChart encargados de
' ..........................................: mostrar las estad�sticas
' ..........................................: del jugador se pasa como
' ..........................................: par�metros.
'****************************************************************
    Dim i%, j%
    Dim sComun As String
    Dim sSql As String
    
    Dim BlancasGanadas As Long
    Dim NegrasGanadas As Long
    Dim BlancasTablas As Long
    Dim NegrasTablas As Long
    Dim BlancasPerdidas As Long
    Dim NegrasPerdidas As Long
    Dim BlancasNofin As Long
    Dim NegrasNofin As Long
    
    Dim RsTmp As Recordset
    
    ' Fragmento com�n de todas las consultas
    sComun = "SELECT COUNT(*) FROM Partidas WHERE "
    
    ' Declara la matriz Variant
    ' (el l�mite inferior no debe ser 0).
    Dim X(1 To 4, 1 To 5) As Variant

'    Set RsJug = dbDatos.OpenRecordset(strSQL)

    ' Establece las etiquetas de las filas.
    X(1, 2) = "Ganadas"
    X(1, 3) = "Tablas"
    X(1, 4) = "Perdidas"
    X(1, 5) = "Sin Final"

    ' Establece las etiquetas de las columnas.
    X(2, 1) = "Totales"
    X(3, 1) = "Blancas"
    X(4, 1) = "Negras"
    
    ' Hallamos los valores estrictamente necesarios.
    ' Blancas Ganadas
    sSql = sComun & "pt_blancas = " & CStr(lngIdJug) & _
            " AND pt_resultado = 1"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    BlancasGanadas = RsTmp(0)
    
    ' Negras Ganadas
    sSql = sComun & "pt_negras = " & CStr(lngIdJug) & _
            " AND pt_resultado = 2"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    NegrasGanadas = RsTmp(0)
    
    ' Blancas Tablas
    sSql = sComun & "pt_blancas = " & CStr(lngIdJug) & _
            " AND pt_resultado = 3"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    BlancasTablas = RsTmp(0)

    ' Negras Tablas
    sSql = sComun & "pt_negras = " & CStr(lngIdJug) & _
            " AND pt_resultado = 3"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    NegrasTablas = RsTmp(0)
    
    ' Blancas Perdidas
    sSql = sComun & "pt_blancas = " & CStr(lngIdJug) & _
            " AND pt_resultado = 2"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    BlancasPerdidas = RsTmp(0)
    
    ' Negras Perdidas
    sSql = sComun & "pt_negras = " & CStr(lngIdJug) & _
            " AND pt_resultado = 1"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    NegrasPerdidas = RsTmp(0)
    
    ' Blancas Sin Final
    sSql = sComun & "pt_blancas = " & CStr(lngIdJug) & _
            " AND pt_resultado = 4"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    BlancasNofin = RsTmp(0)
    
    ' Negras Sin Final
    sSql = sComun & "pt_negras = " & CStr(lngIdJug) & _
            " AND pt_resultado = 4"
    Set RsTmp = dbDatos.OpenRecordset(sSql)
    NegrasNofin = RsTmp(0)
    
    ' Mostramos los resultados en el control Grid
    ' y en el control Chart (Gr�ficas).
    With msgJug
        ' Establece los datos.
        X(2, 2) = BlancasGanadas + NegrasGanadas ' Totales Ganadas
        X(2, 3) = BlancasTablas + NegrasTablas ' Totales Tablas
        X(2, 4) = BlancasPerdidas + NegrasPerdidas ' Totales Perdidas
        X(2, 5) = BlancasNofin + NegrasNofin ' Totales Sin Final
        
        X(3, 2) = BlancasGanadas ' Blancas Ganadas
        .TextMatrix(2, 2) = BlancasGanadas
        X(3, 3) = BlancasTablas ' Blancas Tablas
        .TextMatrix(3, 2) = BlancasTablas
        X(3, 4) = BlancasPerdidas ' Blancas Perdidas
        .TextMatrix(4, 2) = BlancasPerdidas
        X(3, 5) = BlancasNofin ' Blancas Sin Final
        .TextMatrix(5, 2) = BlancasNofin
        
        X(4, 2) = NegrasGanadas ' Negras Ganadas
        .TextMatrix(2, 3) = NegrasGanadas
        X(4, 3) = NegrasTablas ' Negras Tablas
        .TextMatrix(3, 3) = NegrasTablas
        X(4, 4) = NegrasPerdidas ' Negras Perdidas
        .TextMatrix(4, 3) = NegrasPerdidas
        X(4, 5) = NegrasNofin ' Negras Sin Final
        .TextMatrix(5, 3) = NegrasNofin
        
        ' L�neas de Totales
        .TextMatrix(1, 3) = NegrasGanadas + NegrasTablas + NegrasPerdidas + NegrasNofin
        .TextMatrix(1, 2) = BlancasGanadas + BlancasTablas + BlancasPerdidas + BlancasNofin
        .TextMatrix(1, 1) = Val(.TextMatrix(1, 3)) + Val(.TextMatrix(1, 2))
        .TextMatrix(2, 1) = BlancasGanadas + NegrasGanadas
        .TextMatrix(3, 1) = BlancasTablas + NegrasTablas
        .TextMatrix(4, 1) = BlancasPerdidas + NegrasPerdidas
        .TextMatrix(5, 1) = BlancasNofin + NegrasNofin
        
        ' T�tulo de la estad�stica
        chJug.TitleText = RsJug!ju_apenom
    End With


    ' Establece los datos del gr�fico.
    chJug = X
End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub


