VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public Function GetPais(lngKey As Long) As String
'****************************************************************************
' Nombre......................: GetPais
' Tipo..............................: Function
' �mbito........................: P�blico
' Par�metros..............: C�digo del pais
' Valor de retorno..: Denominaci�n del pais
' �ltimo cambio........: 17/8/99
' Descripci�n.............: Obtiene la denominaci�n del pais
'......................................: a partir de su c�digo.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    
    strSQL = "SELECT pa_den FROM Paises WHERE pa_id =" & lngKey
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs.EOF Then
        GetPais = Rs!pa_den
    Else
        GetPais = ""
    End If
    Set Rs = Nothing
End Function

'Public Function GetNAG(bytKey As Byte) As String
''*********************************************************
'' Nombre............: GetNAG
'' Tipo..............: Function
'' �mbito............: P�blico
'' Par�metros........: C�digo de la etiqueta NAG.
'' Valor de retorno..: Denominaci�n de la etiqueta NAG
'' �ltimo cambio.....: 26/8/99
'' Descripci�n.......: Obtiene la denominaci�n de la
''...................: etiqueta NAG a partir de su c�digo.
''*********************************************************
'    Dim Rs As Recordset
'    Dim strSQL As String
'
'    strSQL = "SELECT na_den FROM Nag WHERE na_id =" & bytKey
'    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
'    If Not Rs.EOF Then
'        GetNAG = Rs!na_den
'    Else
'        GetNAG = ""
'    End If
'    Set Rs = Nothing
'
'End Function

Public Function GetJugador(lngKey As Long) As String
'****************************************************************************
' Nombre......................: GetJugador
' Tipo..............................: Function
' �mbito........................: P�blico
' Par�metros..............: C�digo de Jugador.
' Valor de retorno..: String--> Nombre completo del jugador.
' �ltimo cambio........: 7/10/99
' Descripci�n.............: Obtiene en nombre completo del
'......................................: Jugador atrav�s de su c�digo.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    
    strSQL = "SELECT ju_apenom FROM Jugadores WHERE ju_id =" & lngKey
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs.EOF Then
        GetJugador = Rs!ju_apenom
    Else
        GetJugador = ""
    End If
    Set Rs = Nothing

End Function

Public Function GetApertura(lngKey As Long) As String
'****************************************************************************
' Nombre......................: GetApertura
' Tipo..............................: Function
' �mbito........................: P�blico
' Par�metros..............: String.
' Valor de retorno..: String
' �ltimo cambio........: 12/10/99
' Descripci�n.............: Obtiene la denominaci�n de la
'......................................: apertura a partir de su c�digo.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    
    strSQL = "SELECT ap_den FROM Aperturas WHERE ap_id =" & lngKey
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs.EOF Then
        GetApertura = Rs!ap_den
    Else
        GetApertura = ""
    End If
    Set Rs = Nothing

End Function

Public Function GetModalidad(lngKey As Long) As String
'****************************************************************************
' Nombre......................: GetModalidad
' Tipo..............................: Function
' �mbito........................: P�blico
' Par�metros..............: Long
' Valor de retorno..: String
' �ltimo cambio........: 12/10/99
' Descripci�n.............: Obtiene la denominaci�n de la
'......................................: modalidad a partir de su c�digo.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    
    strSQL = "SELECT mo_den FROM Modalidades WHERE mo_id =" & lngKey
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs.EOF Then
        GetModalidad = Rs!mo_den
    Else
        GetModalidad = ""
    End If
    Set Rs = Nothing

End Function

Public Function GetTorneo(lngKey As Long) As String
'****************************************************************************
' Nombre......................: GetTorneo
' Tipo..............................: Function
' �mbito........................: P�blico
' Par�metros..............: Long
' Valor de retorno..: String
' �ltimo cambio........: 12/10/99
' Descripci�n.............: Obtiene la denominaci�n del
'......................................: torneo a partir de su c�digo.
'****************************************************************************
    Dim Rs As Recordset
    Dim strSQL As String
    
    strSQL = "SELECT to_den FROM Torneos WHERE to_id =" & lngKey
    Set Rs = dbDatos.OpenRecordset(strSQL, dbOpenForwardOnly)
    If Not Rs.EOF Then
        GetTorneo = Rs!to_den
    Else
        GetTorneo = ""
    End If
    Set Rs = Nothing

End Function


