Attribute VB_Name = "modDeclaraciones"
Option Explicit

Public mobjControlMDI As New SGBDAjedrez.clsControlMDI
'Public estado As String
 '**********************************************************************
'Explicaion de las variables SelectLista y PincharLista. _
la primera la utilizo para diferenciar a la hora de bus _
car de donde cojo el elemento. _
La segunda la la utilizo al borrar,para saber si he _
pasado por listView.item_click o  no

'****************************************************
 Public frmAbiertos As Integer ' Controla el n� de frm que hay abiertos
 Public Declare Function SendMessage Lib "user32" Alias _
        "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal _
        wParam As Long, lParam As Any) As Long
 Public Const LVS_EX_FULLROWSELECT = &H20
 Public Const LVM_FIRST = &H1000
 Public Const LVM_GETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + &H37
 Public Const LVM_SETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + &H36
 
 Public Declare Function GetDesktopWindow Lib "user32" () As Long
    Public Const SW_SHOWNORMAL = 1
    Public Declare Function ShellExecute Lib "shell32.dll" Alias _
        "ShellExecuteA" (ByVal hWnd As Long, ByVal lpszOp As _
        String, ByVal lpszFile As String, ByVal lpszParams As String, _
        ByVal lpszDir As String, ByVal FsShowCmd As Long) As Long

 Public Const CT_COLOR_BLANCO = &HFFFFFF
 Public Const CT_COLOR_GRIS = &H80000011
 Public Const CT_COLOR_NEGRO = &H0
 Public Const CT_COLOR_CRUDO = &H80FFFF
 
' Nombre de la aplicaci�n
Public Const CT_NOMBRE_APP As String = "Sistema Gestor de Base de Datos de Ajedrez."



' Constante del nombre de fichero de ayuda.
Public Const CT_HELP_FILE As String = "SGBDA Help.hlp"
' Constantes de los identificadores de contexto
' para los formularios de la aplicaci�n.
Public Const CT_HELP_PRINCIPAL As Long = 1
Public Const CT_HELP_APERTURAS As Long = 2
Public Const CT_HELP_FINALES As Long = 3
Public Const CT_HELP_JUGADORES As Long = 4
Public Const CT_HELP_MODALIDADES As Long = 5
Public Const CT_HELP_PAISES As Long = 6
Public Const CT_HELP_PARTIDAS As Long = 7
Public Const CT_HELP_TORNEOS As Long = 8
Public Const CT_HELP_COMPACTAR As Long = 9
Public Const CT_HELP_REPARAR As Long = 10
Public Const CT_HELP_PASSWORD As Long = 11
Public Const CT_HELP_BUSCADOR As Long = 12
Public Const CT_HELP_EXPORTARPGN As Long = 13
Public Const CT_HELP_PROMOCION As Long = 14
Public Const CT_HELP_TIEMPO As Long = 15


' Password de la Base de Datos
Public strPassword As String

' Codigos internos de las piezas
Public Enum TIPOPIEZA
    CodPeonBlanco = 81
    CodTorreBlanca = 82
    CodCaballoBlanco = 83
    CodAlfilBlanco = 84
    CodDamaBlanca = 85
    CodReyBlanco = 86
    CodPeonNegro = 91
    CodTorreNegra = 92
    CodCaballoNegro = 93
    CodAlfilNegro = 94
    CodDamaNegra = 95
    CodReyNegro = 96
    CODCASILLAVACIA = 0
    CODCASILLAMARCA = 13
End Enum

Public Enum TIPOBANDO
    BandoBlanco = 1
    BandoNegro = 2
End Enum

Public Enum TIPOENROQUE
    NINGUNO = 0
    CTBLANCO = 1
    LGBLANCO = 2
    CTNEGRO = 3
    LGNEGRO = 4
End Enum

Public dbDatos As Database

' Matriz que almacena las etiquetas NAG de
' comentario en las partidas en formato PGN.
Public mtNAG(0 To 139) As String
