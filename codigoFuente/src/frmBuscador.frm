VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmBuscador 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscador de partidas"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8955
   Icon            =   "frmBuscador.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   8955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBuscar 
      Height          =   795
      Index           =   7
      Left            =   7860
      Picture         =   "frmBuscador.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   46
      ToolTipText     =   "Ayuda"
      Top             =   3720
      Width           =   975
   End
   Begin VB.ListBox lstBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   2
      Left            =   300
      Sorted          =   -1  'True
      TabIndex        =   15
      Top             =   2580
      Visible         =   0   'False
      Width           =   4635
   End
   Begin VB.ListBox lstBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   3
      Left            =   300
      Sorted          =   -1  'True
      TabIndex        =   19
      Top             =   3240
      Visible         =   0   'False
      Width           =   4635
   End
   Begin VB.ListBox lstBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   4
      Left            =   300
      Sorted          =   -1  'True
      TabIndex        =   23
      Top             =   4140
      Visible         =   0   'False
      Width           =   4635
   End
   Begin VB.ListBox lstBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   1
      Left            =   300
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   1560
      Visible         =   0   'False
      Width           =   5115
   End
   Begin VB.ListBox lstBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   0
      Left            =   300
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   900
      Visible         =   0   'False
      Width           =   5115
   End
   Begin VB.CommandButton cmdBuscar 
      Height          =   795
      Index           =   1
      Left            =   6660
      Picture         =   "frmBuscador.frx":074C
      Style           =   1  'Graphical
      TabIndex        =   29
      ToolTipText     =   "Limpiar campos"
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton cmdBuscar 
      Height          =   795
      Index           =   0
      Left            =   5460
      Picture         =   "frmBuscador.frx":0A56
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Lanzar b�squeda"
      Top             =   3720
      Width           =   975
   End
   Begin VB.Frame fraBuscar 
      Caption         =   " Jugadores "
      Height          =   1575
      Index           =   0
      Left            =   120
      TabIndex        =   41
      Top             =   120
      Width           =   6795
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   180
         TabIndex        =   0
         Top             =   420
         Width           =   5115
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   180
         TabIndex        =   4
         Top             =   1080
         Width           =   5115
      End
      Begin VB.TextBox txtBuscar 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   5640
         MaxLength       =   8
         MultiLine       =   -1  'True
         TabIndex        =   3
         Top             =   420
         Width           =   915
      End
      Begin VB.TextBox txtBuscar 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   5640
         MaxLength       =   8
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   1080
         Width           =   915
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "..."
         Height          =   360
         Index           =   6
         Left            =   5280
         TabIndex        =   1
         Top             =   420
         Width           =   255
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "..."
         Height          =   360
         Index           =   5
         Left            =   5280
         TabIndex        =   5
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Blancas"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   45
         Top             =   240
         Width           =   570
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Negras"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   44
         Top             =   900
         Width           =   510
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "ELO"
         Height          =   195
         Index           =   2
         Left            =   5640
         TabIndex        =   43
         Top             =   240
         Width           =   315
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "ELO"
         Height          =   195
         Index           =   3
         Left            =   5640
         TabIndex        =   42
         Top             =   900
         Width           =   315
      End
   End
   Begin VB.Frame fraBuscar 
      Caption         =   " Resultado "
      Height          =   1575
      Index           =   1
      Left            =   6960
      TabIndex        =   40
      Top             =   120
      Width           =   1875
      Begin VB.OptionButton optBuscar 
         Caption         =   "1 - 0"
         Height          =   255
         Index           =   0
         Left            =   420
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
      Begin VB.OptionButton optBuscar 
         Caption         =   "0 - 1"
         Height          =   255
         Index           =   1
         Left            =   420
         TabIndex        =   9
         Top             =   495
         Width           =   735
      End
      Begin VB.OptionButton optBuscar 
         Caption         =   "Tablas"
         Height          =   255
         Index           =   2
         Left            =   420
         TabIndex        =   10
         Top             =   750
         Width           =   915
      End
      Begin VB.OptionButton optBuscar 
         Caption         =   "Indeterminado"
         Height          =   255
         Index           =   3
         Left            =   420
         TabIndex        =   11
         Top             =   1005
         Width           =   1335
      End
      Begin VB.OptionButton optBuscar 
         Caption         =   "Todos"
         Height          =   255
         Index           =   4
         Left            =   420
         TabIndex        =   12
         Top             =   1260
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin VB.Frame fraBuscar 
      Caption         =   " Caracter�sticas "
      Height          =   1515
      Index           =   2
      Left            =   120
      TabIndex        =   35
      Top             =   1800
      Width           =   8715
      Begin VB.ComboBox cboBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmBuscador.frx":0D60
         Left            =   5640
         List            =   "frmBuscador.frx":0D62
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   420
         Width           =   2955
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   180
         TabIndex        =   13
         Top             =   420
         Width           =   4635
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "..."
         Height          =   360
         Index           =   4
         Left            =   4800
         TabIndex        =   14
         Top             =   420
         Width           =   255
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   180
         TabIndex        =   17
         Top             =   1080
         Width           =   4635
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "..."
         Height          =   315
         Index           =   3
         Left            =   4800
         TabIndex        =   18
         Top             =   1080
         Width           =   255
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   5640
         TabIndex        =   20
         Top             =   1080
         Width           =   2955
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Apertura"
         Height          =   195
         Index           =   4
         Left            =   180
         TabIndex        =   39
         Top             =   240
         Width           =   600
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Modalidad"
         Height          =   195
         Index           =   5
         Left            =   180
         TabIndex        =   38
         Top             =   900
         Width           =   735
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Final"
         Height          =   195
         Index           =   6
         Left            =   5640
         TabIndex        =   37
         Top             =   240
         Width           =   915
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Comentarista"
         Height          =   195
         Index           =   8
         Left            =   5640
         TabIndex        =   36
         Top             =   900
         Width           =   915
      End
   End
   Begin VB.Frame fraBuscar 
      Caption         =   " Otros "
      Height          =   1395
      Index           =   4
      Left            =   120
      TabIndex        =   30
      Top             =   3360
      Width           =   5175
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "..."
         Height          =   360
         Index           =   2
         Left            =   4800
         TabIndex        =   22
         Top             =   420
         Width           =   255
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   180
         TabIndex        =   21
         Top             =   420
         Width           =   4635
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   180
         TabIndex        =   24
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox txtBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   1860
         TabIndex        =   25
         Top             =   960
         Width           =   1920
      End
      Begin VB.TextBox txtBuscar 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   4380
         MaxLength       =   5
         MultiLine       =   -1  'True
         TabIndex        =   26
         Top             =   960
         Width           =   360
      End
      Begin ComCtl2.UpDown updBuscar 
         Height          =   360
         Left            =   4740
         TabIndex        =   27
         Top             =   960
         Width           =   195
         _ExtentX        =   344
         _ExtentY        =   635
         _Version        =   327680
         BuddyControl    =   "txtBuscar(10)"
         BuddyDispid     =   196612
         BuddyIndex      =   10
         OrigLeft        =   4815
         OrigTop         =   960
         OrigRight       =   5010
         OrigBottom      =   1320
         Max             =   255
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Torneo"
         Height          =   195
         Index           =   7
         Left            =   180
         TabIndex        =   34
         Top             =   240
         Width           =   510
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Fecha"
         Height          =   195
         Index           =   9
         Left            =   180
         TabIndex        =   33
         Top             =   780
         Width           =   450
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Lugar"
         Height          =   195
         Index           =   10
         Left            =   1860
         TabIndex        =   32
         Top             =   780
         Width           =   405
      End
      Begin VB.Label lblBuscar 
         AutoSize        =   -1  'True
         Caption         =   "Ronda"
         Height          =   195
         Index           =   11
         Left            =   4380
         TabIndex        =   31
         Top             =   780
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmBuscador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' RecordSet que almacena los datos
' de la partida.
Private rsPt As Recordset

Private Sub ThrowSearch()
'****************************************************************************
' Nombre.............................: ThrowSearch
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/10/99
' Descripci�n.....................: Lanza el proceso de b�squeda.
'****************************************************************************
    Dim strSelect As String
    Dim strWhere As String
    
    Dim bytRslt As Byte
    
    On Error GoTo ThrowSearchError
    
    Clock 1
    strSelect = "SELECT Partidas.pt_id, Jugadores.ju_apenom AS blancas, " & _
        "Jugadores_1.ju_apenom AS negras, " & _
        "Torneos.to_den, Partidas.pt_lugar, Partidas.pt_fecha, " & _
        "Partidas.pt_resultado, Partidas.pt_elobl, Partidas.pt_elong, " & _
        "Aperturas.ap_codigo, Aperturas.ap_den, Partidas.pt_anotador, " & _
        "Finales.fi_den, Modalidades.mo_den, Partidas.pt_ronda " & _
        "FROM (Finales RIGHT JOIN (Aperturas RIGHT JOIN " & _
        "(Torneos RIGHT JOIN (Modalidades RIGHT JOIN " & _
        "(Jugadores RIGHT JOIN Partidas ON Jugadores.ju_id = " & _
        "Partidas.pt_blancas) ON Modalidades.mo_id = Partidas.pt_modalidad) " & _
        "ON Torneos.to_id = Partidas.pt_torneo) ON " & _
        "Aperturas.ap_id = Partidas.pt_apertura) ON " & _
        "Finales.fi_id = Partidas.pt_tipofin) LEFT JOIN " & _
        "Jugadores AS Jugadores_1 ON Partidas.pt_negras = Jugadores_1.ju_id"
        
    strWhere = ""
    
    ' Buscar por los criterios seleccionados.
    ' Jugador con Blancas
    If Not txtBuscar(0).Tag = "" Then
        strWhere = "pt_blancas = " & Val(txtBuscar(0).Tag) & " AND "
    End If
    
    ' Jugador con Negras
    If Not txtBuscar(1).Tag = "" Then
        strWhere = strWhere & "pt_negras = " & _
                Val(txtBuscar(1).Tag) & " AND "
    End If
    
    ' ELO Blancas
    strWhere = strWhere & "pt_elobl >= " & _
            Val(txtBuscar(2)) & " AND "
    
    ' ELO Negras
    strWhere = strWhere & "pt_elong >= " & _
            Val(txtBuscar(3)) & " AND "
    
    ' Resultado
    If Not optBuscar(4).Value Then
        If optBuscar(0).Value = True Then
            bytRslt = 1
        ElseIf optBuscar(1).Value = True Then
            bytRslt = 2
        ElseIf optBuscar(2).Value = True Then
            bytRslt = 3
        ElseIf optBuscar(3).Value = True Then
            bytRslt = 4
        End If
        strWhere = strWhere & "pt_resultado = " & bytRslt & " AND "
    End If
    
    ' Apertura
    If Not txtBuscar(4).Tag = "" Then
        strWhere = strWhere & "pt_apertura = " & _
                Val(txtBuscar(4).Tag) & " AND "
    End If
            
    ' Tipo de Final
    If Not cboBuscar.ListIndex = -1 Then
        strWhere = strWhere & "pt_tipofin = " & _
                Val(cboBuscar.ItemData(cboBuscar.ListIndex)) & " AND "
    End If
    
    ' Modalidad
    If Not txtBuscar(5).Tag = "" Then
        strWhere = strWhere & "pt_modalidad = " & _
                Val(txtBuscar(5).Tag) & " AND "
    End If
    
    ' Comentarista
    strWhere = strWhere & "pt_anotador LIKE " & _
            SnglQuote("*" & txtBuscar(7) & "*") & " AND "
            
    ' Torneo
    If Not txtBuscar(6).Tag = "" Then
        strWhere = strWhere & "pt_torneo = " & _
                Val(txtBuscar(6)) & " AND "
    End If
    
    ' Fecha
    If IsDate(txtBuscar(8)) Then
        strWhere = strWhere & "pt_fecha = " & _
                CDate(txtBuscar(8)) & " AND "
    End If
    
    ' Lugar
    strWhere = strWhere & "pt_lugar LIKE " & _
            SnglQuote("*" & txtBuscar(9) & "*") & " AND "
            
    ' Ronda
    If IsNumeric(txtBuscar(10)) Then
        strWhere = strWhere & "pt_ronda = " & _
                Val(txtBuscar(10)) & " AND "
    End If
    
    If Len(strWhere) > 0 Then
        ' Quitamos el �ltimo AND
        strWhere = Left$(strWhere, Len(strWhere) - 5)
    End If
    
    If Not strWhere = "" Then
        strSelect = strSelect & " WHERE " & strWhere
    End If
    
    Set rsPt = dbDatos.OpenRecordset(strSelect)
    If Not rsPt.EOF Then
        DisplayRegs rsPt
    Else
        MsgBox "No se encontraron registros."
    End If
    
GetExit:
    Clock 0
    Exit Sub
    
ThrowSearchError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub


Private Sub DisplayRegs(Rs As Recordset)
'****************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/10/99
' Descripci�n.....................: Llena la lista del formulario de exportaci�n
' .............................................: con las partidas seleccionadas a partir
' .............................................: del filtro seleccionado por el usuario.
'****************************************************************************

    Dim mItem As ListItem
    On Error GoTo DisplayRegsError
    
    frmExportar.lvwExp.ListItems.Clear
    With Rs
        Do While Not Rs.EOF
            ' Las claves (Key) de los items del lvw no pueden empezar
            ' por un n�mero, por lo que le colocamos una X delante.
            ' Es un poco chapuza pero no hay otra forma de salvar ese bug.
            Set mItem = frmExportar.lvwExp.ListItems.Add(, "X" & CStr(!pt_id), !blancas)
            ' Almacenamos el identificador de partida
            ' en la propiedad TAG para posteriormente
            ' poder recuperarla mediante el m�todo
            ' FINDITEM.
            mItem.Tag = CStr(!pt_id)
            mItem.SubItems(1) = !negras
            mItem.SubItems(2) = CheckForNull(!to_den, "STRING")
            mItem.SubItems(3) = CheckForNull(!pt_lugar, "STRING")
            mItem.SubItems(4) = IIf(!pt_fecha = CDate(vbNull), "", Format(!pt_fecha, "dd/mm/yyyy"))
            Select Case !pt_resultado
                Case 1      ' (1-0)
                    mItem.SubItems(5) = "1-0"
                Case 2      ' (0-1)
                    mItem.SubItems(5) = "0-1"
                Case 3      ' Tablas
                    mItem.SubItems(5) = "1/2-1/2"
                Case 4      ' Indeterminado
                    mItem.SubItems(5) = "*"
            End Select
            mItem.SubItems(6) = CheckForNull(!pt_elobl, "INTEGER")
            mItem.SubItems(7) = CheckForNull(!pt_elong, "INTEGER")
            mItem.SubItems(8) = "(" & CheckForNull(!ap_codigo, "STRING") & _
                ") " & CheckForNull(!ap_den, "STRING")
            mItem.SubItems(9) = CheckForNull(!pt_anotador, "STRING")
            mItem.SubItems(10) = CheckForNull(!fi_den, "STRING")
            mItem.SubItems(11) = CheckForNull(!mo_den, "STRING")
            mItem.SubItems(12) = CheckForNull(!pt_ronda, "STRING")
            .MoveNext
        Loop
    End With
    
GetExit:
    Exit Sub
    
DisplayRegsError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume Next

End Sub

Private Sub cmdBuscar_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Lanza la b�squeda
            ThrowSearch
            Me.Hide
        Case 1
            ' Limpiar campos
            ClearFields
        Case 2
            ' Ayuda de campo Torneos
            ListaTorneos lstBuscar(4), txtBuscar(6)
        Case 3
            ' Ayuda de campo Modalidad
            ListaModalidades lstBuscar(3), txtBuscar(5)
        Case 4
            ' Ayuda de campo Apertura
            ListaAperturas lstBuscar(2), txtBuscar(4)
        Case 5
            ' Ayuda de campo Negras
            ListaJugadores lstBuscar(1), txtBuscar(1)
        Case 6
            ' Ayuda de campo Blancas
            ListaJugadores lstBuscar(0), txtBuscar(0)
        Case 7
            ' Ayuda
            frmPadre.MuestraAyuda HelpContextID
    End Select
End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub InicioForm()
    LoadCboFins cboBuscar
    HelpContextID = CT_HELP_BUSCADOR
End Sub

Private Sub ClearFields()
'****************************************************************************
' Nombre......................: ClearFields
' Tipo.............................: Sub
' �mbito........................: Privado
' Par�metros...............: Ninguno
' Valor de retorno......: Ninguno
' �ltimo cambio.........: 17/6/00
' Descripci�n..............: Limpia todos los controles que
' ......................................: sirven de entrada de criterios
' ......................................: para las b�squedas.
'****************************************************************************
    Dim ctlTxt As TextBox
    Dim ctlLst As ListBox
    
    For Each ctlTxt In txtBuscar
        ctlTxt.Text = ""
        ctlTxt.Tag = ""
    Next
    
    For Each ctlLst In lstBuscar
        ctlLst.Clear
        ctlLst.Visible = False
    Next
    
    cboBuscar.ListIndex = -1
    optBuscar(4).Value = True
    

End Sub

Private Sub txtBuscar_Change(Index As Integer)
    Select Case Index
        Case 0, 1, 4, 5, 6
            If txtBuscar(Index) = "" Then txtBuscar(Index).Tag = ""
    End Select

End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    Select Case Index
        Case 0, 1, 4, 5, 6
            With txtBuscar(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
    txtBuscar(Index).BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 2, 3, 10  ' ELO's y Ronda
            Select Case KeyAscii
                Case vbKeyReturn
                    KeyAscii = vbKeyShift
            End Select
        Case 0
            ' Ayuda de campo Jug. Blancas
            If KeyAscii = vbKeyReturn Then
                cmdBuscar_Click 6
            End If
        Case 1
            ' Ayuda de campo Jug. Negras
            If KeyAscii = vbKeyReturn Then
                cmdBuscar_Click 5
            End If
        Case 4
            ' Ayuda de campo Aperturas
            If KeyAscii = vbKeyReturn Then
                cmdBuscar_Click 4
            End If
        Case 5
            ' Ayuda de campo Modalidad
            If KeyAscii = vbKeyReturn Then
                cmdBuscar_Click 3
            End If
        Case 6
            ' Ayuda de campo Torneo
            If KeyAscii = vbKeyReturn Then
                cmdBuscar_Click 2
            End If
    End Select
End Sub

Private Sub txtBuscar_LostFocus(Index As Integer)
    Select Case Index
        Case 2, 3, 10  ' ELO's y Ronda
            If IsNumeric(txtBuscar(Index)) Then
                txtBuscar(Index) = Format$(txtBuscar(Index), "#,##0")
            End If
        Case 8  ' Fecha
            If IsNumeric(txtBuscar(Index)) And _
                Len(txtBuscar(Index)) = 8 Then
                txtBuscar(Index) = Format$(txtBuscar(Index), "00/00/0000")
            End If
    End Select
    txtBuscar(Index).BackColor = CT_COLOR_BLANCO
End Sub

Private Sub lstBuscar_DblClick(Index As Integer)
    On Error GoTo LstDblClickError

    Select Case Index
        Case 0      ' Ayuda de campo Jug. Blancas
            With lstBuscar(Index)
                txtBuscar(0) = .List(.ListIndex)
                txtBuscar(0).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            txtBuscar(2).SetFocus
        Case 1      ' Ayuda de campo Jug. Negras
            With lstBuscar(Index)
                txtBuscar(1) = .List(.ListIndex)
                txtBuscar(1).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            txtBuscar(3).SetFocus
        Case 2      ' Ayuda de campo Aperturas
            With lstBuscar(Index)
                txtBuscar(4) = .List(.ListIndex)
                txtBuscar(4).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            cboBuscar.SetFocus
        Case 3      ' Ayuda de campo Modalidades
            With lstBuscar(Index)
                txtBuscar(5) = .List(.ListIndex)
                txtBuscar(5).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            txtBuscar(7).SetFocus
        Case 4      ' Ayuda de campo Torneos
            With lstBuscar(Index)
                txtBuscar(6) = .List(.ListIndex)
                txtBuscar(6).Tag = .ItemData(.ListIndex)
                .Visible = False
            End With
            txtBuscar(8).SetFocus
    End Select
    
GetExit:
    Exit Sub
    
LstDblClickError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub lstBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo LstKeyPressError

    Select Case Index
        Case 0      ' Ayuda de campo Jug. Blancas.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstBuscar(Index).Visible = False
                    txtBuscar(0).Tag = ""
                    txtBuscar(0) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstBuscar_DblClick 0
            End Select
            
        Case 1      ' Ayuda de campo Jug. Negras.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstBuscar(Index).Visible = False
                    txtBuscar(1).Tag = ""
                    txtBuscar(1) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstBuscar_DblClick 1
            End Select
            
        Case 2      ' Ayuda de campo Aperturas.
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstBuscar(Index).Visible = False
                    txtBuscar(4).Tag = ""
                    txtBuscar(4) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstBuscar_DblClick 2
            End Select
            
        Case 3      ' Ayuda de campo Modalidades
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstBuscar(Index).Visible = False
                    txtBuscar(5).Tag = ""
                    txtBuscar(5) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstBuscar_DblClick 3
            End Select
            
        Case 4      ' Ayuda de campo Torneos
            Select Case KeyAscii
                Case 27
                    ' Si ESC escondemos la lista.
                    lstBuscar(Index).Visible = False
                    txtBuscar(6).Tag = ""
                    txtBuscar(6) = ""
                Case 13
                    ' ENTER = Doble Click
                    lstBuscar_DblClick 4
            End Select
    End Select
    
GetExit:
    Exit Sub
    
LstKeyPressError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume GetExit

End Sub

Private Sub lstBuscar_LostFocus(Index As Integer)
    Select Case Index
        Case 0, 1, 2, 3, 4
            lstBuscar(Index).Visible = False
    End Select
End Sub

