VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsControlMDI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Dim i As Integer

Sub ActivarFrm(FRM As Form, volver As Boolean)
'****************************************************************************
' Nombre..................: ActivarFrm
' Tipo....................: Procedure
' Ambito..................: Public
' Par�metros..............: Formulario,estado de la MDI
' ........................: y volver (ida o vuelta)
' Valor de retorno........:
' Ultimo cambio...........: 11/6/99
' Descripci�n.............: Se realiza al activarse el form.
' ........................: seleccionado.Se configura la MDI
' ........................: dependiendo del estado en el que estuviese el form.
'****************************************************************************
'Activar el formulario seleccionado _
y se configgura la MDI dependiendo del estado _
en el que estuviese el form anteriormente
' La var. volver es para controlar que en la "ida" _
el formulario no se toque.Solo me interesa en la _
"vuelta"
' IMP :SI USO ACTIVEFORM DA PROBLEMAS YA QUE LA LLAMADA _
 QUE SE HACE EN EL ACTIVATE (EL CUAL SE EJECUTA ANTES DE _
QUE EL FORM EST� CARGADO
If volver Then ' significa que a pasado por deactivate
    Select Case FRM.Estado
        Case "ALTA":    Alta
        Case "BUSCAR":  Buscar
        Case "EDICION": Edicion
        Case "ESPERA":  Espera
    End Select
    ' para traer " al frente " el formulario seleccionado
    FRM.ZOrder
Else
    ' por aui solo se pasa la 1� vez que se carga un formulario _
    por ello inicializo
    FRM.Estado = "ESPERA"
    
    Espera
    
    volver = True
    
End If
End Sub

Public Sub ControlFrmsOpen()
    frmAbiertos = frmAbiertos - 1
    If frmAbiertos = 0 Then
        Dim i As Byte
        
        With frmPadre
            With .tlbOpciones
                .Buttons("botInsertar").Enabled = False
                .Buttons("botBuscar").Enabled = False
                .Buttons("botEliminar").Enabled = False
                .Buttons("botGuardar").Enabled = False
                .Buttons("botCancelar").Enabled = False
            End With
            .mnuReg.Enabled = False
        End With
    End If
    
End Sub

Public Sub Espera()
'****************************************************************************
' Nombre..................:
' Tipo....................: Sub
' Ambito..................: Public
' Par�metros..............:
' Valor de retorno........: No
' Ultimo cambio...........: 3/6/99
' Descripci�n.............:
' ........................:
'****************************************************************************
With frmPadre
    .ActiveForm.Estado = "ESPERA"
    .ActiveForm.Caption = "Mantenimiento de " & .ActiveForm.Tag
   With .tlbOpciones
        .Buttons("botInsertar").Enabled = True
        .Buttons("botBuscar").Enabled = True
        .Buttons("botEliminar").Enabled = False
        .Buttons("botGuardar").Enabled = False
        .Buttons("botCancelar").Enabled = False
    End With
    .mnuRegAlta.Enabled = True
    .mnuRegBuscar.Enabled = True
    .mnuRegEliminar.Enabled = False
    .mnuRegGuardar.Enabled = False
    .mnuRegCancelar.Enabled = False
    End With
End Sub

Public Sub Alta()
'****************************************************************************
' Nombre..................:
' Tipo....................: Sub
' Ambito..................: Public
' Par�metros..............:
' Valor de retorno........: No
' Ultimo cambio...........: 3/6/99
' Descripci�n.............:
' ........................:
'****************************************************************************
With frmPadre
    .ActiveForm.Estado = "ALTA"
    .ActiveForm.Caption = "Alta Registro " & .ActiveForm.Tag
    ' barra de herramientas
    With .tlbOpciones
        .Buttons("botInsertar").Enabled = False
        .Buttons("botBuscar").Enabled = False
        .Buttons("botEliminar").Enabled = False
        .Buttons("botGuardar").Enabled = True
        .Buttons("botCancelar").Enabled = True
    End With
    .mnuRegAlta.Enabled = False
    .mnuRegBuscar.Enabled = False
    .mnuRegEliminar.Enabled = False
    .mnuRegGuardar.Enabled = True
    .mnuRegCancelar.Enabled = True
    End With
End Sub

Public Sub Edicion()
'****************************************************************************
' Nombre..................:
' Tipo....................: Sub
' Ambito..................: Public
' Par�metros..............:
' Valor de retorno........: No
' Ultimo cambio...........: 3/6/99
' Descripci�n.............:
' ........................:
'****************************************************************************
With frmPadre
    .ActiveForm.Estado = "EDICION"
    .ActiveForm.Caption = "Editar registro " & .ActiveForm.Tag
    With .tlbOpciones
        .Buttons("botInsertar").Enabled = False
        .Buttons("botBuscar").Enabled = False
        .Buttons("botEliminar").Enabled = True
        .Buttons("botGuardar").Enabled = True
        .Buttons("botCancelar").Enabled = True
    End With
    .mnuRegAlta.Enabled = False
    .mnuRegBuscar.Enabled = False
    .mnuRegEliminar.Enabled = True
    .mnuRegGuardar.Enabled = True
    .mnuRegCancelar.Enabled = True
End With
End Sub


Public Sub Buscar()
'****************************************************************************
' Nombre..................:
' Tipo....................: Sub
' Ambito..................: Public
' Par�metros..............:
' Valor de retorno........: No
' Ultimo cambio...........: 3/6/99
' Descripci�n.............: Habilita el boton cancelar de la
' ........................: barra de herramientas
'****************************************************************************

    With frmPadre
        .ActiveForm.Estado = "BUSCAR"
        .ActiveForm.Caption = "Buscar registro " & .ActiveForm.Tag
        With .tlbOpciones
            .Buttons("botInsertar").Enabled = False
            .Buttons("botBuscar").Enabled = False
            .Buttons("botEliminar").Enabled = False
            .Buttons("botGuardar").Enabled = False
            .Buttons("botCancelar").Enabled = True
        End With
        .mnuRegAlta.Enabled = False
        .mnuRegBuscar.Enabled = False
        .mnuRegEliminar.Enabled = False
        .mnuRegGuardar.Enabled = False
        .mnuRegCancelar.Enabled = True
    End With
End Sub

Public Sub Mantenimientos_Off()
'*******************************************************
' Nombre............: Mantenimientos_Off
' Tipo..............: Sub
' Ambito............: Public
' Par�metros........:
' Valor de retorno..: No
' Ultimo cambio.....: 3/6/99
' Descripci�n.......: Deshabilita los accesos a los
' ..................: mantenimientos (botones y
' ..................: opciones de men�). Se utiliza
' ..................: cuando no hay ninguna BBDD activa.
'*******************************************************

    With frmPadre
        ' Botones
        With .tlbOpciones
            .Buttons("botAperturas").Enabled = False ' Aperturas
            .Buttons("botFinales").Enabled = False ' Finales
            .Buttons("botJugadores").Enabled = False ' Jugadores
            .Buttons("botModalidades").Enabled = False ' Modalidades
            .Buttons("botPaises").Enabled = False ' Paises
            .Buttons("botPartidas").Enabled = False ' Partidas
            .Buttons("botTorneos").Enabled = False ' Torneos
        End With
        ' Opciones de men�
        .mnuApes.Enabled = False
        .mnuFins.Enabled = False
        .mnuJugs.Enabled = False
        .mnuModals.Enabled = False
        .mnuPaises.Enabled = False
        .mnuPt.Enabled = False
        .mnuTors.Enabled = False
        
        ' Herramientas
        .mnuCompactDB.Enabled = False
        .mnuRepairDB.Enabled = False
        .mnuPassword.Enabled = False
        .mnuExport.Enabled = False
        
        ' La opci�n de men� "Cerrar Base de Datos".
        .mnuCerrarDB.Enabled = False
    End With

End Sub


Public Sub Mantenimientos_On()
'*******************************************************
' Nombre............: Mantenimientos_On
' Tipo..............: Sub
' Ambito............: Public
' Par�metros........: -
' Valor de retorno..: No
' Ultimo cambio.....: 3/6/99
' Descripci�n.......: Habilita los accesos a los
' ..................: mantenimientos (botones y
' ..................: opciones de men�). Se utiliza
' ..................: cuando activa una BBDD.
'*******************************************************

    With frmPadre
        ' Botones
        With .tlbOpciones
            .Buttons("botAperturas").Enabled = True ' Aperturas
            .Buttons("botFinales").Enabled = True ' Finales
            .Buttons("botJugadores").Enabled = True ' Jugadores
            .Buttons("botModalidades").Enabled = True ' Modalidades
            .Buttons("botPaises").Enabled = True ' Paises
            .Buttons("botPartidas").Enabled = True ' Partidas
            .Buttons("botTorneos").Enabled = True ' Torneos
        End With
        ' Opciones de men�
        ' Mantenimientos
        .mnuApes.Enabled = True
        .mnuFins.Enabled = True
        .mnuJugs.Enabled = True
        .mnuModals.Enabled = True
        .mnuPaises.Enabled = True
        .mnuPt.Enabled = True
        .mnuTors.Enabled = True
        
        ' Herramientas
        .mnuCompactDB.Enabled = True
        .mnuRepairDB.Enabled = True
        .mnuPassword.Enabled = True
        .mnuExport.Enabled = True
        
        ' La opci�n de men� "Cerrar Base de Datos".
        .mnuCerrarDB.Enabled = True
        
    End With

End Sub

