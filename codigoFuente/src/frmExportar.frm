VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmExportar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Exportar partidas a PGN"
   ClientHeight    =   6915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9885
   Icon            =   "frmExportar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6915
   ScaleWidth      =   9885
   Tag             =   "Exportar partidas a PGN"
   Begin MSComDlg.CommonDialog dlgExp 
      Left            =   3660
      Top             =   2220
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame fraExp 
      Caption         =   " Caracter�sticas de la partida "
      Height          =   2595
      Index           =   1
      Left            =   4440
      TabIndex        =   4
      Top             =   60
      Width           =   5355
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   20
         Left            =   1140
         TabIndex        =   25
         Top             =   2040
         Width           =   4095
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Modalidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   19
         Left            =   60
         TabIndex        =   24
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   18
         Left            =   540
         TabIndex        =   23
         Top             =   1680
         Width           =   4275
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Final"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   17
         Left            =   60
         TabIndex        =   22
         Top             =   1680
         Width           =   435
      End
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   16
         Left            =   840
         TabIndex        =   21
         Top             =   1320
         Width           =   4275
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Apertura"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   15
         Left            =   60
         TabIndex        =   20
         Top             =   1320
         Width           =   765
      End
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   14
         Left            =   960
         TabIndex        =   19
         Top             =   960
         Width           =   3855
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Anotador"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   13
         Left            =   60
         TabIndex        =   18
         Top             =   960
         Width           =   825
      End
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   660
         TabIndex        =   17
         Top             =   600
         Width           =   4575
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Lugar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   11
         Left            =   60
         TabIndex        =   16
         Top             =   600
         Width           =   510
      End
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   4800
         TabIndex        =   15
         Top             =   300
         Width           =   435
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Ronda"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   9
         Left            =   4080
         TabIndex        =   14
         Top             =   300
         Width           =   615
      End
      Begin VB.Label lblExp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   840
         TabIndex        =   13
         Top             =   300
         Width           =   3075
      End
      Begin VB.Label lblExp 
         AutoSize        =   -1  'True
         Caption         =   "Torneo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   7
         Left            =   60
         TabIndex        =   12
         Top             =   300
         Width           =   660
      End
   End
   Begin VB.CommandButton cmdExp 
      Height          =   675
      Index           =   1
      Left            =   3540
      Picture         =   "frmExportar.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Exportar partidas a un fichero con formato PGN"
      Top             =   960
      Width           =   795
   End
   Begin VB.CommandButton cmdExp 
      Height          =   675
      Index           =   0
      Left            =   3540
      Picture         =   "frmExportar.frx":1194
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Filtro de selecci�n de partidas"
      Top             =   120
      Width           =   795
   End
   Begin VB.Frame fraExp 
      Caption         =   " Partidas "
      Height          =   4035
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   2760
      Width           =   9735
      Begin ComctlLib.ListView lvwExp 
         Height          =   3315
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   9495
         _ExtentX        =   16748
         _ExtentY        =   5847
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327680
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   13
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Blancas"
            Object.Width           =   5645
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Negras"
            Object.Width           =   5645
         EndProperty
         BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   2
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Torneo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   3
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Lugar"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   2
            SubItemIndex    =   4
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Fecha"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(6) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   2
            SubItemIndex    =   5
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Resultado"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(7) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "ELO Blancas"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "ELO Negras"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   8
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Apertura"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   9
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Anotador"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   10
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   11
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Modalidad"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            SubItemIndex    =   12
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Ronda"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label lblExp 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Resultado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   6
         Left            =   8280
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
      Begin VB.Label lblExp 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   5
         Left            =   7200
         TabIndex        =   10
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label lblExp 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Negras"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   3660
         TabIndex        =   9
         Top             =   300
         Width           =   3495
      End
      Begin VB.Label lblExp 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Blancas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   120
         TabIndex        =   8
         Top             =   300
         Width           =   3495
      End
   End
   Begin ComctlLib.ProgressBar prgExp 
      Height          =   315
      Left            =   120
      TabIndex        =   26
      Top             =   1800
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   556
      _Version        =   327680
      Appearance      =   1
      Min             =   1e-4
   End
   Begin VB.Label lblExp 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   2580
      TabIndex        =   7
      Top             =   2340
      Width           =   855
   End
   Begin VB.Label lblExp 
      AutoSize        =   -1  'True
      Caption         =   "N� de partidas disponibles:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   120
      TabIndex        =   6
      Top             =   2340
      Width           =   2430
   End
   Begin VB.Label lblExp 
      BorderStyle     =   1  'Fixed Single
      Caption         =   $"frmExportar.frx":15D6
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1515
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   3315
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmExportar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Estado As String
Private ReLoad As Boolean

Private Sub InicioForm()
'************************************************************************************
' Nombre....................: InicioForm
' Tipo...........................: Sub
' �mbito......................: Privado
' Par�metros.............: Ninguno
' Valor de retorno....: Ninguno
' �ltimo cambio........: 17/6/00
' Descripci�n............: Crea objetos y configura el inicio del formulario.
'***********************************************************************************
    Dim lngStyle As Long

    ' Dimensionamos el formulario.
    Width = 9975
    Height = 7290
    
    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_EXPORTARPGN
    
    ' Para poder se�alar un fila completa del ListView
    lngStyle = SendMessage(lvwExp.hWnd, _
              LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    lngStyle = lngStyle Or LVS_EX_FULLROWSELECT
    Call SendMessage(lvwExp.hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, ByVal lngStyle)

    ReLoad = False
    frmAbiertos = frmAbiertos + 1
    
End Sub

Private Sub cmdExp_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Bot�n de b�squeda
            frmBuscador.Show vbModal
            ' Mostramos el n�mero de partidas
            lblExp(2) = Format$(lvwExp.ListItems.Count, "#,###")
        Case 1
            ' Bot�n de exportaci�n
            Exportar
    End Select
End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub lvwExp_ItemClick(ByVal Item As ComctlLib.ListItem)
    With Item
        lblExp(8) = .SubItems(2)    ' Torneo
        lblExp(10) = .SubItems(12)  ' Ronda
        lblExp(12) = .SubItems(3)   ' Lugar
        lblExp(14) = .SubItems(9)   ' Anotador
        lblExp(16) = .SubItems(8)   ' Apertura
        lblExp(18) = .SubItems(10)  ' Final
        lblExp(20) = .SubItems(11)  ' Modalidad
    End With
End Sub

Private Sub Exportar()
'************************************************************************************
' Nombre....................: Exportar
' Tipo...........................: Sub
' �mbito......................: Privado
' Par�metros.............: Ninguno
' Valor de retorno....: Ninguno
' �ltimo cambio........: 17/6/00
' Descripci�n............: Exportar las partidas seleccionadas a un fichero
' ....................................: en formato PGN especificado por el operador.
'***********************************************************************************

    Dim mItem As ListItem
    Dim intFile As Integer
    
    On Error GoTo ExportError
    
    Clock 1
    If lvwExp.SelectedItem Is Nothing Then
        ' Si no se ha seleccionado ninguna partida
        ' se aborta el proceso.
        MsgBox "No ha seleccionado ninguna partida.", _
            vbExclamation, Me.Caption
        Clock 0
        Exit Sub
    End If
    With dlgExp
        .DialogTitle = "Guardar fichero PGN"
        .Flags = &H4 + &H2
        .Filter = "Archivos PGN (*.pgn)|*.pgn"
        .DefaultExt = "pgn"
        .CancelError = True
        .ShowSave
        
        ' Abrimos el fichero cre�ndolo
        intFile = FreeFile
        DoEvents
        Open .filename For Output As intFile
    End With
    
    prgExp.Min = 0
    prgExp.Max = lvwExp.ListItems.Count
    For Each mItem In lvwExp.ListItems
        If mItem.Selected = True Then
            EscribirCabecera Mid$(mItem.Key, 2, Len(mItem.Key)), intFile
            EscribirMvtos Mid$(mItem.Key, 2, Len(mItem.Key)), intFile
            ' Resultado al final
            Print #intFile, mItem.SubItems(5)
        End If
        prgExp.Value = mItem.Index
    Next

    Close intFile
    Clock 0
    MsgBox "La exportaci�n de partidas ha sido realizada con " & _
            " �xito", vbOKOnly, "Resultado de la exportaci�n"
    
    prgExp.Value = 0
    Exit Sub
    
ExportError:
    If Err.Number = 32755 Then
        Exit Sub
    Else
        MsgBox "Error desconocido al abrir el archivo"
    End If
    Clock 0
End Sub

Private Sub EscribirCabecera(lngPartida As Long, intFichero As Integer)
'****************************************************************************
' Nombre.....................: EscribirCabecera
' Tipo............................: Sub
' �mbito.......................: Privado
' Par�metros..............: Identificador de partida, N� de fichero
' Valor de retorno.....: Ninguno
' �ltimo cambio.........: 19/6/00
' Descripci�n..............: Escribe en el fichero que se pasa como
' ......................................: par�metro la cabecera de la partida
' ......................................: seg�n el standard PGN. Los datos de las
' ......................................: cabeceras se toman de los almacenados en
' ......................................: el control listview para ahorrar accesos
' ......................................: a la base de datos a trav�s de
' ......................................: instrucciones SQL.
'****************************************************************************
    Dim mItem As ListItem
    Dim strLinea As String
    On Error GoTo CabeceraError
    
    ' Linea en blanco
    Print #intFichero,
        
    Set mItem = lvwExp.FindItem(CStr(lngPartida), lvwTag)
    With mItem
        If Not mItem Is Nothing Then
            ' Torneo
            strLinea = "[Event " & Chr(34) & _
                IIf(.SubItems(2) = "", "?", .SubItems(2)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Lugar
            strLinea = "[Site " & Chr(34) & _
                IIf(.SubItems(3) = "", "?", .SubItems(3)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Fecha
            strLinea = "[Date " & Chr(34) & _
                IIf(.SubItems(4) = "", "?", .SubItems(4)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Ronda
            strLinea = "[Round " & Chr(34) & _
                IIf(.SubItems(12) = "", "?", .SubItems(12)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Jugador con blancas
            strLinea = "[White " & Chr(34) & _
                IIf(.Text = "", "?", .Text) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Jugador con negras
            strLinea = "[Black " & Chr(34) & _
                IIf(.SubItems(1) = "", "?", .SubItems(1)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Resultado
            strLinea = "[Result " & Chr(34) & _
                IIf(.SubItems(5) = "", "?", .SubItems(5)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' ELO blanco
            strLinea = "[WhiteElo " & Chr(34) & _
                IIf(.SubItems(6) = "", "?", .SubItems(6)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' ELO negro
            strLinea = "[BlackElo " & Chr(34) & _
                IIf(.SubItems(7) = "", "?", .SubItems(7)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Apertura
            strLinea = "[Opening " & Chr(34) & _
                IIf(.SubItems(8) = "", "?", .SubItems(8)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Final
            strLinea = "[Termination " & Chr(34) & _
                IIf(.SubItems(10) = "", "?", .SubItems(10)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Anotador
            strLinea = "[Annotator " & Chr(34) & _
                IIf(.SubItems(9) = "", "?", .SubItems(9)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Modalidad
            strLinea = "[Mode " & Chr(34) & _
                IIf(.SubItems(11) = "", "?", .SubItems(11)) & _
                Chr(34) & "]"
            Print #intFichero, strLinea
            ' Linea en blanco
            Print #intFichero,
        End If
    End With
    
    Exit Sub
    
CabeceraError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume Next

End Sub

Private Sub EscribirMvtos(lngPartida As Long, intFichero As Integer)
'****************************************************************************
' Nombre....................: EscribirMvtos
' Tipo...........................: Sub
' �mbito......................: Privado
' Par�metros.............: N� de fichero
' Valor de retorno....: Ninguno
' �ltimo cambio.......: 20/6/00
' Descripci�n............: Escribe en el fichero que se pasa como
' ....................................: par�metro los movimientos en notaci�n
' ....................................: algebraica larga de la partida
' ....................................: seg�n el standard PGN.
'****************************************************************************
    Dim strSQL As String
    Dim vntResultadoMv As Variant
    Dim rsMv As Recordset
    Dim strLinea As String
    Dim blnComentBl As Boolean
    
    ' Objeto que almacena en memoria
    ' la posici�n de la partida.
    Dim mobjTablero As New clsTablero
    
    On Error GoTo WriteMovesError
    
    ' Recuperamos de la tabla "Movimientos" todos aquellos correspondientes
    ' a la partida seleccionada.
    strSQL = "SELECT * FROM Movimientos WHERE mv_partida = " & lngPartida
    Set rsMv = dbDatos.OpenRecordset(strSQL, dbOpenDynaset)
    ' Inicializamos el tablero virtual para introducir los
    ' movimientos recogidos de la BB.DD.
    mobjTablero.Inicializar
    ' Cargamos los movimientos registrados para la partida.
    With rsMv
        Do While Not .EOF
            strLinea = ""
            If !mv_bando = BandoBlanco Then
                ' Si es un movimiento del bando blanco
                ' a�adimos el n�mero de movimiento
                strLinea = !mv_nmov & ". "
            ElseIf !mv_bando = BandoNegro And blnComentBl Then
                strLinea = !mv_nmov & "... "
            End If
            blnComentBl = False
            ' Para todos los movimientos validaremos
            ' su legalidad a trav�s de la clase "mobjTablero".
            vntResultadoMv = _
                mobjTablero.Mover(!mv_orig \ 10, !mv_orig Mod 10, !mv_dest \ 10, !mv_dest Mod 10)
            If Not vntResultadoMv = False Then
                strLinea = strLinea & CStr(vntResultadoMv) & " "
            End If
            If Not IsNull(!mv_coment) Then
                ' Si el movimiento contiene un comentario
                ' lo escribimos entre llaves como especifica
                ' el standard PGN.
                strLinea = strLinea & "{" & !mv_coment & "} "
                If !mv_bando = BandoBlanco Then
                    ' Si el comentario pertenece al bando blanco
                    ' con la variable booleana "blnComentBl" in-
                    ' dicamos que para el bando negro hay que
                    ' escribir el n�mero de movimiento seguido de
                    ' tres puntos.
                    blnComentBl = True
                End If
            End If
            ' Imprimimos la l�nea en el fichero
            Print #intFichero, strLinea;
            ' Leemos el siguiente movimiento
            .MoveNext
        Loop
    End With
    
    ' Linea en blanco
    Print #intFichero,

GetExit:
    Exit Sub
    
WriteMovesError:
    MsgBox "Codigo:" & vbTab & Err.Number & vbCrLf & vbCrLf & _
        "Descripci�n:" & vbTab & Err.Description & vbCrLf & vbCrLf, _
        vbInformation, " " & Err.Source
    Resume Next

End Sub
