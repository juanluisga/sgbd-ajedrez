VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPaises 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pa�ses"
   ClientHeight    =   4110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6645
   Icon            =   "frmPaises.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4110
   ScaleWidth      =   6645
   Tag             =   "Paises."
   Begin MSComDlg.CommonDialog dlgPais 
      Left            =   6180
      Top             =   3600
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Buscar bandera"
      Filter          =   "Mapa de bits|*.bmp|Im�genes GIF|*.gif|Im�genes JPEG|*.jpg|Todas las im�genes|*.bmp;*.gif;*.jpg "
      FilterIndex     =   4
   End
   Begin VB.Frame fraPais 
      Caption         =   " Bandera "
      Height          =   2295
      Index           =   2
      Left            =   4320
      TabIndex        =   5
      Top             =   120
      Width           =   2235
      Begin VB.CommandButton cmdPais 
         Height          =   540
         Index           =   2
         Left            =   1320
         Picture         =   "frmPaises.frx":0442
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Borrar bandera"
         Top             =   1620
         Width           =   780
      End
      Begin VB.CommandButton cmdPais 
         Height          =   540
         Index           =   1
         Left            =   120
         Picture         =   "frmPaises.frx":074C
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Buscar bandera"
         Top             =   1620
         Width           =   780
      End
      Begin VB.Image imgPais 
         BorderStyle     =   1  'Fixed Single
         Height          =   1215
         Left            =   120
         OLEDropMode     =   1  'Manual
         Stretch         =   -1  'True
         ToolTipText     =   "Hacer doble click para borrar bandera."
         Top             =   300
         Width           =   1995
      End
   End
   Begin VB.Frame fraPais 
      Caption         =   "Nombre del  pa�s"
      Height          =   750
      Index           =   0
      Left            =   150
      TabIndex        =   4
      Top             =   150
      Width           =   4020
      Begin VB.TextBox txtPais 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   150
         TabIndex        =   1
         Top             =   240
         Width           =   3735
      End
   End
   Begin VB.Frame fraPais 
      Caption         =   "Pa�ses"
      Height          =   2685
      Index           =   1
      Left            =   180
      TabIndex        =   0
      Top             =   1320
      Width           =   4020
      Begin VB.ListBox lstPaises 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2220
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   3
         Top             =   300
         Width           =   3720
      End
   End
   Begin VB.CommandButton cmdPais 
      Default         =   -1  'True
      Height          =   660
      Index           =   0
      Left            =   5100
      Picture         =   "frmPaises.frx":0B8E
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Buscar pa�s"
      Top             =   2820
      Width           =   780
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00E0E0E0&
      BorderWidth     =   5
      X1              =   180
      X2              =   4140
      Y1              =   1080
      Y2              =   1080
   End
End
Attribute VB_Name = "frmPaises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strDen As String
    ' Guarda la denominaci�n del �ltimo tipo de modalidad
    ' buscado. Se utiliza para refrescar la lista despu�s
    ' de las modificaciones.
Private RsPais  As Recordset

Public Estado As String
Private ReLoad As Boolean
' Variable para controlar cambios
Private blnChange As Boolean

Private Sub InicioForm()
'***********************************************************************************
' Nombre.........................: InicioForm
' Tipo................................: Sub
' �mbito............................: Privado
' Par�metros..................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio.............: 12/8/99
' Descripci�n..................: Crea objetos y configura el inicio del form.
'***********************************************************************************
    ' Dimensionamos el formulario.
    Width = 6735
    Height = 4485

    ' Definimos el �ndice de la ayuda
    HelpContextID = CT_HELP_PAISES
    
    ReLoad = False
    frmAbiertos = frmAbiertos + 1
    DisableAllFields
End Sub

Private Sub cmdPais_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Buscar Pa�s
            ThrowSearch
        Case 1
            ' Buscar Bandera
            SearchFlag
        Case 2
            ' Borrar Bandera
            DeleteFlag
    End Select
End Sub

Private Sub Form_Activate()
    mobjControlMDI.ActivarFrm Me, ReLoad
End Sub

Private Sub FinForm()
'****************************************************************************
' Nombre..........................: FinForm
' Tipo.................................: Sub
' �mbito............................: Privado
' Par�metros...................: Ninguno
' Valor de retorno..........: Ninguno
' �ltimo cambio.............: 12/8/99
' Descripci�n..................: Destruye los objetos.
'****************************************************************************

'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If

    CheckChanges blnChange
    Set RsPais = Nothing
    mobjControlMDI.ControlFrmsOpen

End Sub

Private Sub Form_Unload(Cancel As Integer)
    FinForm
End Sub

Private Sub Form_Load()
    InicioForm
End Sub

Private Sub DisableAllFields()
'**************************************************************************************
' Nombre..............................: DisableAllFields
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio.................: 12/8/99
' Descripci�n......................: Deshabilita los campos del formulario.
'*************************************************************************************

    DesActivarTxt txtPais
    DesActivarLstBox lstPaises
    imgPais.Enabled = False
    cmdPais(0).Enabled = False
    cmdPais(1).Enabled = False
    cmdPais(2).Enabled = False
End Sub

Private Sub DisplayRegs(Rs As Recordset)
 '************************************************************************************
' Nombre.............................: DisplayRegs
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Recordset
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Muestra en la lista de selecci�n los registros
'..............................................: recuperados en el proceso de b�squeda.
'***********************************************************************************

    ActivarLstBox lstPaises
    lstPaises.Clear
    Do While Not Rs.EOF
        With lstPaises
            .AddItem CheckForNull(RsPais!pa_den, "STRING")
            .ItemData(.NewIndex) = CheckForNull(RsPais!pa_id, "LONG")
        End With
        Rs.MoveNext
    Loop
End Sub

Private Sub SetSelectControls()
'************************************************************************************
' Nombre.............................: SetSelectControls
' Tipo....................................: Sub
' �mbito...............................: Privado
' Par�metros......................: Ninguno
' Valor de retorno.............: Ninguno
' �ltimo cambio................: 5/8/99
' Descripci�n.....................: Habilita los controles del formulario necesarios
'..............................................: para que el usuario pueda seleccionar entre los
' .............................................: registros recuperados en la b�squeda.
'***********************************************************************************

    mobjControlMDI.Buscar
    DesActivarTxt txtPais
    ActivarLstBox lstPaises
    imgPais.Enabled = False
    cmdPais(0).Enabled = False
    cmdPais(1).Enabled = False
    cmdPais(2).Enabled = False

End Sub

Public Sub Buscar()
    mobjControlMDI.Buscar
    SetSearchControls
End Sub

Private Sub SetSearchControls()
'****************************************************************************************
' Nombre..............................: SetSearchControls
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio.................: 12/8/99
' Descripci�n......................: Habilita los campos del formulario para que
'...............................................: el usuario pueda hacer b�squedas.
'*************************************************************************************
    
    ActivarTxt txtPais
    txtPais.SetFocus
    imgPais.Enabled = False
    cmdPais(0).Enabled = True
    cmdPais(1).Enabled = False
    cmdPais(2).Enabled = False
    DesActivarLstBox lstPaises
End Sub

Private Sub imgPais_OLEDragDrop(Data As DataObject, _
                                Effect As Long, Button As Integer, _
                                Shift As Integer, X As Single, _
                                Y As Single)
    ' DataObject contiene datos de tipo vbCFFiles
    On Error Resume Next
    
    Set imgPais.Picture = LoadPicture(Data.Files(1))
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub lstPaises_Click()
    ' Mostrar el registro seleccionado en la lista.
    Dim lngKey As Long

    Clock 1
    lngKey = lstPaises.ItemData(lstPaises.ListIndex)
    
    CheckChanges blnChange
    
    If RsPais.Type = dbOpenTable Then
        ' Si el Recordset es de tipo TABLE usamos el m�todo SEEK
        RsPais.Index = "pa_id"
        RsPais.Seek "=", lngKey
    Else
        ' Utilizamos el m�todo FIND si el Recordset es DYNASET o SNAPSHOT
        RsPais.FindFirst "pa_id =" & lngKey
    End If
    SetEditControls
    mobjControlMDI.Edicion
    DisplayFields
    Clock 0

End Sub

Private Sub SetEditControls()
'**************************************************************************************
' Nombre..............................: SetEditControls
' Tipo.....................................: Sub
' �mbito................................: Privado
' Par�metros.......................: Ninguno
' Valor de retorno..............: Ninguno
' �ltimo cambio..................: 12/8/99
' Descripci�n.......................: Habilita los controles necesarios para
'................................................: operaciones de busqueda y alta.
'***************************************************************************************

    ActivarTxt txtPais
    txtPais.SetFocus
    imgPais.Enabled = True
    cmdPais(0).Enabled = False
    cmdPais(1).Enabled = True
    cmdPais(2).Enabled = True

End Sub

Private Sub DisplayFields()
'********************************************************************************************
' Nombre.................................: DisplayFields
' Tipo........................................: Sub
' �mbito...................................: Privado
' Par�metros..........................: Ninguno
' Valor de retorno.................: Ninguno
' �ltimo cambio....................: 12/8/99
' Descripci�n.........................: Muestra en el form los datos del registro le�do.
'********************************************************************************************

    ClearFields
    txtPais = CheckForNull(RsPais!pa_den, "STRING")
    If Not IsNull(RsPais!pa_flag) Then
        GetPicture RsPais!pa_flag, imgPais
    End If
    blnChange = False
End Sub

Private Sub ClearFields()
'***************************************************************************************
' Nombre.................................: ClearFields
' Tipo........................................: Sub
' �mbito...................................: Privado
' Par�metros..........................: Ninguno
' Valor de retorno.................: Ninguno
' �ltimo cambio....................: 12/8/99
' Descripci�n.........................: Borra el contenido de los campos del form.
'***************************************************************************************
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If
    
    Set imgPais.Picture = Nothing
    txtPais = ""
    txtPais.Tag = ""
    blnChange = False
End Sub

Public Sub Insertar()
    mobjControlMDI.Alta
    SetEditControls
    ClearFields

End Sub

Public Sub Grabar()
'*************************************************************************************************
' Nombre...............................: Grabar
' Tipo......................................: Sub
' �mbito.................................: P�blico
' Par�metros........................: Ninguno
' Valor de retorno...............: Ninguno
' �ltimo cambio..................: 12/8/99
' Descripci�n.......................: Graba un reg. en la BB.DD. con los datos introducidos.
'*************************************************************************************************

    ' Si el objeto RsPais a�n no se ha abierto,
    ' lo hacemos como tipo Dynaset.
    If RsPais Is Nothing Then _
        Set RsPais = dbDatos.OpenRecordset("paises", dbOpenDynaset)
    If Not DatosValidos() Then Exit Sub
    Select Case Estado
        Case "ALTA"
            ' Si es un ALTA, llamamos al m�todo AddNew para que
            ' reserve espacio para un nuevo registro.
            RsPais.AddNew
        Case "EDICION"
            ' Si es una MODIFICACI�N, llamamos al m�todo Edit para que
            ' proceda a la actualizaci�n del registro actual.
            RsPais.Edit
    End Select

    AsignarValores
    RsPais.Update
    If Estado = "EDICION" Then
        Dim lngItem As Long
        With lstPaises
            lngItem = FindItem(lstPaises, RsPais!pa_id)
            If lngItem <> -1 Then
                .RemoveItem lngItem
            End If
        End With
'        If LCase(RsPais!pa_den) Like LCase(strDen) Then
            ' Si la modificaci�n no afecta al criterio de b�squeda,
            ' lo mantenemos en la lista
            With lstPaises
                .AddItem RsPais!pa_den
                .ItemData(.NewIndex) = RsPais!pa_id
            End With
'        End If
    End If
    MsgBox "El registro ha sido grabado con �xito."
    blnChange = False
    ClearFields
    SetSelectControls
    mobjControlMDI.Espera
End Sub

Private Function DatosValidos() As Boolean
'****************************************************************************
' Nombre........................: DatosValidos
' Tipo...............................: Function
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Booleano
' �ltimo cambio...........: 18/8/99
' Descripci�n................: Valida los datos
' ........................................: referidos al jugador.
'****************************************************************************

Dim msg$, cCtrlErr As Control, iCod%, j%

    msg = "": iCod = 0: DatosValidos = True
    
    If Len(txtPais) = 0 Then
        Set cCtrlErr = txtPais
        msg = "La denominaci�n del final es obligatoria."
        iCod = 1
    ElseIf Len(txtPais) > 25 Then
        Set cCtrlErr = txtPais
        msg = "Longitud m�x. 25 caracteres."
        iCod = 2
    End If

    If iCod > 0 Then
        j = MsgBox(msg, vbCritical, Me.Caption)
        cCtrlErr.SetFocus
        DatosValidos = False
    End If
End Function

Private Sub AsignarValores()
'****************************************************************************
' Nombre...............................: AsignarValores
' Tipo......................................: Sub
' �mbito.................................: Privado
' Par�metros........................: Ninguno
' Valor de retorno...............: Ninguno
' �ltimo cambio..................: 12/8/99
' Descripci�n.......................:  Asigna los valores que el usuario ha
'................................................: introducido en el formulario a la clase.
'****************************************************************************
    RsPais!pa_den = txtPais
    If Not imgPais.Picture = 0 Then
        LetPicture RsPais!pa_flag, imgPais
    Else
        RsPais!pa_flag = Null
    End If
    
End Sub

Public Sub Borrar()
'****************************************************************************
' Nombre................................: Borrar
' Tipo.......................................: Sub
' �mbito..................................: P�blico
' Par�metros.........................: Ninguno
' Valor de retorno................: Ninguno
' �ltimo cambio...................: 12/8/99
' Descripci�n........................: Da de baja un registro.
'****************************************************************************
    Dim Respuesta   As Integer

    mobjControlMDI.Edicion
    Respuesta = MsgBox("Esta operaci�n borrar� el registro que est� editando", _
    vbOKCancel + vbDefaultButton1 + vbQuestion + vbApplicationModal)
    If Respuesta = vbOK Then Delete
    ClearFields
    SetSelectControls
End Sub

Private Sub Delete()
    On Error GoTo errDel
    
    With lstPaises
        .RemoveItem .ListIndex
    End With
    RsPais.Delete
    
GetExit:
    Exit Sub
    
errDel:
    TratarError Err.Number
End Sub

Public Sub Cancelar()
'******************************************************************************************
' Nombre...................................: Cancelar
' Tipo..........................................: Sub
' �mbito.....................................: P�blico
' Par�metros............................: Ninguno
' Valor de retorno...................: Ninguno
' �ltimo cambio......................: 4/8/99
' Descripci�n...........................: Cancela la acci�n que se estuviera
' ...................................................: realizando y vuelve al estado de espera.
'******************************************************************************************
    
'    If blnChange Then
'        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
'            "Cambios no guardados") = vbYes Then Grabar
'    End If

    CheckChanges blnChange
    ClearFields
    DisableAllFields
    mobjControlMDI.Espera
End Sub

Private Sub txtPais_Change()
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub txtPais_GotFocus()
    txtPais.BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtPais_LostFocus()
    txtPais = Trim$(txtPais)
    txtPais.BackColor = CT_COLOR_BLANCO
End Sub

Private Sub ThrowSearch()
'****************************************************************************
' Nombre.......................: ThrowSearch
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Lanza el proceso de b�squeda.
'****************************************************************************

    Dim strSQL As String

    Clock 1
    strDen = "*" & Trim$(txtPais) & "*"
    If Not (txtPais = "") Then
        ' Buscar por la denominaci�n
        strSQL = "SELECT * FROM Paises " & _
            "WHERE pa_den LIKE " & _
            SnglQuote(strDen) & _
            " ORDER BY pa_den"
    Else
        ' Buscar todos
        strSQL = "Paises"
    End If
    Set RsPais = dbDatos.OpenRecordset(strSQL)
    If Not RsPais.EOF Then
        DisplayRegs RsPais
    Else
        MsgBox "No se encontraron registros."
    End If
    SetSelectControls
    Clock 0
End Sub

Private Sub SearchFlag()
'*************************************************************************************
' Nombre.......................: SearchFlag
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Muestra un dialogo com�n "Abrir" a trav�s del
' ........................................: cual es posible buscar un archivo gr�fico que
' ........................................: contenga la bandera del pa�s.
'*************************************************************************************

    Dim strImageFile
    On Error GoTo Cancelar
    With dlgPais
        .ShowOpen
        strImageFile = .filename
        Set imgPais.Picture = LoadPicture(strImageFile)
        If Not Estado = "BUSCAR" Then blnChange = True
    End With
    Exit Sub
    
Cancelar:
    If Err.Number = 32755 Then
        Exit Sub
    Else
        MsgBox "Error desconocido al abrir el archivo " & _
            strImageFile
    End If
End Sub

Private Sub DeleteFlag()
'*************************************************************************************
' Nombre.......................: DeleteFlag
' Tipo..............................: Sub
' �mbito.........................: Privado
' Par�metros................: Ninguno
' Valor de retorno.......: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Borra el contenido del control image destinado
' ........................................: a mostrar la bandera del pa�s seleccionado.
'*************************************************************************************

    Set imgPais.Picture = Nothing
    If Not Estado = "BUSCAR" Then blnChange = True
End Sub

Private Sub CheckChanges(blnCambio As Boolean)
    If blnCambio Then
        If MsgBox("�Desea grabar los cambios?", vbYesNo + vbExclamation, _
            "Cambios no guardados") = vbYes Then Grabar
    End If
End Sub
