VERSION 5.00
Begin VB.Form frmPaso 
   BackColor       =   &H80000004&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Base de Datos protegida con contrase�a"
   ClientHeight    =   1800
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1800
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPaso 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Index           =   1
      Left            =   2520
      TabIndex        =   3
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Frame fraPaso 
      Caption         =   " Contrase�a "
      Height          =   975
      Left            =   360
      TabIndex        =   1
      Top             =   180
      Width           =   3915
      Begin VB.TextBox txtPaso 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   960
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   420
         Width           =   2835
      End
      Begin VB.Image imgPaso 
         Height          =   480
         Left            =   180
         Picture         =   "frmPaso.frx":0000
         Top             =   300
         Width           =   480
      End
   End
   Begin VB.CommandButton cmdPaso 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Index           =   0
      Left            =   1230
      TabIndex        =   2
      Top             =   1320
      Width           =   1215
   End
End
Attribute VB_Name = "frmPaso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strBaseD As String

Public Property Let NombreDB(ByVal sNewValue As String)
    strBaseD = sNewValue
End Property

Private Sub cmdPaso_Click(Index As Integer)
    Select Case Index
        Case 0
            DoAceptar
        Case 1
            Unload Me
    End Select
End Sub

Private Sub DoAceptar()
'*************************************************************************************
' Nombre........................: DoAceptar
' Tipo...............................: Sub
' �mbito..........................: Privado
' Par�metros.................: Ninguno
' Valor de retorno........: Ninguno
' �ltimo cambio...........: 16/8/99
' Descripci�n................: Abre la base de datos utilizando la contrase�a
' ........................................: introducida por el usuario. Si esta no es correcta
'.........................................: la operaci�n de apertura ser� abortada.
'**************************************************************************************

    Dim sPass As String
    On Error GoTo BadPassWord
    
    sPass = ";PWD=" & txtPaso
    Set dbDatos = OpenDatabase(strBaseD, True, False, sPass)
    strPassword = txtPaso
    Me.Caption = CT_NOMBRE_APP & " [" & _
                strBaseD & "]"
    mobjControlMDI.Mantenimientos_On
    Unload Me
    
BadPassWord:
    If Err.Number = 3031 Then
        MsgBox "La contrase�a introducida es incorrecta.", vbExclamation, _
        "Contrase�a err�nea"
        txtPaso.SetFocus
        txtPaso = ""
    End If

End Sub

Private Sub txtPaso_GotFocus()
    txtPaso.BackColor = CT_COLOR_CRUDO
End Sub

Private Sub txtPaso_LostFocus()
    txtPaso.BackColor = CT_COLOR_BLANCO
End Sub
